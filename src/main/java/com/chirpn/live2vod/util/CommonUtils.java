package com.chirpn.live2vod.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
/**
 * @author satish ghonge
 *
 */
public class CommonUtils {
	
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	public static final String CHANNEL_PREFIX = "CHL";
	public static final String GROUP_PREFIX = "GRP";
	public static final String ASSET_PREFIX = "AST";
	public static final String AD_SLOT_PREFIX = "ADS";
	public static final String HLS_PREFIX = "vod_";
	public static final String ASSET_DELETE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final DateTimeFormatter TIMEFORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
	public static final String DATE_AND_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	
	public static String getUUID() {
		UUID idOne = UUID.randomUUID();
		Long date = System.currentTimeMillis();
		return idOne.toString() + date;
	}
	
	public static String formatDate(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static Timestamp parseTimestamp(String date, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return new Timestamp(sdf.parse(date).getTime());
	}
	
	public static boolean isNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static boolean isNotNullOrEmpty(String value) {
		return !isNullOrEmpty(value);
	}

	public static boolean isEmptyString(String stringToCheck) {
		if (stringToCheck == null || stringToCheck.trim().length() <= 0)
			return true;
		return false;
	}
	
	public static <T> boolean isEmpty(Collection<T> objList) {
		return (isNull(objList) || (objList.size() == 0));
	}

	public static <T> boolean isNotEmpty(Collection<T> objList) {
		return !isEmpty(objList);
	}

	public static <K, V> boolean isNotEmpty(Map<K, V> map) {
		return !(isNull(map) || (map.size() == 0));
	}

	public static boolean isNull(Object object) {
		return (object == null) ? Boolean.TRUE : Boolean.FALSE;
	}
	
	private static final String getRandomUUID() {
		return String.valueOf(Math.abs(UUID.randomUUID().getLeastSignificantBits())).substring(5, 8)+ randomAlphaNumeric(3);
	}
	
	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	public static final String generateChannelNumber() {
		return CHANNEL_PREFIX.concat(getRandomUUID());
	}
	
	public static final String generateGroupNumber() {
		return GROUP_PREFIX.concat(getRandomUUID());
	}
	
	public static final String generateAssetNumber() {
		return ASSET_PREFIX.concat(getRandomUUID());
	}
	
	public static final String generateAdSlotNumber() {
		return AD_SLOT_PREFIX.concat(getRandomUUID());
	}
	
	public static final String convertLocalDateToSpecifiedString(LocalDate localDate, DateTimeFormatter formatter) {
		return localDate.format(formatter);
	}
	
	public static final String createDateTime(String date, String startTime, String endTime) {
		if (isNotNullOrEmpty(startTime)) {
			return date+"T"+startTime+"+10:00";
		}
	    if (isNotNullOrEmpty(endTime)) {
	    	return date+"T"+endTime+"+10:00";
	    }
		return null;
	}
	
	public static final String getStartTime(LocalDate localDate, String startTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
		return createDateTime(convertLocalDateToSpecifiedString(localDate, formatter), startTime, null);
	}
	
	public static final String getEndTime(LocalDate localDate, String startTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
		return createDateTime(convertLocalDateToSpecifiedString(localDate, formatter), startTime, null);
	}
	
	public static final String generateUniqueHlsName() {
		return HLS_PREFIX.concat(getRandomUUID());
	}
	
	public static final String convertTimeToSecond(String time) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	    Date date = dateFormat.parse(time);
	    long seconds = date.getTime() / 1000L;
	    return String.valueOf(seconds);
	}
	
	public static final String retrieveLastSundayDate() {
		Calendar c=Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
		c.set(Calendar.HOUR_OF_DAY,0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND,0);
		DateFormat df=new SimpleDateFormat(ASSET_DELETE_DATE_FORMAT);
		return df.format(c.getTime());      // This past Sunday [ May include today ]
	}
	
	public static final LocalDateTime convertStringToLocalDateTime(String date, String time) {
		 int timeLength = time.length();
		 if (timeLength==5) {
			 time = time+":00";
		 }
		String dateTime = date+" "+time;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_AND_TIME_FORMAT); 
		return LocalDateTime.parse(dateTime, formatter);

	}
	
	public static String TimeConversion(String time) {
		//String a = "00:07:46.896";
		if (time.length() == 8) {
			return time;
		}
        String[] tokens = time.split(":");
        int hours = Integer.parseInt(tokens[0]);
        int minutes = Integer.parseInt(tokens[1]);
        int sec = tokens[2].contains(".") ? Integer.parseInt(tokens[2].split("\\.")[0]) : Integer.parseInt(tokens[2]);
        
        return String.format("%02d:%02d:%02d", hours, minutes, sec);
	}
	
	public static final Date getLastThirtyMinuteDateAndTime() {
		Date currentDate = new Date();
	    Calendar c = Calendar.getInstance();
	    c.setTime(currentDate);
	    c.add(Calendar.MINUTE, -30);
	    // convert calendar to date
	    Date newDate = c.getTime();
	    return newDate;
	}
	
	public static final String convertEpochTimeToDateWtihTimeZone(String epochTime) {
		Calendar now = Calendar.getInstance();
	    TimeZone timeZone = now.getTimeZone();
	    Long millis = Long.valueOf(epochTime.contains(".") ?  epochTime.substring(0, epochTime.indexOf(".")) : epochTime); 
		ZonedDateTime dateTime = Instant.ofEpochMilli(millis).atZone(ZoneId.of(timeZone.getID()));
		String formatted = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ"));
		return formatted;
	}
	
	public static final int calculateDuration(String timestamp) {
		if (timestamp.length() == 5) {
			timestamp = timestamp+":00";
		 }
        String[] tokens = timestamp.split(":");
        int hours = Integer.parseInt(tokens[0]);
        int minutes = Integer.parseInt(tokens[1]);
        int seconds = Integer.parseInt(tokens[2]);
        int duration  = 3600 * hours + 60 * minutes + seconds;
        System.out.println("hrs" +hours + "mins" +minutes+ "sec"+seconds +"deu" +duration);
		return duration;
	}
	
	public static final LocalDate convertDateToLocalDate(String date) throws ParseException {
		//1. System Default TimeZone
		ZoneId defaultZoneId = ZoneId.systemDefault();  
		Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(date); 
		
		//2. Convert Date -> Instant
		Instant instant = dt.toInstant();
		
		//3. Instant + system default time zone + toLocalDate() = LocalDate
	    return instant.atZone(defaultZoneId).toLocalDate();
	}
	
}
