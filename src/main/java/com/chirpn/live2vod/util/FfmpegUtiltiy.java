package com.chirpn.live2vod.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
@Component
public class FfmpegUtiltiy {

	private static final Logger LOGGER = Logger.getLogger(FfmpegUtiltiy.class);
	public String getVideoDuration(String ffprobeLocation, String videoPath) throws IOException {
		LOGGER.info("mp4 video: Calculating duration for: " + videoPath);
		FFprobe ffprobe = null;
		ffprobe = new FFprobe(ffprobeLocation);
		FFmpegProbeResult probeResult = ffprobe.probe(videoPath);
		FFmpegFormat format = probeResult.getFormat();
		LOGGER.info("mp4 video: duration: " +  format.duration + " for path: " + videoPath);
		return getFormattedDuration(format.duration);
	}

	public String getFormattedDuration(double videoDurationInDouble) throws IOException {
		long videoDurationInLong = (long) videoDurationInDouble;
		final long dy = TimeUnit.SECONDS.toDays(videoDurationInLong);
		final long hr = TimeUnit.SECONDS.toHours(videoDurationInLong) - TimeUnit.DAYS.toHours(TimeUnit.SECONDS.toDays(videoDurationInLong));
		final long min = TimeUnit.SECONDS.toMinutes(videoDurationInLong) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(videoDurationInLong));

		final long sec = videoDurationInLong - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(videoDurationInLong));

		double remainingSeconds = (videoDurationInDouble - videoDurationInLong) + sec;
		BigDecimal a = new BigDecimal(remainingSeconds).setScale(2, RoundingMode.CEILING);
		LOGGER.info("mp4 video: formatted duration: " +  new String(dy + ":" + hr + ":" + min + ":" + a));
		
		String secAndmillisecond = String.valueOf(a.doubleValue()).length() == 1 ? "0"+ String.valueOf(a.doubleValue()) : String.valueOf(a.doubleValue());
		if (CommonUtils.isNotNullOrEmpty(secAndmillisecond) && secAndmillisecond.contains("."))
		{
			String[] tokens = secAndmillisecond.split("\\.");
			long seconds = Long.parseLong(tokens[0]);
		    long milliseconds = Long.parseLong(tokens[1]);
		    secAndmillisecond = String.format("%02d.%02d", seconds, milliseconds);
	        LOGGER.info("Formatted second and milliseconds:" + secAndmillisecond);
		}
		if (dy == 0) {
			return String.format("%02d:%02d", hr, min)+":"+secAndmillisecond;  
		} else {
			return String.format("%02d:%02d:%02d", dy , hr, min)+":"+secAndmillisecond;
		}
		
		
	}

}
