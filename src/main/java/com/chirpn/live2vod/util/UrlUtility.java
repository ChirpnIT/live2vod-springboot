package com.chirpn.live2vod.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.chirpn.live2vod.common.constants.Constants;

public class UrlUtility {
	private static final Logger logger = Logger.getLogger(UrlUtility.class);

    public static String getRootURL(HttpServletRequest httpRequest) {
        String root = "";
        if (httpRequest != null && httpRequest.getRequestURL() != null) {
            String url = httpRequest.getRequestURL().toString();
            String uri = httpRequest.getRequestURI();
            root = url.substring(0, url.indexOf(uri));
            root = root.concat(httpRequest.getContextPath() + "/");

            try {
                if (httpRequest.getHeader("x-forwarded-proto") != null 
                		&& httpRequest.getHeader("x-forwarded-proto").trim().equalsIgnoreCase(Constants.HTTPS)
                		&& !root.contains(Constants.HTTPS)) {
                        root = root.replace(Constants.HTTP, Constants.HTTPS);
                    
                }
            } catch (NullPointerException e) {
                logger.debug("x-forwarded-proto header not found. Connect is http");
            }
        }
        return root;
    }
    
    public static String getBaseURL(HttpServletRequest httpRequest) {
    	String baseURL = "";
		String referrer = httpRequest.getHeader("Referer");
		if (referrer != null) {
			baseURL = referrer;
		} else {
			baseURL = UrlUtility.getRootURL(httpRequest)
					.concat("resources/login.html");
		}
		return baseURL;
    }
}
