package com.chirpn.live2vod.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.model.IntegrationPropertiesDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Entity
@Table(name = "integration_properties")
public class IntegrationProperties implements Serializable {

	private static final long serialVersionUID = -2660765477232677426L;

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationProperties.class);

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "admin_ad_group")
	private String adminADGroup;

	@Column(name = "ldap_url")
	private String ldapUrl;

	@Column(name = "mulesoft_url")
	private String muleSoft;

	@Column(name = "delta_url")
	private String deltaUrl;

	@Column(name = "delta_fail_over_url")
	private String deltaFailOverUrl;

	@Column(name = "mpx_url")
	private String mpxUrl;

	@Column(name = "mpx_fail_over_url")
	private String mpxFailOverUrl;

	@Column(name = "offset_time")
	private long offsetTime;
	
	@Column(name = "delta_url_flag")
	private Boolean deltaUrlFlag = false;
	
	@Column(name = "mpx_url_flag")
	private Boolean mpxUrlFlag = false;
	
	@Column(name = "fail_over_offset_time")
	private long failOverOffsetTime;

	public IntegrationProperties() {
		super();
	}
	
	public IntegrationProperties(IntegrationPropertiesDTO integrationPropertiesDTO) {
		super();
		this.id = integrationPropertiesDTO.getId();
		this.adminADGroup = integrationPropertiesDTO.getAdminADGroup();
		this.ldapUrl = integrationPropertiesDTO.getLdapUrl();
		this.muleSoft = integrationPropertiesDTO.getMuleSoft();
		this.deltaUrl = integrationPropertiesDTO.getDeltaUrl();
		this.deltaFailOverUrl = integrationPropertiesDTO.getDeltaFailOverUrl();
		this.mpxUrl = integrationPropertiesDTO.getMpxUrl();
		this.mpxFailOverUrl = integrationPropertiesDTO.getMpxFailOverUrl();
		this.offsetTime = integrationPropertiesDTO.getOffsetTime();
		this.deltaUrlFlag = integrationPropertiesDTO.getDeltaUrlFlag();
		this.mpxUrlFlag = integrationPropertiesDTO.getMpxUrlFlag();
		this.failOverOffsetTime = integrationPropertiesDTO.getFailOverOffsetTime();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdminADGroup() {
		return adminADGroup;
	}

	public void setAdminADGroup(String adminADGroup) {
		this.adminADGroup = adminADGroup;
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}

	public String getMuleSoft() {
		return muleSoft;
	}

	public void setMuleSoft(String muleSoft) {
		this.muleSoft = muleSoft;
	}

	public String getDeltaUrl() {
		return deltaUrl;
	}

	public void setDeltaUrl(String deltaUrl) {
		this.deltaUrl = deltaUrl;
	}

	public String getDeltaFailOverUrl() {
		return deltaFailOverUrl;
	}

	public void setDeltaFailOverUrl(String deltaFailOverUrl) {
		this.deltaFailOverUrl = deltaFailOverUrl;
	}

	public String getMpxUrl() {
		return mpxUrl;
	}

	public void setMpxUrl(String mpxUrl) {
		this.mpxUrl = mpxUrl;
	}

	public String getMpxFailOverUrl() {
		return mpxFailOverUrl;
	}

	public void setMpxFailOverUrl(String mpxFailOverUrl) {
		this.mpxFailOverUrl = mpxFailOverUrl;
	}

	public long getOffsetTime() {
		return offsetTime;
	}

	public void setOffsetTime(long offsetTime) {
		this.offsetTime = offsetTime;
	}

	public void setFailOverOffsetTime(long failOverOffsetTime) {
		this.failOverOffsetTime = failOverOffsetTime;
	}

	public Boolean getDeltaUrlFlag() {
		return deltaUrlFlag;
	}

	public void setDeltaUrlFlag(Boolean deltaUrlFlag) {
		this.deltaUrlFlag = deltaUrlFlag;
	}

	public Boolean getMpxUrlFlag() {
		return mpxUrlFlag;
	}

	public void setMpxUrlFlag(Boolean mpxUrlFlag) {
		this.mpxUrlFlag = mpxUrlFlag;
	}

	public long getFailOverOffsetTime() {
		return failOverOffsetTime;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			LOGGER.error("Exception in method:{}", e.getMessage());
		}
		return str;
	}

}
