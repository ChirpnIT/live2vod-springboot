package com.chirpn.live2vod.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.audit.Auditable;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.common.enums.ExtractionType;
import com.chirpn.live2vod.common.validation.LocalDateDeserializer;
import com.chirpn.live2vod.common.validation.LocalDateSerializer;
import com.chirpn.live2vod.common.validation.LocalTimeDeserializer;
import com.chirpn.live2vod.common.validation.LocalTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * @author awalunj
 * 
 */
@Entity
@Table(name = "asset")
@JsonInclude(Include.NON_EMPTY)
public class Asset extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = -6104655756428860978L;

	private static final Logger logger = LoggerFactory.getLogger(Asset.class);

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "asset_no")
	private String assetNo;

	@Column(name = "program_name")
	private String programName;

	@Column(name = "pilat_id")
	private String pilatId;

	@Column(name = "external_id")
	private String externalId;

	@Column(name = "mpx_id")
	private String mpxId;

	@Column(name = "date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate date;

	@Column(name = "end_date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@Column(name = "start_time")
	private LocalTime startTime;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@Column(name = "end_time")
	private LocalTime endTime;

	@Column(name = "start_frame")
	private Integer startFrame;

	@Column(name = "end_frame")
	private Integer endFrame;

	@Column(name = "duration")
	private String duration;

	@Column(name = "status")
	private AssetStatus status = AssetStatus.SCHEDULED;

	@Column(name = "extraction_type")
	private ExtractionType extractionType = ExtractionType.MANUAL_EXTARCT;

	@Column(name = "auto_publish")
	private Boolean autoPublish = false;

	@Column(name = "ad_removal")
	private Boolean adRemoval = false;

	@Column(name = "repeat_type")
	private String repeat;

	@Column(name = "repeat_end_date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate repeatEndDate;

	@ManyToOne
	@JoinColumn(name = "channel_id")
	private Channel channel;

	@Column(name = "geo_targeting")
	private String geoTargeting;

	@JsonIgnore
	@Column(name = "meta_tags")
	@ElementCollection(targetClass = String.class)
	private List<String> metaTags;

	@JsonIgnore
	@OneToMany(mappedBy = "asset", targetEntity = AdSlot.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<AdSlot> adSlots = new ArrayList<>();

	@Column(name = "url")
	private String url;

	@Column(name = "is_enabled")
	private Boolean enabled = Boolean.TRUE;

	@Column(name = "deleted")
	private Boolean deleted = false;

	@Column(name = "delta_url_flag")
	private Boolean deltaUrlFlag = false;

	@Column(name = "mpx_url_flag")
	private Boolean mpxUrlFlag = false;

	@Column(name = "mp4_url")
	private String mp4Url;
	
	@Column(name = "start_date_time")
	private String startDateTime;
	
	@Column(name = "end_date_time")
	private String endDateTime;
	
	@Column(name = "unique_hls_name")
	private String uniqueHlsName;

	@Column(name="fail_attempt")
	private Integer failAttempt = 0;
	
	@Column(name="mp4_video_duration")
	private String mp4VideoDuration;
	
	@Column(name= "exporting_xml_file_flag")
	private Boolean exportingXmlFileFlag = false;

	public Boolean getExportingXmlFileFlag() {
		return exportingXmlFileFlag;
	}

	public void setExportingXmlFileFlag(Boolean exportingXmlFileFlag) {
		this.exportingXmlFileFlag = exportingXmlFileFlag;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getPilatId() {
		return pilatId;
	}

	public void setPilatId(String pilatId) {
		this.pilatId = pilatId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getMpxId() {
		return mpxId;
	}

	public void setMpxId(String mpxId) {
		this.mpxId = mpxId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getAssetNo() {
		return assetNo;
	}

	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}

	public List<AdSlot> getAdSlots() {
		return adSlots;
	}

	public void setAdSlots(List<AdSlot> adSlots) {
		this.adSlots = adSlots;
	}

	public AssetStatus getStatus() {
		return status;
	}

	public void setStatus(AssetStatus status) {
		this.status = status;
	}

	public ExtractionType getExtractionType() {
		return extractionType;
	}

	public void setExtractionType(ExtractionType extractionType) {
		this.extractionType = extractionType;
	}

	public Boolean getAutoPublish() {
		return autoPublish;
	}

	public void setAutoPublish(Boolean autoPublish) {
		this.autoPublish = autoPublish;
	}

	public Boolean getAdRemoval() {
		return adRemoval;
	}

	public void setAdRemoval(Boolean adRemoval) {
		this.adRemoval = adRemoval;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public LocalDate getRepeatEndDate() {
		return repeatEndDate;
	}

	public void setRepeatEndDate(LocalDate repeatEndDate) {
		this.repeatEndDate = repeatEndDate;
	}

	public String getGeoTargeting() {
		return geoTargeting;
	}

	public void setGeoTargeting(String geoTargeting) {
		this.geoTargeting = geoTargeting;
	}

	public List<String> getMetaTags() {
		return metaTags;
	}

	public void setMetaTags(List<String> metaTags) {
		this.metaTags = metaTags;
	}

	public Integer getStartFrame() {
		return startFrame;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public void setStartFrame(Integer startFrame) {
		this.startFrame = startFrame;
	}

	public Integer getEndFrame() {
		return endFrame;
	}

	public void setEndFrame(Integer endFrame) {
		this.endFrame = endFrame;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Boolean getDeltaUrlFlag() {
		return deltaUrlFlag;
	}

	public void setDeltaUrlFlag(Boolean deltaUrlFlag) {
		this.deltaUrlFlag = deltaUrlFlag;
	}

	public Boolean getMpxUrlFlag() {
		return mpxUrlFlag;
	}

	public void setMpxUrlFlag(Boolean mpxUrlFlag) {
		this.mpxUrlFlag = mpxUrlFlag;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getMp4Url() {
		return mp4Url;
	}

	public void setMp4Url(String mp4Url) {
		this.mp4Url = mp4Url;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}
	
	public Integer getFailAttempt() {
		return failAttempt;
	}

	public void setFailAttempt(Integer failAttempt) {
		this.failAttempt = failAttempt;
	}

	public String getUniqueHlsName() {
		return uniqueHlsName;
	}

	public void setUniqueHlsName(String uniqueHlsName) {
		this.uniqueHlsName = uniqueHlsName;
	}

	public String getMp4VideoDuration() {
		return mp4VideoDuration;
	}

	public void setMp4VideoDuration(String mp4VideoDuration) {
		this.mp4VideoDuration = mp4VideoDuration;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when asset object created:{}", e.getMessage());
		}
		return str;
	}

}
