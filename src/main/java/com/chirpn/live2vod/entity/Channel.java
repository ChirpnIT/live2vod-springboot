package com.chirpn.live2vod.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Entity
@Table(name = "channel")
public class Channel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(Channel.class);

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "channel_No")
	private String channelNo;

	@Column(name = "deluxe_id")
	private String deluxeId;

	@Column(name = "name")
	private String name;

	@Column(name = "stream_id")
	private String streamId;

	@JsonIgnore
	@OneToMany(mappedBy = "channel", targetEntity = Asset.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Asset> assets = new HashSet<>();

	@JsonIgnore
	@OneToMany(mappedBy = "channel", targetEntity = Group.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Group> groups = new HashSet<>();
	
	@Column(name = "delta_fail_over_stream_id")
	private String deltaFailOverStreamId;
	
	@Column(name = "duration")
	private String duration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeluxeId() {
		return deluxeId;
	}

	public void setDeluxeId(String deluxeId) {
		this.deluxeId = deluxeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public Set<Asset> getAssets() {
		return assets;
	}

	public void setAssets(Set<Asset> assets) {
		this.assets = assets;
	}

	public String getChannelNo() {
		return channelNo;
	}

	public void setChannelNo(String channelNo) {
		this.channelNo = channelNo;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public String getDeltaFailOverStreamId() {
		return deltaFailOverStreamId;
	}

	public void setDeltaFailOverStreamId(String deltaFailOverStreamId) {
		this.deltaFailOverStreamId = deltaFailOverStreamId;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when channel object created:{}", e.getMessage());
		}
		return str;
	}

}
