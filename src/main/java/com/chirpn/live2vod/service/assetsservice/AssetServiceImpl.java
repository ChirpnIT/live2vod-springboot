package com.chirpn.live2vod.service.assetsservice;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.constants.Constants;
import com.chirpn.live2vod.common.constants.Messages;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.AdSlot;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.AdSlotDTO;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.mpx.Media;
import com.chirpn.live2vod.repository.AdSlotRepository;
import com.chirpn.live2vod.repository.AssetRepository;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;
import com.chirpn.live2vod.service.mpx.MPXService;
import com.chirpn.live2vod.service.muleintegrationservice.MuleIntegrationService;
import com.chirpn.live2vod.service.scheduler.Publisher;
import com.chirpn.live2vod.util.CommonUtils;

/**
 * @author awalunj
 *
 */
@Service
@Transactional(noRollbackFor = {Live2VodException.class, Exception.class})
public class AssetServiceImpl implements AssetService {

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private ChannelRepository channelRepository;

	@Autowired
	private AdSlotRepository adSlotRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private Publisher publisher;

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;
	
	@Autowired
	private MPXService mpxService;
	
	@Autowired
	private MuleIntegrationService muleIntegrationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AssetServiceImpl.class);

	@Override
	public Response<Page<AssetDTO>> getAssetsList(Optional<String> optionalChannelNo, Optional<LocalDate> optionalDate,
			Optional<AssetStatus> optionalAssetStatus, Pageable pageable) throws Exception {
		Response<Page<AssetDTO>> response = new Response<>();

		List<AssetDTO> assestDTOList = new ArrayList<>();
		Page assetsPage = null;

		// get 3 day dates
		List<LocalDate> threeayDates = getThreeDayDates();

		if (optionalChannelNo.isPresent() && optionalDate.isPresent() && optionalAssetStatus.isPresent()) {
			assetsPage = assetRepository.findByChannelChannelNoAndDateAndStatus(optionalChannelNo.get(),
					optionalDate.get(), optionalAssetStatus.get(), pageable);
		} else if (optionalChannelNo.isPresent() && optionalDate.isPresent()) {
			assetsPage = assetRepository.findByChannelChannelNoAndDate(optionalChannelNo.get(), optionalDate.get(),
					pageable);
		} else if (optionalChannelNo.isPresent() && optionalAssetStatus.isPresent()) {
			assetsPage = assetRepository.findByChannelChannelNoAndStatus(optionalChannelNo.get(),
					optionalAssetStatus.get(), pageable);
		} else if (optionalDate.isPresent() && optionalAssetStatus.isPresent()) {
			assetsPage = assetRepository.findByDateAndStatus(optionalDate.get(), optionalAssetStatus.get(), pageable);
		} else if (optionalChannelNo.isPresent()) {
			// if dates not provided, return data for tomorrow, today and yesterday
			assetsPage = assetRepository.findByChannelChannelNoAndDateIn(optionalChannelNo.get(), threeayDates,
					pageable);
		} else if (optionalDate.isPresent()) {
			assetsPage = assetRepository.findByDate(optionalDate.get(), pageable);
		} else if (optionalAssetStatus.isPresent()) {
			// if dates not provided, return data for tomorrow, today and yesterday
			assetsPage = assetRepository.findByStatusAndDateIn(optionalAssetStatus.get(), threeayDates, pageable);
		} else {
			// if dates not provided, return data for tomorrow, today and yesterday
			assetsPage = assetRepository.findByDateIn(threeayDates, pageable);
		}

		List<Asset> assestList = assetsPage.getContent();
		assestList.forEach(asset -> {
			AssetDTO assetDTO = convertToDto(asset);
			assetDTO.setDuration(CommonUtils.isNotNullOrEmpty(asset.getMp4VideoDuration())
					? CommonUtils.TimeConversion(asset.getMp4VideoDuration()) : asset.getDuration());
			isAssetEditable(assetDTO);
			assestDTOList.add(assetDTO);
		});
		if (assestDTOList.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT);
		} else {
			Page assetDTOsPage = new PageImpl(assestDTOList, pageable, assetsPage.getTotalElements());
			response.setPage(assetDTOsPage);
		}
		return response;
	}

	@Override
	public Response<AssetDTO> getAssetByAssetNo(String assetNo) throws Exception {
		Response<AssetDTO> response = new Response<>();
		Asset asset = assetRepository.findByAssetNo(assetNo);
		AssetDTO assetDTO = null;
		if (asset != null) {
			assetDTO = convertToDto(asset);
			isAssetEditable(assetDTO);
		}
		if (assetDTO == null) {
			response.setStatus(HttpStatus.NO_CONTENT);
			response.setStatusMessage(Messages.ASSETS_NOT_FOUND);
			return response;
		}
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		response.setData(assetDTO);
		return response;
	}

	private void isAssetEditable(AssetDTO assetDTO) {
		LocalDate date = assetDTO.getDate();
		LocalDate localDateNow = LocalDate.now();
		long second = calculateSecond(assetDTO.getDate(), assetDTO.getStartTime());
		if ((assetDTO.getStatus() == AssetStatus.SCHEDULED
				&& (date.isAfter(localDateNow) || date.isEqual(localDateNow)))
				|| (assetDTO.getStatus() == AssetStatus.READY_TO_PUBLISH && 1440 >= second)) {
			assetDTO.setEditable(Boolean.TRUE);
		} else {
			assetDTO.setEditable(Boolean.FALSE);
		}

	}

	public long calculateSecond(LocalDate date, LocalTime time) {
		// calculate seconds from asset end date and end time.
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CommonUtils.DATE_FORMAT);
		LocalDateTime currentDateTime = LocalDateTime.now();
		LocalDateTime assetStartTime = CommonUtils.convertStringToLocalDateTime(
				CommonUtils.convertLocalDateToSpecifiedString(date, formatter),
				time.toString());
		Duration dur = Duration.between(currentDateTime, assetStartTime);
		return TimeUnit.MILLISECONDS.toSeconds(dur.toMillis());
	}

	public AssetDTO convertToDto(Asset asset) {
		return modelMapper.map(asset, AssetDTO.class);
	}

	public Asset convertToEntity(AssetDTO assetDTO) throws Exception {
		Asset asset = new Asset();
		Asset dbAsset = assetDTO.getAssetNo() == null ? null : assetRepository.findByAssetNo(assetDTO.getAssetNo());
		BeanUtils.copyProperties(assetDTO, asset, new String[] { "status" });
		Channel channel = assetDTO.getChannel() != null
				? channelRepository.findByChannelNo(assetDTO.getChannel().getChannelNo())
				: null;
		if (channel == null)
			throw new Live2VodException("Selected Channel not found");
		if (dbAsset != null) {
			asset.setId(dbAsset.getId());
			adSlotRepository.deleteByAsset(asset);
		} else {
			asset.setAssetNo(CommonUtils.generateAssetNumber());
		}
		List<AdSlot> adSlots = new ArrayList<>();
		for (AdSlotDTO adSlotDTO : assetDTO.getAdSlots()) {
			AdSlot adSlot = modelMapper.map(adSlotDTO, AdSlot.class);
			adSlot.setAsset(asset);
			AdSlot dbAdSlot = adSlotDTO.getAdSlotNo() == null ? null
					: adSlotRepository.findByAdSlotNo(adSlotDTO.getAdSlotNo());
			if (dbAdSlot != null) {
				adSlot.setId(dbAdSlot.getId());
			} else {
				adSlot.setAdSlotNo(CommonUtils.generateAdSlotNumber());
			}
			adSlots.add(adSlot);
		}
		asset.setChannel(channel);
		asset.setAdSlots(adSlots);
		return asset;
	}

	@Override
	public Response<AssetDTO> saveAsset(AssetDTO assetDTO, String streamNo, boolean proceed) throws Exception {
		Response<AssetDTO> response = new Response<>();
		//updateKey key will true every time when we update schedule/ready to publish asset Or .
		if (assetDTO == null) {
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
			response.setStatusMessage(Messages.ASSET_JSON_SHOULD_NOT_BE_NULL);
			return response;
		}
		Asset dbAsset = assetDTO.getAssetNo() == null ? null : assetRepository.findByAssetNo(assetDTO.getAssetNo());
		isAbleToUpdateRequest(assetDTO);
		
		if (!proceed && assetDTO.isUpdateKey()) {
			LocalDate aMonthOldDate = LocalDate.now().minusMonths(1);
			if (assetDTO.getPilatId() != null && !assetDTO.getPilatId().isEmpty()
					&& assetRepository.existsByPilatIdAndDateGreaterThan(assetDTO.getPilatId(), aMonthOldDate) ) {
				response.setStatus(HttpStatus.BAD_REQUEST);
				response.setStatusMessage(Messages.ASSET_ALREADY_EXISTS_WITH_PILAT_ID);
				return response;

			}
			if (assetDTO.getMpxId() != null && !assetDTO.getMpxId().isEmpty()
					&& assetRepository.existsByMpxIdAndDateGreaterThan(assetDTO.getMpxId(), aMonthOldDate)) {
				response.setStatus(HttpStatus.BAD_REQUEST);
				response.setStatusMessage(Messages.ASSET_ALREADY_EXISTS_WITH_MPX_ID);
				return response;

			}
			
		}
		
		if (!assetDTO.isUpdateKey() && dbAsset != null) {
			long second = calculateSecond(assetDTO.getEndDate(), assetDTO.getEndTime());
			LOGGER.info("current time minus asset time for update schedule:{}", second);
			if (240 >= second) {
			//If old scheduled start time is very close to current time, the user shouldn’t be allowed to update it.
			LOGGER.info("cant update status because, asset time is nearest to schedule start time:{}, "
					+ "pilat id/Mpx id: {}", second, CommonUtils.isNotNullOrEmpty(assetDTO.getPilatId()) ? assetDTO.getPilatId() : assetDTO.getMpxId()); 
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
			response.setStatusMessage(Messages.ASSET_IS_NEAR_TO_FOUR_MINUTE_BOUNDARY);
			return response;
			}
			
		}
		
		//this is used when asset with same pilat id exist with different asset in the system then it will override.
		if (proceed && CommonUtils.isNullOrEmpty(assetDTO.getAssetNo())) {
			dbAsset = assetRepository.findByPilatId(assetDTO.getPilatId());
			if (dbAsset!=null) {
				 assetDTO.setAssetNo(dbAsset.getAssetNo());
			}
		}

		Asset asset = null;
		asset = convertToEntity(assetDTO);
		assetRepository.save(asset);
		response.setData(convertToDto(asset));
		return response;
	}
	
	public void isAbleToUpdateRequest(AssetDTO assetDTO) throws Exception {
		if (assetDTO.getPilatId() != null && !assetDTO.getPilatId().isEmpty()) {
			List<Asset> asset = assetRepository.existByPilatIdAndStatus(assetDTO.getPilatId());
			 if (asset != null && !asset.isEmpty()) {
					throw new Live2VodException("Same pilat ID/MPX ID exist in the system. Either wait for the other job finish or use a different ID");

			 }
		}
	    if(assetDTO.getMpxId() != null && !assetDTO.getMpxId().isEmpty()) {
	    	List<Asset> asset = assetRepository.existByMpxIdAndStatus(assetDTO.getMpxId());
	    	 if (asset != null && !asset.isEmpty()) {
					throw new Live2VodException("Same pilat ID/MPX ID exist in the system. Either wait for the other job finish or use a different ID");

			 }	    
	    }
	}
	

	@Override
	public Response<AssetDTO> updateAsset(AssetDTO assetDTO) throws Exception {
		Response<AssetDTO> response = new Response<>();
		Asset asset = null;
		if (assetDTO != null)
			asset = convertToEntity(assetDTO);
		assetRepository.save(asset);
		response.setData(convertToDto(asset));
		return response;
	}

	@Override
	public Response<AssetDTO> cancelScheduledAsset(String assetNo) throws Exception {
		Response<AssetDTO> response = new Response<>();
		if (CommonUtils.isNullOrEmpty(assetNo)) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NO_IS_REQUIRED);
			return response;
		}
		Asset asset = assetRepository.findByAssetNo(assetNo);
		if (asset == null) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NOT_FOUND_BASED_ON_ASSETS_ID);
			return response;
		}
		if (isAbleToCancled(asset.getStatus())) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSET_CAN_NOT_BE_CANCELLED);
			return response;
		}
		asset.setStatus(AssetStatus.CANCELLED);
		assetRepository.save(asset);
		AssetDTO assetDTO = convertToDto(asset);
		response.setStatus(HttpStatus.OK);
		response.setData(assetDTO);
		return response;
	}
	
	public boolean isAbleToCancled(AssetStatus assetStatus) {
		boolean permitted = false;
		switch (assetStatus) {
		case READY_TO_PUBLISH:
			permitted = true;
			break;
		case PUBLISHED:
			permitted = true;
			break;
		case SUBMITTED_FOR_EXTRACTION:
			permitted = true;
			break;
		default:
			break;
		}
		return permitted;

	}

	@Override
	public Response<AssetDTO> publishAsset(String assetNo) throws Exception {
		Response<AssetDTO> response = new Response<>();
		if (CommonUtils.isNullOrEmpty(assetNo)) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NO_IS_REQUIRED);
			return response;
		}
		Asset asset = assetRepository.findByAssetNo(assetNo);
		if (asset == null) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NOT_FOUND_BASED_ON_ASSETS_ID);
			return response;
		}
		if (asset.getDeleted()) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSET_IS_DELETED_FROM_DELTA_SERVER);
			return response;
		}
		if (!AssetStatus.READY_TO_PUBLISH.equals(asset.getStatus()) && !AssetStatus.FAILED_TO_PUBLISH.equals(asset.getStatus())) {
			LOGGER.info("Can not pusblish an asset:{}  ,Not in ready to publish state.", asset.getAssetNo());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NOT_IN_READY_TO_PUBLISH_STATE);
			return response;
		}
		
		if (CommonUtils.isNullOrEmpty(asset.getGeoTargeting())) {
			throw new Live2VodException(Messages.GEO_TRAGETING_SHOULD_NOT_BE_NULL);
		}
		asset.setStatus(AssetStatus.SUBMITTED_FOR_PUBLICATION);
		assetRepository.save(asset);
		try {
			publisher.publish(asset);
			response.setStatus(HttpStatus.OK);
			response.setStatusMessage(Messages.ASSETS_IS_BEING_PUBLISHED);
			LOGGER.info("Publish job has be added for ass");
			
		} catch (Exception e) {
			LOGGER.info("Unable to add asynchronous publish job for assetNo:{} , Exception: {}", asset.getAssetNo(), e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(Messages.ASSETS_NOT_IN_READY_TO_PUBLISH_STATE);
		}

		AssetDTO assetDTO = convertToDto(asset);
		
		response.setData(assetDTO);
		
		return response;
	}

	@Override
	public IntegrationProperties validateMpxProperties() throws Exception {
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		String mpxUrl = null;
		
		if (property == null || CommonUtils.isNullOrEmpty(property.getMuleSoft()) || property.getMpxUrlFlag() == null) {
			throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
		}

		if (property.getMpxUrlFlag()) {
			if (CommonUtils.isNullOrEmpty(property.getMpxFailOverUrl()))
				throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
			mpxUrl = property.getMpxFailOverUrl();
		} else {
			if (CommonUtils.isNullOrEmpty(property.getMpxUrl()))
				throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
			mpxUrl = property.getMpxUrl();
		}

		if (CommonUtils.isNullOrEmpty(mpxUrl)) {
			throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
		}
		return property;
	}

	@Override
	public Response<String> deleteAssets(String assetNo) throws Exception {
		Response<String> response = new Response<>();
		if (CommonUtils.isNullOrEmpty(assetNo)) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NO_IS_REQUIRED);
			return response;
		}

		Asset asset = assetRepository.findByAssetNo(assetNo);
		if (asset == null) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.ASSETS_NOT_FOUND_BASED_ON_ASSETS_ID);
			return response;
		}
		assetRepository.deleteByAssetNo(assetNo);
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(Messages.ASSETS_DELETED);
		return response;
	}

	private List<LocalDate> getThreeDayDates() {
		List<LocalDate> threeDayDates = new ArrayList<>();
		LocalDate todayLocalDate = LocalDate.now();
		LocalDate tomorrowLocalDate = LocalDate.now().plusDays(1);
		LocalDate yesterdayLocalDate = LocalDate.now().minusDays(1);
		threeDayDates.add(tomorrowLocalDate);
		threeDayDates.add(todayLocalDate);
		threeDayDates.add(yesterdayLocalDate);
		return threeDayDates;
	}

	@Override
	public void ExportAsset(Asset asset) throws Exception {
		Media media = new Media();
		mpxService.mapAssetToMedia(asset, media);
		IntegrationProperties property = validateMpxProperties();
		String mpxUrl = property.getMpxUrlFlag() ? property.getMpxFailOverUrl() : property.getMpxUrl();
		LOGGER.info("asset no, media, mpx url : {}{}{}", asset.getAssetNo(), media, mpxUrl);
		try {
			String publishResponse = muleIntegrationService.sendPublishRequestToMule(media, asset, mpxUrl, property.getMuleSoft());
			
			if (CommonUtils.isNullOrEmpty(publishResponse) || !publishResponse.equalsIgnoreCase(Constants.SUCCESS)) {
				LOGGER.error("Failed to publish the asset assetNo: {} and publish response: {}", asset.getAssetNo(), publishResponse);
				AssetStatus status = asset.getFailAttempt() < 3 ? AssetStatus.SUBMITTED_FOR_PUBLICATION : AssetStatus.FAILED_TO_PUBLISH;
				asset.setStatus(status);
				asset.setFailAttempt(asset.getFailAttempt()+1);
			} else {
				LOGGER.info("Published Done for the asset, asset no: {} and publish response:{}", asset.getAssetNo(), publishResponse);
				asset.setStatus(AssetStatus.PUBLISHED);
			}
		} catch (Exception e) {
			LOGGER.error("Failed to publish the asset, asset no:{} and error message:{} ", asset.getAssetNo(), e.getMessage());
			AssetStatus status = asset.getFailAttempt() < 3 ? AssetStatus.SUBMITTED_FOR_PUBLICATION : AssetStatus.FAILED_TO_PUBLISH;
			asset.setStatus(status);
			asset.setFailAttempt(asset.getFailAttempt()+1);
		}
		LOGGER.info("Asset Export status : {} and assets no :{} and Fail Attempt : {} ",asset.getStatus(),asset.getAssetNo(),asset.getFailAttempt());
		assetRepository.save(asset);
	}

}
