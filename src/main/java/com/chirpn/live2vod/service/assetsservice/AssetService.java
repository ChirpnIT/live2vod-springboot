package com.chirpn.live2vod.service.assetsservice;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.AssetDTO;

/**
 * @author awalunj
 *
 */
public interface AssetService {
	
	Response<Page<AssetDTO>> getAssetsList(Optional<String> optionalChannelNo, Optional<LocalDate> optionalDate, Optional<AssetStatus> optionalStatus, Pageable pageable) throws Exception;
	
	Response<AssetDTO> getAssetByAssetNo(String assetNo) throws Exception;
	
	Response<AssetDTO> saveAsset(AssetDTO assetDTO, String streamNo, boolean preceed) throws Exception;

	Response<AssetDTO> updateAsset(AssetDTO assetDTO) throws Exception;
	
	Response<AssetDTO> cancelScheduledAsset(String assetNo) throws Exception;
	
	Response<AssetDTO> publishAsset(String assetNo) throws Exception;
	
	Response<String> deleteAssets(String streamId) throws Exception;
	
	Asset convertToEntity(AssetDTO assetDTO) throws Exception;
	
	AssetDTO convertToDto(Asset asset);
	
	IntegrationProperties validateMpxProperties() throws Exception;
	
	void ExportAsset(Asset asset) throws Exception;
}
