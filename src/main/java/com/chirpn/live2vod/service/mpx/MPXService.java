package com.chirpn.live2vod.service.mpx;

import java.text.ParseException;
import java.util.List;

import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.mpx.Media;

public interface MPXService {

	List<Media> getMediaMetadata();

	Media getMediaMetadataByPilatId(String pilatId);

	String updateMediaMetadata(Media media);

	void mapAssetToMedia(Asset asset, Media media) throws ParseException;

	void mapAssetToMedia(AssetDTO assetDTO, Media media);

}
