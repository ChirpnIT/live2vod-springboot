package com.chirpn.live2vod.service.mpx;

import java.text.ParseException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.chirpn.live2vod.entity.AdSlot;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.model.AdSlotDTO;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.mpx.Media;
import com.chirpn.live2vod.model.mpx.Plmediachapter;
import com.chirpn.live2vod.util.CommonUtils;

@Service
public class MPXServiceImpl implements MPXService {

	

	@Override
	public List<Media> getMediaMetadata() {
		return new ArrayList<>();
	}

	@Override
	public Media getMediaMetadataByPilatId(String pilatId) {
		return null;
	}

	@Override
	public String updateMediaMetadata(Media media) {
		return null;
	}

	@Override
	public void mapAssetToMedia(AssetDTO asset, Media media) {

		LocalTime programStartTime = asset.getStartTime();
		LocalTime programEndTime = asset.getEndTime();

		List<AdSlotDTO> adSlots = asset.getAdSlots();

		List<Plmediachapter> chapters = new ArrayList<>();
		Plmediachapter chapter = null;
		Long adSlotStartTime = null;

		if (adSlots != null && !adSlots.isEmpty()) {
			for (int i = 0; i < adSlots.size(); i++) {
				AdSlotDTO adSlotDTO = adSlots.get(i);
				adSlotStartTime = programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS);
				if (chapter == null) {
					chapter = new Plmediachapter();
					chapter.setPlmedia$startTime(0l);
					chapter.setPlmedia$endTime(adSlotStartTime);
					chapters.add(chapter);
				} else {
					chapter.setPlmedia$endTime(programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS));
					chapters.add(chapter);
				}
				chapter = new Plmediachapter();
				chapter.setPlmedia$startTime((adSlotDTO.getStartTime().until(adSlotDTO.getEndTime(), ChronoUnit.SECONDS)) + programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS));
			}
			if(chapter != null)
				chapter.setPlmedia$endTime(programStartTime.until(programEndTime, ChronoUnit.SECONDS));
			chapters.add(chapter);
		}
		media.setPlmedia$chapters(chapters);
		media.setPl1$pilatId(asset.getPilatId());
	}

	@Override
	public void mapAssetToMedia(Asset asset, Media media) throws ParseException {

		LocalTime programStartTime = asset.getStartTime();
		LocalTime programEndTime = asset.getEndTime();

		List<AdSlot> adSlots = asset.getAdSlots();

		List<Plmediachapter> chapters = new ArrayList<>();
		Plmediachapter chapter = null;
		Long adSlotStartTime = null;

		if (adSlots != null && !adSlots.isEmpty()) {
			for (int i = 0; i < adSlots.size(); i++) {
				AdSlot adSlotDTO = adSlots.get(i);
				adSlotStartTime = programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS);
				if (chapter == null) {
					chapter = new Plmediachapter();
					chapter.setPlmedia$startTime(0l);
					chapter.setPlmedia$endTime(adSlotStartTime);
					chapters.add(chapter);
				} else {
					chapter.setPlmedia$endTime(programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS));
					chapters.add(chapter);
				}
				chapter = new Plmediachapter();
				chapter.setPlmedia$startTime((adSlotDTO.getStartTime().until(adSlotDTO.getEndTime(), ChronoUnit.SECONDS)) + programStartTime.until(adSlotDTO.getStartTime(), ChronoUnit.SECONDS));
			}
			if(chapter != null)
				chapter.setPlmedia$endTime(programStartTime.until(programEndTime, ChronoUnit.SECONDS));
			chapters.add(chapter);
		}
		media.setPlmedia$chapters(chapters);
		media.setPl1$pilatId(CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId());
	}

}
