package com.chirpn.live2vod.service.delta;

import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.delta.Content;
import com.chirpn.live2vod.model.delta.Filter;

public interface DeltaAPIHelperService {

	Content getCreatFilterRequest(AssetDTO assetDTO, String uniqueHlsName);

	Filter getCopyFilter();

	Filter getHSLPackageFilter();

}
