package com.chirpn.live2vod.service.delta;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class JaxbObjectToXMLService {

	private static final Logger LOGGER = Logger.getLogger(JaxbObjectToXMLService.class);
	
	public String getXMLString(Object object, Class clazz) {
		StringWriter sw = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(object, sw);
		} catch (JAXBException e) {
			LOGGER.error("Exception: " + e.getMessage());
		}
		return sw.toString();
	}

}
