package com.chirpn.live2vod.service.delta;

import org.springframework.stereotype.Service;

import com.chirpn.live2vod.common.constants.Constants;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.delta.Content;
import com.chirpn.live2vod.model.delta.Filter;
import com.chirpn.live2vod.model.delta.Filter_settings;
import com.chirpn.live2vod.model.delta.Filters;
import com.chirpn.live2vod.model.delta.Network_id;
import com.chirpn.live2vod.util.CommonUtils;

@Service
public class DeltaAPIHelperServiceImpl implements DeltaAPIHelperService {

	public Content getCreatFilterRequest(AssetDTO assetDTO, String uniqueHlsName) {
		Content content = new Content();
		Filter[] filterArray = new Filter[2];
		String faVodName = "FA_VOD_1";
		Filter_settings filterSettings = new Filter_settings(CommonUtils.getStartTime(assetDTO.getDate(), assetDTO.getStartTime().toString()), 
				CommonUtils.getEndTime(assetDTO.getDate(), assetDTO.getEndTime().toString()), Constants.FALSE, Constants.FALSE, Constants.FALSE, assetDTO.getStartFrame() + "", assetDTO.getEndFrame() + "", "25", "1");
		Filter filter = new Filter("", Constants.FALSE, "live_to_vod", "", "m3u8", faVodName, "FA_VOD", filterSettings, faVodName);
		filterArray[0] = filter;

		Network_id networkId1 = new Network_id("true");
		Filter_settings filterSettings1 = new Filter_settings(Constants.FALSE, Constants.FALSE, Constants.FALSE, "6", "600", "VOD", networkId1, Constants.FALSE, "all", "none", "30", Constants.FALSE, Constants.FALSE);
		Filter filter1 = new Filter(faVodName, "true", "hls_package", "", "m3u8", "hls-vod", "FA_VOD", filterSettings1, uniqueHlsName);
		filterArray[1] = filter1;

		Filters filters = new Filters();
		filters.setFilter(filterArray);

		content.setFilters(filters);

		return content;
	}

	@Override
	public Filter getCopyFilter() {
		return null;
	}

	@Override
	public Filter getHSLPackageFilter() {
		return null;
	}

}
