package com.chirpn.live2vod.service.integrationpropertiesservice;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.IntegrationPropertiesDTO;

/**
 * 
 * @author Satish ghonge
 * 
 */
public interface IntegrationPropertiesService {
	Response<IntegrationPropertiesDTO> createIntegrationProperties(IntegrationPropertiesDTO integrationProperties)
			throws Exception;

	Response<IntegrationPropertiesDTO> retrieve() throws Live2VodException;

}
