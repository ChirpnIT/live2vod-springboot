package com.chirpn.live2vod.service.integrationpropertiesservice;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.constants.Messages;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.IntegrationPropertiesDTO;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class IntegrationPropertiesServiceImpl implements IntegrationPropertiesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationPropertiesServiceImpl.class);

	@Autowired
	@Qualifier("integrationPropertiesMapper")
	private ModelMapper integrationPropertiesMapper;

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;

	@Override
	public Response<IntegrationPropertiesDTO> createIntegrationProperties(IntegrationPropertiesDTO integrationPropertiesDTO) throws Exception {
		Response<IntegrationPropertiesDTO> response = new Response<>();
		if (integrationPropertiesDTO == null) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.PROPERTIES_INTEGRATION_SHOULD_NOT_BE_NULL);
			return response;
		}

		IntegrationProperties integrationProperties = null;
		if (integrationPropertiesDTO.getId() != null) {
			integrationProperties = integrationPropertiesRepository.findOne(integrationPropertiesDTO.getId());
		}
		if (integrationProperties != null) {
			LOGGER.info("Old integration properties object:{}", integrationProperties);
			integrationPropertiesMapper.map(integrationPropertiesDTO, integrationProperties);
			integrationPropertiesRepository.save(integrationProperties);
			response.setStatus(HttpStatus.OK);
			response.setStatusMessage(Messages.INTEGRATION_PROP_UPDATE_SUCCESSFULLY);
			return response;
		} else {
			LOGGER.info("new integration properties object:{}", integrationPropertiesDTO);
			integrationProperties = new IntegrationProperties();
			integrationPropertiesMapper.map(integrationPropertiesDTO, integrationProperties);
			integrationPropertiesRepository.save(integrationProperties);
			integrationPropertiesDTO = new IntegrationPropertiesDTO(integrationProperties);
			response.setData(integrationPropertiesDTO);
			response.setStatus(HttpStatus.OK);
			response.setStatusMessage(HttpStatus.OK.name());
			return response;
		}
	}

	@Override
	public Response<IntegrationPropertiesDTO> retrieve() throws Live2VodException {
		Response<IntegrationPropertiesDTO> response = new Response<>();
		List<IntegrationProperties> integrationprop = integrationPropertiesRepository.findAllByOrderByIdDesc();
		if (integrationprop == null || integrationprop.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT);
			response.setStatusMessage(HttpStatus.NO_CONTENT.name());
			return response;
		}
		
		IntegrationPropertiesDTO integrationPropertiesDTO = new IntegrationPropertiesDTO();
		integrationPropertiesMapper.map(integrationprop.get(0), integrationPropertiesDTO);
		
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		response.setData(integrationPropertiesDTO);
		return response;
	}

}
