package com.chirpn.live2vod.service.scheduler;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.repository.AssetRepository;
import com.chirpn.live2vod.util.CommonUtils;
import com.chirpn.live2vod.util.FfmpegUtiltiy;

import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;

@Service
public class AssetSchedulerServiceImpl implements AssetSchedulerService {

	@Autowired
	private AssetRepository assetRepository;
	
	@Autowired
	private FfmpegUtiltiy ffmpegUtiltiy;

	@Value("${application.tools.ffmpeg.location}")
	private String ffmpeg_location;

	private static final Logger logger = LoggerFactory.getLogger(AssetSchedulerServiceImpl.class);

	@Override
	public List<Asset> getUpcomingScheduledAssets() {
		//LocalDateTime lastDateTime = LocalDateTime.now().minusHours(24);
		LocalDateTime currentDateTime = LocalDateTime.now();

		//LocalDate lastDate = lastDateTime.toLocalDate();

		LocalDate currentDate = currentDateTime.toLocalDate();
		LocalTime currentTime = currentDateTime.toLocalTime().withNano(0);

		//logger.info("@ Extract assets for extraction for endDate:{} currentDate:{} currentTime:{}", lastDate, currentDate, currentTime);

		List<Asset> scheduledAssetsForExtraction = null;
		try {
			scheduledAssetsForExtraction = assetRepository.findByEndDateAndEndTime(currentDate, currentTime);

			//scheduledAssetsForExtraction = assetRepository.findByStartDateStartTimeAndEndDateAndEndTime(lastDate, currentDate, currentTime);
		} catch (Exception e) {
			logger.error("Exception fetching assets by date and end time", e);
		}
		return scheduledAssetsForExtraction;
	}

	@Override
	public List<Asset> getAssetsForExport() {
		//LocalDateTime lastDateTime = LocalDateTime.now().minusHours(24);
		LocalDateTime currentDateTime = LocalDateTime.now();

		//LocalDate lastDate = lastDateTime.toLocalDate();

		LocalDate currentDate = currentDateTime.toLocalDate();
		LocalTime currentTime = currentDateTime.toLocalTime().withNano(0);

		//logger.info("@@ Export assets for extraction for lasttDate:{} currentDate:{} currentTime:{}", lastDate, currentDate, currentTime);

		List<Asset> scheduledAssetsForExtraction = null;
		try {
			scheduledAssetsForExtraction = assetRepository.findByEndDateAndEndTimeAndStatus(currentDate, currentTime);

			//scheduledAssetsForExtraction = assetRepository.findByStartDateStartTimeAndEndDateAndEndTimeAndStatus(lastDate, currentDate, currentTime);
		} catch (Exception e) {
			logger.error("Exception fetching assets by date and end time", e);
		}
		return scheduledAssetsForExtraction;
	}

	@Override
	public Integer getHlsPackageId(String mp4Url, Boolean addRemoval) {
		Integer mp4Id = Integer.valueOf(mp4Url.substring(mp4Url.lastIndexOf('/') + 1, mp4Url.lastIndexOf(".mp4")));
		return addRemoval ? mp4Id - 2 : mp4Id - 1;
	}

	@Override
	public List<Asset> getExtractingScheduledAssets() {
		List<Asset> assets = null;
		try {
			assets = assetRepository.findByStatus();
		} catch (Exception e) {
			logger.error("Exception while fetching extracting assets", e);
		}
		return assets;
	}

	@Override
	public String getMp4VideoDuration(String mp4Url) {
		String duration = "";
		try {
			duration = ffmpegUtiltiy.getVideoDuration(ffmpeg_location, mp4Url);
		} catch (Exception e) {
			logger.error("Exception while getting mp4 video duration: {} and mp4 url is:{}" + e.getMessage(), mp4Url);
			e.printStackTrace();
		}
		return duration;
	}

	@Override
	public List<Asset> getScheduledAssetsForCreatingXml() {
		List<Asset> assets = null;
		try {
			assets = assetRepository.findByStatusExportingXmlFileFlag();
		} catch (Exception e) {
			logger.error("Exception while fetching extracting assets", e);
		}
		return assets;
	}
	
	public static void main(String args[]) {
		//String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/deltatest/Out/T002_1.mp4";
		String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/out/Euro_Semi1_Live_128K.mp4";
		//String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/deltatest/Out/MTest2_1.mp4";
		//String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/deltatest/Out/MTest1_1.mp4";
		//String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/deltatest/Out/Drop/FilNONGEO_128K.mp4";
		//String url = "//sydisilonsmb/sydisilon/ProdUnits/Online/content/Elemental/deltatest/Out/Drop/S04_128K.mp4";
		FFprobe ffprobe = null;
		try {
			ffprobe = new FFprobe("C:/ffmpeg/bin/ffprobe");
			FFmpegProbeResult probeResult = ffprobe.probe(url);
			FFmpegFormat format = probeResult.getFormat();
			
			long videoDurationInLong = (long) format.duration;
			System.out.println("Main mp4 Duration:" + videoDurationInLong);
			final long dy = TimeUnit.SECONDS.toDays(videoDurationInLong);
			final long hr = TimeUnit.SECONDS.toHours(videoDurationInLong) - TimeUnit.DAYS.toHours(TimeUnit.SECONDS.toDays(videoDurationInLong));
			final long min = TimeUnit.SECONDS.toMinutes(videoDurationInLong) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(videoDurationInLong));

			final long sec = videoDurationInLong - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(videoDurationInLong));
			System.out.println("Main mp4 Duration second:" + sec);
			
			double remainingSeconds = (format.duration - videoDurationInLong) + sec;
			System.out.println("Main mp4 remainingSeconds:" + remainingSeconds);
			BigDecimal a = new BigDecimal(remainingSeconds).setScale(3, RoundingMode.CEILING);
			
			//BigDecimal a = new BigDecimal(remainingSeconds).setScale(3, BigDecimal.ROUND_HALF_UP);
			System.out.println("Main mp4 a:" + a);
			
			String secAndmillisecond = String.valueOf(a.doubleValue()).length() == 1 ? "0"+ String.valueOf(a.doubleValue()) : String.valueOf(a.doubleValue());
			System.out.println("Main mp4 secAndmillisecond:" + secAndmillisecond);
			if (CommonUtils.isNotNullOrEmpty(secAndmillisecond) && secAndmillisecond.contains("."))
			{
				String[] tokens = secAndmillisecond.split("\\.");
				long seconds = Long.parseLong(tokens[0]);
			    long milliseconds = Long.parseLong(tokens[1]);
			    secAndmillisecond = String.format("%02d.%02d", seconds, milliseconds);
			    System.out.println("Formatted second and milliseconds:" + secAndmillisecond);
			}
			if (dy == 0) {
				System.out.println("hrs:"+hr +"min:" +min +"sec:"+secAndmillisecond);  
			} else {
				System.out.println("day" +dy +"hrs:"+hr +"min:" +min +"sec:"+secAndmillisecond);
			}
			System.out.println("mp4 video: duration: " +  format.duration);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
