package com.chirpn.live2vod.service.scheduler;

import java.util.List;

import com.chirpn.live2vod.entity.Asset;

public interface AssetSchedulerService {
	
	List<Asset> getUpcomingScheduledAssets();
	
	List<Asset> getAssetsForExport();
	
	Integer getHlsPackageId(String mp4Url, Boolean addRemoval);
	
	List<Asset> getExtractingScheduledAssets();
	
	String getMp4VideoDuration(String mp4Url) throws Exception;
	
	List<Asset> getScheduledAssetsForCreatingXml();

}
