package com.chirpn.live2vod.service.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.common.constants.Constants;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.deltapreviewstream.DeltaPreviewStreamDTO;
import com.chirpn.live2vod.repository.AssetRepository;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;
import com.chirpn.live2vod.security.AppProperties;
import com.chirpn.live2vod.service.assetsservice.AssetService;
import com.chirpn.live2vod.service.muleintegrationservice.MuleIntegrationService;
import com.chirpn.live2vod.util.CommonUtils;

@Component
@Profile("scheduler_enabled")
public class ScheduledAssetExtraction {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduledAssetExtraction.class);

	@Autowired
	private AssetSchedulerService assetSchedulerService;

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;
	
	@Autowired
	private AssetService assetService;

	@Autowired
	private MuleIntegrationService muleIntegrationService;
	
	@Autowired
	private AppProperties appProperties;
	
	@Scheduled(cron = "${live2vod.asset_published_asset_cron_time}")
	@Transactional//(noRollbackFor = { Live2VodException.class, Exception.class })
	public void assetExportJob() {
		//logger.error("@@ asset export job ");
		List<Asset> assetsSchedules = assetSchedulerService.getAssetsForExport();
		if (assetsSchedules != null && !assetsSchedules.isEmpty()) {
			assetsSchedules.forEach(asset -> {
				if ((asset.getAutoPublish() && asset.getStatus() == AssetStatus.READY_TO_PUBLISH)|| (asset.getStatus() == AssetStatus.SUBMITTED_FOR_PUBLICATION)) {//
					try {
						logger.info("Request for Export Asset : {} : Program Name : {}",asset.getAssetNo(), asset.getProgramName());
						assetService.ExportAsset(asset);
					} catch (Exception e) {
						logger.error("Exception while submitting asset {} for publication. Exception: {}", asset, e.getMessage());

					}
				}
			});
		}
	}
	
	@Scheduled(cron = "${live2vod.asset_extract_cron_time}")
	@Transactional//(noRollbackFor = { Live2VodException.class, Exception.class })
	public void assetExtractJob() {
		//logger.error("@@ asset extract job ");
		List<Asset> assetsSchedules = assetSchedulerService.getUpcomingScheduledAssets();
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		if (property != null && assetsSchedules != null && !assetsSchedules.isEmpty()) {
			//logger.error("extraction asset list:{}", assetsSchedules);
			assetsSchedules.forEach(asset -> {
				logger.info("Request for Extract Asset : {}",asset.getAssetNo());
				String deltaUrl = null;
				String streamId = null;
				Boolean deltaUrlflag = null;
				if (property.getDeltaUrlFlag()) {
					if (CommonUtils.isNotNullOrEmpty(property.getDeltaFailOverUrl())) {
						deltaUrl = property.getDeltaFailOverUrl();
						streamId = asset.getChannel().getDeltaFailOverStreamId();
						deltaUrlflag = true;
					}
				} else {
					if (CommonUtils.isNotNullOrEmpty(property.getDeltaUrl())) {
						deltaUrl = property.getDeltaUrl();
						streamId = asset.getChannel().getStreamId();
						deltaUrlflag = false;
					}
				}
				if (CommonUtils.isNotNullOrEmpty(property.getMuleSoft()) && CommonUtils.isNotNullOrEmpty(deltaUrl) && CommonUtils.isNotNullOrEmpty(streamId)) {
					asset.setStatus(AssetStatus.SUBMITTED_FOR_EXTRACTION);
					assetRepository.save(asset);
					DeltaPreviewStreamDTO deltaPreviewStreamDTO = null;
					try {
						deltaPreviewStreamDTO = muleIntegrationService.sendRequestToMuleForExtractAssets(asset, streamId, deltaUrl, property.getMuleSoft(), deltaUrlflag);
					
						if (deltaPreviewStreamDTO != null && deltaPreviewStreamDTO.getContent().getFilters() != null 
								&& !deltaPreviewStreamDTO.getContent().getFilters().isEmpty() 
								&& CommonUtils.isNotNullOrEmpty(deltaPreviewStreamDTO.getContent().getFilters().get(0).getDefaultEndpointUri())) {
							String m3u8url =muleIntegrationService.replacePortAndBasePath(deltaPreviewStreamDTO.getContent().getFilters().get(0).getDefaultEndpointUri(), deltaUrl, Constants.EXTRACT_STREAM);
							String m3u8Id = deltaPreviewStreamDTO.getContent().getFilters().get(0).getId();
							String mp4Url = muleIntegrationService.makeMp4Url(m3u8url, m3u8Id);
							asset.setUrl(m3u8url);
							asset.setMp4Url(mp4Url);
							asset.setFailAttempt(0);
							asset.setStatus(AssetStatus.EXTRACTING);
							asset.setDeltaUrlFlag(deltaUrlflag);
						 } else {
							//Set attempt for failure
							AssetStatus status = asset.getFailAttempt() < 2 ? AssetStatus.SUBMITTED_FOR_EXTRACTION : AssetStatus.FAILED_TO_EXTRACT;
							asset.setStatus(status);
							asset.setFailAttempt(asset.getFailAttempt()+1);
						}
					} catch (Exception e) {
						logger.error("Exception occured while extracting asset: {} and  Exception: {}", asset.getAssetNo(), e.getMessage());
						AssetStatus status = asset.getFailAttempt() < 2 ? AssetStatus.SUBMITTED_FOR_EXTRACTION : AssetStatus.FAILED_TO_EXTRACT;
						asset.setStatus(status);
						asset.setFailAttempt(asset.getFailAttempt()+1);
					}
					logger.info("Asset Extract status : {} and assets no :{} and Fail Attempt : {} ",asset.getStatus(),asset.getAssetNo(),asset.getFailAttempt());
					assetRepository.save(asset);
				}
			});
		}
	}
	
	@Scheduled(cron = "${live2vod.asset_mp4_video_duration_cron_time}")
	public void mp4DurationJob() {
		//logger.error("@@ mp4 duration job");
		List<Asset> assetsSchedules = assetSchedulerService.getExtractingScheduledAssets();
		if (assetsSchedules != null && !assetsSchedules.isEmpty()) {
			//logger.error("@@ mp4 duration job and asset records :{}", assetsSchedules);
			assetsSchedules.forEach(asset -> {
					try {
						//get mp4 video duration
						logger.info("Request for Get Asset MP4 Duration : {}",asset.getAssetNo());
						String pilatOrMpxId = CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId();
						String mp4Url = appProperties.getMountFolderPath()+pilatOrMpxId+"_1.mp4";
						logger.info("Complete url to get mp4 duration: {}", mp4Url);
 						//String mp4duration = assetSchedulerService.getMp4VideoDuration(asset.getMp4Url());
						String mp4duration = assetSchedulerService.getMp4VideoDuration(mp4Url);
						logger.info("Mp4 duration : {} ", mp4duration);
						if (mp4duration.equalsIgnoreCase("12:36:-22:-808") || mp4duration.equalsIgnoreCase("12:36:-22") || CommonUtils.isNullOrEmpty(mp4duration)) {
							logger.info("Fail to retrieve mp4 duration, Pilat_id Or Mpx_id: {}", 
									CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId());
							AssetStatus status = asset.getFailAttempt() < 19 ? AssetStatus.EXTRACTING : AssetStatus.FAILED_TO_EXTRACT;
							asset.setStatus(status);
							asset.setFailAttempt(asset.getFailAttempt()+1);
					} else {
							asset.setMp4VideoDuration(mp4duration);
							asset.setFailAttempt(0);
							asset.setExportingXmlFileFlag(true);
						}
						
					} catch (Exception e) {
						logger.error("Exception while getting mp4 duration: {}, Exception:{}", asset, e.getMessage());
						AssetStatus status = asset.getFailAttempt() < 19 ? AssetStatus.EXTRACTING : AssetStatus.FAILED_TO_EXTRACT;
						asset.setStatus(status);
						asset.setFailAttempt(asset.getFailAttempt()+1);

					}
					logger.info("Asset MP4 Duration status : {} and assets no :{} and Fail Attempt : {} ",asset.getStatus(),asset.getAssetNo(),asset.getFailAttempt());
					assetRepository.save(asset);
			});
		}
	}
	
	@Scheduled(cron = "${live2vod.asset_post_xml_create_cron_time}")
	public void postXmlCreateJob() {
		//logger.info("@@ asset xml create job ");
		List<Asset> assetsSchedules = assetSchedulerService.getScheduledAssetsForCreatingXml();
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		if (property != null && CommonUtils.isNotNullOrEmpty(property.getMuleSoft()) && assetsSchedules != null && !assetsSchedules.isEmpty()) {
			assetsSchedules.forEach(asset -> {
				logger.info("Request for Perpare Export Asset : {}",asset.getAssetNo());
				String response = null;
					try {
						response = muleIntegrationService.sendRequestToMuleForCreatingXmlFile(asset, property.getMuleSoft());
					
						if (CommonUtils.isNotNullOrEmpty(response)) {
							logger.info("Fail response from mule to create xml file, pilat_id OR mpx_id: {}", 
									CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId());
							AssetStatus status = asset.getAutoPublish() ? AssetStatus.SUBMITTED_FOR_PUBLICATION : AssetStatus.READY_TO_PUBLISH;
							asset.setStatus(status);
							asset.setFailAttempt(0);
						} else {
							//Set attempt for failure
							AssetStatus status = asset.getFailAttempt() < 14 ? AssetStatus.EXTRACTING : AssetStatus.FAILED_TO_EXTRACT;
							asset.setStatus(status);
							asset.setFailAttempt(asset.getFailAttempt()+1);
						}
					} catch (Exception e) {
						logger.error("Exception occured while creating asset xml file: {} and  Exception: {}", asset.getAssetNo(), e.getMessage());
						AssetStatus status = asset.getFailAttempt() < 14 ? AssetStatus.EXTRACTING : AssetStatus.FAILED_TO_EXTRACT;
						asset.setStatus(status);
						asset.setFailAttempt(asset.getFailAttempt()+1);
					}
					logger.info("Asset Prepare Export status : {} and assets no :{} and Fail Attempt : {} ",asset.getStatus(),asset.getAssetNo(),asset.getFailAttempt());
					assetRepository.save(asset);
				
			});
		}
	}
	
	
}

