package com.chirpn.live2vod.service.scheduler;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;

import com.chirpn.live2vod.entity.Asset;

@Service
public interface Publisher {

	CompletableFuture<Asset> publish(Asset asset) throws Exception;

}
