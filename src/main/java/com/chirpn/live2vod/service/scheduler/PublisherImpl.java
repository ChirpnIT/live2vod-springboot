package com.chirpn.live2vod.service.scheduler;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.service.assetsservice.AssetService;
import com.chirpn.live2vod.service.assetsservice.AssetServiceImpl;

@Service
public class PublisherImpl implements Publisher {

	@Autowired
	private AssetService assetService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AssetServiceImpl.class);

	@Async
	@Override
	public CompletableFuture<Asset> publish(Asset asset) throws Exception {
		LOGGER.info("Request submitted for Export Asset :{} ", asset.getAssetNo());
		assetService.ExportAsset(asset);
		return CompletableFuture.completedFuture(asset);
	}


}
