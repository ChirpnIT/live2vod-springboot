package com.chirpn.live2vod.service.scheduler;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.repository.AssetRepository;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;
import com.chirpn.live2vod.service.muleintegrationservice.MuleIntegrationService;
import com.chirpn.live2vod.util.CommonUtils;

/**
 * 
 * @author Satish ghonge
 * 
 */

@Component
@Profile("scheduler_enabled")
public class DeleteAssetScheduler {
	
	private static final Logger logger = LoggerFactory.getLogger(DeleteAssetScheduler.class);
	
	@Autowired
	private AssetSchedulerService assetSchedulerService;

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;
	
	@Autowired
	private MuleIntegrationService muleIntegrationService;
	
	@Autowired
	private ChannelRepository channelRepository;
	
	
	@Scheduled(cron = "${live2vod.delete_published_asset_cron_time}")
	@Transactional
	public void deleteAssetPackagesFromDeltaAndOutFolder() {
		List<Asset> assets = null;
		//logger.info("@@ delete scheduler");
		try {
			Date newDate = CommonUtils.getLastThirtyMinuteDateAndTime();
			assets = assetRepository.retrieveByStatus(newDate);
		} catch (Exception err) {
			logger.error("Exception while getting published asset deleting: {}", err.getMessage());
			
		}
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		//logger.info("Asset list for delete assets:{} and integration property:{}", assets, property);
		if (property != null && assets != null && !assets.isEmpty()) {
			assets.forEach(asset -> {
				logger.info("Request for Delete Asset Asset : {}",asset.getAssetNo());
				String deltaUrl = asset.getDeltaUrlFlag() ? property.getDeltaFailOverUrl() : property.getDeltaUrl();
				String channelId = asset.getDeltaUrlFlag() ? asset.getChannel().getDeltaFailOverStreamId() : asset.getChannel().getStreamId();
				try {
					Integer hlsPackageId = assetSchedulerService.getHlsPackageId(asset.getMp4Url(), asset.getAdRemoval());
					String pilatOrMpxId = CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId();
					pilatOrMpxId = asset.getUniqueHlsName()+"_"+ pilatOrMpxId;
					if (hlsPackageId != null && CommonUtils.isNotNullOrEmpty(deltaUrl) 
							&& CommonUtils.isNotNullOrEmpty(property.getMuleSoft()) 
							&& CommonUtils.isNotNullOrEmpty(pilatOrMpxId)) {
						String deltaUrlresponse = muleIntegrationService.sendRequestToMuleForDeleteAssetsPackageAndVideo(channelId, deltaUrl, hlsPackageId, property.getMuleSoft(), pilatOrMpxId);
						if (CommonUtils.isNotNullOrEmpty(deltaUrlresponse)) {
							asset.setDeleted(true);
						}
					}
				} catch (Exception e) {
					logger.error("Exception while deleting published asset based on delta url: {}, content id: {} and error message: {}", deltaUrl, channelId, e.getMessage());
					asset.setDeleted(true);
				}
				logger.info("Asset Delete status : {} and assets no :{}",asset.getStatus(),asset.getAssetNo());
				assetRepository.save(asset);
			});
		}

	}
	
	
	@Scheduled(cron = "${live2vod.delete_asset_cron_time}")
	@Transactional
	public void deleteExtractedAssetsJob() {
		String lastSundayDate = CommonUtils.retrieveLastSundayDate();
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		List<Channel> channels = channelRepository.findAll();
		logger.info("channel list for delete assets:{}", channels);
		if (property != null && channels != null && !channels.isEmpty()) {
			channels.forEach(c -> {
				try {
					if (CommonUtils.isNotNullOrEmpty(property.getDeltaUrl())) {
						String deltaUrlresponse = muleIntegrationService.sendRequestToMuleForDeleteAssets(c.getStreamId(), lastSundayDate, property.getDeltaUrl(), property.getMuleSoft(), false);
						if (CommonUtils.isNotNullOrEmpty(deltaUrlresponse)) {
							assetRepository.updateByChannelAndDate(c, CommonUtils.convertDateToLocalDate(lastSundayDate), false);
						}
					}
					
				} catch (Exception e) {
					logger.error("Exception while deleting asset based on delta url: {} and content id: {}", e.getMessage(), c.getStreamId());
				}

				try {
					if (CommonUtils.isNotNullOrEmpty(property.getDeltaFailOverUrl())) {
						String deltaFailurlresponse = muleIntegrationService.sendRequestToMuleForDeleteAssets(c.getDeltaFailOverStreamId(), lastSundayDate, property.getDeltaFailOverUrl(), property.getMuleSoft(), true);
						if (CommonUtils.isNotNullOrEmpty(deltaFailurlresponse)) {
							assetRepository.updateByChannelAndDate(c, CommonUtils.convertDateToLocalDate(lastSundayDate), true);
						}
					}
				} catch (Exception e) {
					logger.error("Exception while deleting based on delta fail over url asset: {} and content id: {}", e.getMessage(), c.getDeltaFailOverStreamId());
				}
			});
		}

	}

}
