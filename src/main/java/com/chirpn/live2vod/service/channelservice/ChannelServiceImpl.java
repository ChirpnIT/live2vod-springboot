package com.chirpn.live2vod.service.channelservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.constants.Messages;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.Group;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.model.GroupDTO;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.GroupRepository;
import com.chirpn.live2vod.security.SecurityService;
import com.chirpn.live2vod.service.adgroupservice.AdGroupService;
import com.chirpn.live2vod.util.CommonUtils;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChannelServiceImpl implements ChannelService {
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired 
	private GroupRepository groupRepository;
	
	@Autowired
	private AdGroupService adGroupService;
	
	@Autowired
	private SecurityService secueiryService;
	
	@Override
	public Response<List<ChannelDTO>> retrieve() throws Exception {
		Response<List<ChannelDTO>> response = new Response<>();
		List<ChannelDTO> channelsDto = new ArrayList<>();
		UserDetails userDetails = secueiryService.getLoggeInUser();
		if (userDetails == null) {
			response.setStatus(HttpStatus.UNAUTHORIZED);
			response.setStatusMessage(Messages.UNAUTHORIZED);
			return response;
		} 
		
		List<Channel> channelRecords = null;

		if (secueiryService.isAdministrator()) {
			channelRecords = channelRepository.findAll();
		} else {
			List<String> roles = secueiryService.getUserRoles();
			channelRecords = channelRepository.findByGroups(roles);
		}
		
		if (channelRecords == null || channelRecords.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT);
			response.setStatusMessage(Messages.RECORD_NOT_FOUND);
			return response;
		}
		channelsDto = channelEntityMapToChannelDto(channelRecords);
		response.setData(channelsDto);
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		return response;
	}

	@Override
	public Response<ChannelDTO> retrieve(String channelNo) throws Exception {
		Response<ChannelDTO> response = new Response<>();
		Channel channelRecord = channelRepository.findByChannelNo(channelNo);
		if (channelRecord == null) {
			response.setStatus(HttpStatus.NO_CONTENT);
			response.setStatusMessage(HttpStatus.NO_CONTENT.name());
			return response;
		}
		ChannelDTO channelDTO = new ChannelDTO();
		mapChannelEntityTOChannelDto(channelDTO, channelRecord);
		response.setData(channelDTO);
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		return response;
	}

	@Override
	public Response<List<ChannelDTO>> create(List<ChannelDTO> channelModel) throws Exception {
		Response<List<ChannelDTO>> response = new Response<>();
		List<Channel> channels = new ArrayList<>();
		List<ChannelDTO> channelRecords = new ArrayList<>();

		if (channelModel.isEmpty()) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.CHANNEL_LIST);
			return response;
		}
		channelModel.forEach(c -> {
			Channel channel = mapChannelDtoTOChannelEntity(c);
			channels.add(channel);
		});
		
		channelRepository.save(channels);

		channels.forEach(c -> {
			ChannelDTO channel = new ChannelDTO();
			mapChannelEntityTOChannelDto(channel, c);
			channelRecords.add(channel);
		});
		
		response.setData(channelRecords);
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		return response;
	}

	@Override
	public Response<List<ChannelDTO>> update(List<ChannelDTO> channelModel) throws Exception {
		Response<List<ChannelDTO>> response = new Response<>();
		List<Channel> channels = new ArrayList<>();
		List<ChannelDTO> channelRecords = new ArrayList<>();

		if (channelModel.isEmpty()) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.CHANNEL_LIST);
			return response;
		}
		channelModel.forEach(c -> {
			Channel channel =  mapChannelDtoTOChannelEntity(c);
			channels.add(channel);
		});
		
		channelRepository.save(channels);

		channels.forEach(c -> {
			ChannelDTO channel = new ChannelDTO();
			mapChannelEntityTOChannelDto(channel, c);
			channelRecords.add(channel);
		});
		response.setData(channelRecords);
		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		return response;
	}

	@Override
	public Response<String> delete(String channelNo) throws Exception {
		Response<String> response = new Response<>();
		if(CommonUtils.isNullOrEmpty(channelNo)) {
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(Messages.CHANNEL_NO_IS_REQUIRED);
			return response;
		}
		channelRepository.deleteByChannelNo(channelNo);

		response.setStatus(HttpStatus.OK);
		response.setStatusMessage(HttpStatus.OK.name());
		return response;
	}

	@Override
	public void mapChannelEntityTOChannelDto(ChannelDTO channels, Channel channelRecords) {
		channels.setChannelNo(channelRecords.getChannelNo());
		channels.setDeluxeId(channelRecords.getDeluxeId());
		channels.setName(channelRecords.getName());
		channels.setStreamId(channelRecords.getStreamId());
		channels.setDeltaFailOverStreamId(channelRecords.getDeltaFailOverStreamId());
		channels.setDuration(channelRecords.getDuration());
		Set<Group> groups = channelRecords.getGroups();
		for (Group group : groups) {
			GroupDTO groupDTO = new GroupDTO();
			adGroupService.mapGroupEntityToGroupDto(groupDTO, group);
			channels.getGroups().add(groupDTO);
		}
	}

	@Override
	public Channel mapChannelDtoTOChannelEntity(ChannelDTO channels) {
		Channel channelRecords = null;
		if (CommonUtils.isNotNullOrEmpty(channels.getChannelNo())) {
			channelRecords = channelRepository.findByChannelNo(channels.getChannelNo());
		}
		if (channelRecords == null) {
			channelRecords = new Channel();
		}
		if (CommonUtils.isNullOrEmpty(channels.getChannelNo())) {
			channelRecords.setChannelNo(CommonUtils.generateChannelNumber());
		}
		channelRecords.setDeluxeId(channels.getDeluxeId());
		channelRecords.setName(channels.getName());
		channelRecords.setStreamId(channels.getStreamId());
		channelRecords.setDeltaFailOverStreamId(channels.getDeltaFailOverStreamId());
		channelRecords.setDuration(channels.getDuration());
		for (GroupDTO groupDTO : channels.getGroups()) {
			Group group = groupRepository.findByGroupNo(groupDTO.getGroupNo());
			if (group == null) {
				group = new Group();
			}
			adGroupService.mapGroupDtoToGroupEntity(groupDTO, group);
			group.setChannel(channelRecords);
			channelRecords.getGroups().add(group);
		}
		return channelRecords;
	}

	@Override
	public List<ChannelDTO> channelEntityMapToChannelDto(List<Channel> channels) throws Exception {
		List<ChannelDTO> channelsDto = new ArrayList<>();
		// map Entity to DTO for return created response
		ModelMapper modelMapper1 = new ModelMapper();
		channels.forEach(c -> {
			ChannelDTO returnResponse = modelMapper1.map(c, ChannelDTO.class);
			List<GroupDTO> groupDtos = new ArrayList<>();
			if (c.getGroups() != null) {
				c.getGroups().forEach(g -> {
					GroupDTO groupResponse = new GroupDTO();
					adGroupService.mapGroupEntityToGroupDto(groupResponse, g);
					if (groupResponse != null) {
						groupDtos.add(groupResponse);
					}
				});
				if (groupDtos != null && !groupDtos.isEmpty())
					returnResponse.setGroups(groupDtos);
			}
			channelsDto.add(returnResponse);
		});
		return channelsDto;
	}

}
