package com.chirpn.live2vod.service.channelservice;

import java.util.List;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.model.ChannelDTO;

/**
 * 
 * @author Satish ghonge
 * 
 */
public interface ChannelService {

	Response<List<ChannelDTO>> retrieve() throws Exception;
	
	Response<ChannelDTO> retrieve(String channelNo) throws Exception;

	Response<List<ChannelDTO>> create(List<ChannelDTO> channelModel) throws Exception;
	
	Response<List<ChannelDTO>> update(List<ChannelDTO> channelModel) throws Exception;

	Response<String> delete(String channelNo) throws Exception;

	void mapChannelEntityTOChannelDto(ChannelDTO channels, Channel channelRecords);
	
	Channel mapChannelDtoTOChannelEntity(ChannelDTO channels);
	
	List<ChannelDTO> channelEntityMapToChannelDto(List<Channel> channels) throws Exception;

}
