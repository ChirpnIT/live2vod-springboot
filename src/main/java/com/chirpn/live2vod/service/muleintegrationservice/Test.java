package com.chirpn.live2vod.service.muleintegrationservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public abstract class Test {
	private static final Logger logger = LoggerFactory.getLogger(Test.class);

	public static void main(String[] args) {
		for (int i = 13053; i < 13500; i++) {
			try {
				RestTemplate restTemplate = new RestTemplate();
String requestUrl = "http://10.21.160.59:8112/live2vod/83/filters/" + i + "?deltaHost=10.21.210.7";
				ResponseEntity<String> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.DELETE, null,
						String.class);
				logger.info("response of preview stream is:{} ", responseEntity);
			} catch (Exception e) {
				logger.info("Error occured when test:{}", e.getMessage());
			}
		}
	}

}
