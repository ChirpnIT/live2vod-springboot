package com.chirpn.live2vod.service.muleintegrationservice;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.model.ChannelContentDTO;
import com.chirpn.live2vod.model.deltapreviewstream.DeltaPreviewStreamDTO;
import com.chirpn.live2vod.model.mpx.Media;

public class ChirpnMuleIntegrationServiceImpl implements MuleIntegrationService {

	@Value("${mule.delta.preview.stream.url}")
	private String muleDeltaPreviewStreamUrl;

	public static final Logger LOGGER = Logger.getLogger(ChirpnMuleIntegrationServiceImpl.class);

	@Override
	public Response<ChannelContentDTO> callMuleApiToRetrievePreviewAssetsStream(String streamId) throws Live2VodException {
		LOGGER.info("Returning static video stream url: " + muleDeltaPreviewStreamUrl);
		Response<ChannelContentDTO> response = new Response<>();
		ChannelContentDTO channelContentDTO = new ChannelContentDTO();
		channelContentDTO.setHlsVideoURL(muleDeltaPreviewStreamUrl);
		channelContentDTO.setLowestFramerateNumerator("25");
		channelContentDTO.setLowestFramerateDenominator("1");
		response.setStatus(HttpStatus.OK);
		response.setData(channelContentDTO);
		return response;
	}

	@Override
	public DeltaPreviewStreamDTO sendRequestToMuleForExtractAssets(Asset asset, String streamNo, String deltaUrl, String muleSoftUrl, Boolean DeltaUrlFlag) throws Live2VodException {
		LOGGER.info("Extract asset request received with assetDTO: " + asset + ", for stream no: " + streamNo);
		throw new Live2VodException("Mule exception");
	}

	@Override
	public String sendPublishRequestToMule(Media media, Asset asset, String mpxUrl, String muleSoftUrl)
			throws Live2VodException {

		return null;
	}

	@Override
	public String sendRequestToMuleForDeleteAssets(String streamId, String date, String deltaUrl, String muleSoftUrl, Boolean DeltaUrlFlag) {

		return null;
	}

	@Override
	public String makeMp4Url(String url) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String replacePortAndBasePath(String url, String deltaUrl, String requestType) throws Live2VodException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String makeMp4Url(String m3u8Url, String m3u8Id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String sendRequestToMuleForDeleteAssetsPackageAndVideo(String streamId, String deltaUrl,
			Integer hlsPackageId, String muleSoftUrl, String pilatId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String sendRequestToMuleForCreatingXmlFile(Asset asset, String muleSoftUrl) {
		// TODO Auto-generated method stub
		return null;
	}

	


}
