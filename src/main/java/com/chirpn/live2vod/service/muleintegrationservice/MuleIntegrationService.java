package com.chirpn.live2vod.service.muleintegrationservice;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.model.ChannelContentDTO;
import com.chirpn.live2vod.model.deltapreviewstream.DeltaPreviewStreamDTO;
import com.chirpn.live2vod.model.mpx.Media;

/**
 * 
 * @author Satish ghonge
 * 
 */
public interface MuleIntegrationService {
	
	Response<ChannelContentDTO> callMuleApiToRetrievePreviewAssetsStream(String streamId) throws Live2VodException;
	
	DeltaPreviewStreamDTO sendRequestToMuleForExtractAssets(Asset asset, String streamNo, String deltaUrl, String muleSoftUrl, Boolean deltaUrlFlag) throws Live2VodException;
	
	String sendPublishRequestToMule(Media media, Asset asset, String mpxUrl, String muleSoftUrl) throws Live2VodException;
	
	String sendRequestToMuleForDeleteAssets(String streamId, String date, String deltaUrl, String muleSoftUrl, Boolean deltaUrlFlag);
	
	String makeMp4Url(String url);

	String replacePortAndBasePath(String url, String deltaUrl, String requestType) throws Live2VodException;

	String makeMp4Url(String m3u8Url, String m3u8Id);
	
	String sendRequestToMuleForDeleteAssetsPackageAndVideo(String streamId, String deltaUrl, Integer hlsPackageId, String muleSoftUrl, String pilatId);
	
	String sendRequestToMuleForCreatingXmlFile(Asset asset, String muleSoftUrl);

}
