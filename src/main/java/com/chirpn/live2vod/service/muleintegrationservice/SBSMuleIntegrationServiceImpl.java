package com.chirpn.live2vod.service.muleintegrationservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.config.RestTemplateResponseErrorHandler;
import com.chirpn.live2vod.common.constants.Constants;
import com.chirpn.live2vod.common.constants.Messages;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.ChannelContentDTO;
import com.chirpn.live2vod.model.delta.CreateFilterRequest;
import com.chirpn.live2vod.model.deltapreviewstream.DeltaPreviewStreamDTO;
import com.chirpn.live2vod.model.mpx.Media;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;
import com.chirpn.live2vod.util.CommonUtils;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Transactional(noRollbackFor = {Live2VodException.class, Exception.class})
public class SBSMuleIntegrationServiceImpl implements MuleIntegrationService {

	private static final Logger logger = LoggerFactory.getLogger(SBSMuleIntegrationServiceImpl.class);

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;
	
	@Autowired
	private ChannelRepository channelRepository;

	@Override
	public Response<ChannelContentDTO> callMuleApiToRetrievePreviewAssetsStream(String channelNo) throws Live2VodException {
		ChannelContentDTO cannelContentDTO = null;
		Response<ChannelContentDTO> response = new Response<>();
		String deltaUrl = null;
		String streamId = null;
		Boolean deltaUrlFlag;
		String requestUrl;
		logger.info("channel number to get hls video url: {}", channelNo);
		Channel channel = channelRepository.findByChannelNo(channelNo);
		if (channel == null) {
			throw new Live2VodException(Messages.CHANNEL_NOT_FOUND);
		}
		IntegrationProperties property = integrationPropertiesRepository.findTopByOrderById();
		
		if (property == null || CommonUtils.isNullOrEmpty(property.getMuleSoft())) {
			throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
		}
		
		if (property.getDeltaUrlFlag()) {
			if (property.getDeltaFailOverUrl() == null || property.getDeltaFailOverUrl().isEmpty())
				throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
			if (CommonUtils.isNullOrEmpty(channel.getStreamId()))
				throw new Live2VodException(Messages.DELTA_STREAM_ID_SHOULD_NOT_BE_NULL);
			
			deltaUrl = property.getDeltaFailOverUrl();
			streamId = channel.getDeltaFailOverStreamId();
			deltaUrlFlag = true;
			
		} else {
			if (property.getDeltaUrl() == null || property.getDeltaUrl().isEmpty())
				throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
			if (CommonUtils.isNullOrEmpty(channel.getDeltaFailOverStreamId()))
				throw new Live2VodException(Messages.DELTA_STREAM_FAIL_OVER_ID_SHOULD_NOT_BE_NULL);
			deltaUrl = property.getDeltaUrl();
			streamId = channel.getStreamId();
			deltaUrlFlag = false;
		}
		
		if (CommonUtils.isNullOrEmpty(deltaUrl)) {
			throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
		}
		logger.info("stream id to get hls video url: {} and delta url : {}  and delta url flag : {}", streamId, deltaUrl, deltaUrlFlag);
		ArrayList<String> urls = separateHostAndPortFromUrl(deltaUrl);
		if (deltaUrlFlag) {
			requestUrl = property.getMuleSoft() + Constants.MULE_BASE_PATH + streamId + Constants.DELTAHOST + urls.get(1)+ Constants.DELTA_URL_FLAG +deltaUrlFlag; 
		} else {
			requestUrl = property.getMuleSoft() + Constants.MULE_BASE_PATH + streamId + Constants.DELTAHOST + urls.get(1); 
		}
		logger.info("request url for preview stream is: {}", requestUrl);
		try {
			cannelContentDTO =  callMuleRestApiForDeltaPreviewStream(requestUrl);
			if (cannelContentDTO == null) {
				response.setStatus(HttpStatus.BAD_REQUEST);
				response.setStatusMessage(Messages.DELTAEXCEPTION);
				return response;
			}
			String url = replacePortAndBasePath(cannelContentDTO.getHlsVideoURL(), deltaUrl, Constants.PREVIEW_STREAM);
			cannelContentDTO.setHlsVideoURL(url);
			response.setData(cannelContentDTO);
			response.setStatus(HttpStatus.OK);
			response.setStatusMessage(HttpStatus.OK.toString());
		} catch (Exception e) {
			throw new Live2VodException(Messages.DELTAEXCEPTION);
			
		}
		return response;

	}
	
	public ChannelContentDTO callMuleRestApiForDeltaPreviewStream(String requestUrl) {
		ChannelContentDTO channelContentDTO = new ChannelContentDTO();
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<DeltaPreviewStreamDTO> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, DeltaPreviewStreamDTO.class);
		logger.info("response of preview stream is: {}", responseEntity);
		if (responseEntity.getBody() != null) {
			logger.info("response body for preview live2vod stream: {}", responseEntity.getBody());
			DeltaPreviewStreamDTO deltaPreviewStreamDTO = responseEntity.getBody();
			if (deltaPreviewStreamDTO.getContent().getFilters() != null) {
				if (CommonUtils.isNullOrEmpty(deltaPreviewStreamDTO.getContent().getFilters().get(0).getDefaultEndpointUri())) {
					throw new Live2VodException(Messages.HLS_VIDEO_PREVIEW_STREAM_NOT_AVAILABLE);
				}
				channelContentDTO.setHlsVideoURL(deltaPreviewStreamDTO.getContent().getFilters().get(0).getDefaultEndpointUri());
				if (deltaPreviewStreamDTO.getContent().getFilters().get(0).getFilterSettings() != null) {
					String lowestFramerateNumerator = deltaPreviewStreamDTO.getContent().getFilters().get(0).getFilterSettings().getLowestFramerateNumerator();
                    String lowestFramerateDenominator = deltaPreviewStreamDTO.getContent().getFilters().get(0).getFilterSettings().getLowestFramerateDenominator();
                    channelContentDTO.setLowestFramerateNumerator(lowestFramerateNumerator);
                    channelContentDTO.setLowestFramerateDenominator(lowestFramerateDenominator);
				}
				
			}
			logger.info("livetovod preview stream url is : {}, lowest frame rate numerator is : {}, "
					+ "lowest frame rate denominator is :{}", channelContentDTO.getHlsVideoURL(), 
					channelContentDTO.getLowestFramerateNumerator(), 
					channelContentDTO.getLowestFramerateDenominator());
		}
		return channelContentDTO;
	}

	@Override
	public DeltaPreviewStreamDTO sendRequestToMuleForExtractAssets(Asset asset, String streamNo, String deltaUrl, String muleSoftUrl, Boolean deltaUrlFlag) throws Live2VodException {
		DeltaPreviewStreamDTO deltaPreviewStreamDto;
		String startTime = CommonUtils.convertEpochTimeToDateWtihTimeZone(asset.getStartDateTime());
		String endTime = CommonUtils.convertEpochTimeToDateWtihTimeZone(asset.getEndDateTime());
		logger.info("StartDateTime: {}, EndDateTime:{}, startFrame:{}, endFrame: {}", startTime, endTime,  asset.getStartFrame(), asset.getEndFrame());
		String uniqueHlsName = CommonUtils.generateUniqueHlsName();
		asset.setUniqueHlsName(uniqueHlsName);
		String requestPath = muleSoftUrl + Constants.MULE_BASE_PATH 
				+ Constants.MULE_URL_EXTRACT_ASSETS_CUT_POINT;
		ArrayList<String> urls = separateHostAndPortFromUrl(deltaUrl);
		String requestUrl = createMuleUrlForExtractAssets(requestPath, streamNo, urls, deltaUrlFlag);
		CreateFilterRequest requestObj = createExtractAssetRequestObject(asset, asset.getStartFrame(), asset.getEndFrame(), startTime, endTime);
		logger.info("Complete url to extract assets{} and createFilter request object: {} and stream number:{}", requestUrl, requestObj, streamNo);
		try {
			deltaPreviewStreamDto = callMuleRestApiForExtractAssets(requestUrl, uniqueHlsName, requestObj);
		} catch (Exception e) {
			logger.error("Exception from Delta server while extracting asset: {}", e.getMessage());
			throw new Live2VodException(Messages.DELTAEXCEPTION);
		}
		
		return deltaPreviewStreamDto;
	}
	
	private String createMuleUrlForExtractAssets(String basePathUrl, String streamNo, ArrayList<String> urls, Boolean deltaUrlFlag) {
		if (deltaUrlFlag) {
			return  basePathUrl + streamNo + Constants.DELTAHOST + urls.get(1) + Constants.DELTA_URL_FLAG + deltaUrlFlag;
		} else {
			return  basePathUrl + streamNo + Constants.DELTAHOST + urls.get(1);
		}
		
		
	}
	
	private DeltaPreviewStreamDTO callMuleRestApiForExtractAssets(String requestUrl, String uniqueHlsName, CreateFilterRequest createFilterRequest) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<CreateFilterRequest> request = new HttpEntity<>(createFilterRequest, headers);
		logger.info("Extract asset request:{} ", request);
		ResponseEntity<DeltaPreviewStreamDTO> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.POST, request, DeltaPreviewStreamDTO.class);
		logger.info("Extract asset response:{} ", responseEntity);
		DeltaPreviewStreamDTO deltaPreviewStream = null;
		if (responseEntity.getBody() !=null) {
			logger.info("Extract asset response body:{} ", responseEntity.getBody());
			return responseEntity.getBody();
			
		}
		return deltaPreviewStream;
	}

	@Override
	public String sendPublishRequestToMule(Media media, Asset asset, String mpxUrl, String muleSoftUrl) throws Live2VodException {
		String response = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		logger.info("Pilat id for publish assets: {}", media.getPl1$pilatId());
		ArrayList<String> urls = separateHostAndPortFromUrl(mpxUrl);
		Integer idType = CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? 0 : 1;
		String requestUrl = createMpxUrlToPublish(muleSoftUrl , media.getPl1$pilatId(), asset.getGeoTargeting(), urls, idType, asset);
		logger.info("request url for publish assets: {}", requestUrl);
		try {
			response = callMpxToPublishVideo(media, requestUrl);
			return response;
		} catch (Exception e) {
			logger.error("Error while to publish assets:{}", e.getMessage());
			throw new Live2VodException(Messages.MPXEXCEPTION);
		}
	}
	
	private String createMpxUrlToPublish(String muleSoftUrl, String pilatId, String location, ArrayList<String> urls, Integer idType, Asset asset ) {
		String outType = location.equalsIgnoreCase("Geo blocked") ? "GeoBlocked"
				: location.equalsIgnoreCase("Not Geo Blocked") ? "NonBlocking" : "Encrypted";
		return muleSoftUrl +Constants.MULE_BASE_PATH+"media/"+pilatId+"?mpxHost="+urls.get(1)+"&outputType="+outType+"&duration="+asset.getDuration()+"&idType="+idType ;
	}
	
	private String callMpxToPublishVideo(Media media, String requestUrl) {
		String response = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Media> request = new HttpEntity<>(media, headers);
		logger.info("request body for publish mpx metadata: {}", request);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		ResponseEntity<Media> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.POST, request,
				Media.class);
		logger.info("response of publish mpx metadata api is : {}", responseEntity);
		if(responseEntity !=null && responseEntity.getStatusCodeValue()==200) {
			response = Constants.SUCCESS;
		}
		return response;
	}
	
	private ArrayList<String> separateHostAndPortFromUrl(String url) throws Live2VodException {
		if(CommonUtils.isNullOrEmpty(url))
			throw new Live2VodException(Messages.INTEGRATION_PROPERTY_UNAVAILABLE);
		
		ArrayList<String> urlGroups = new ArrayList<>();
		Pattern pattern = Pattern.compile("(https?://)([^:^/]*)(:\\d*)?(.*)?");
		Matcher matcher = pattern.matcher(url);
	    matcher.find();
	    String protocol = matcher.group(1);
	    String domain = matcher.group(2);
	    String port = matcher.group(3);
	    String uri = matcher.group(4);
	    urlGroups.add(protocol);
	    urlGroups.add(domain);
	    urlGroups.add(CommonUtils.isNotNullOrEmpty(port) ? port.replaceAll(":", "") : "8080");
	    urlGroups.add(CommonUtils.isNotNullOrEmpty(uri) ? uri : "");
	    logger.info("protocol:{} domain:{} port:{} basePath:{}", protocol, domain, port, uri);
	    return urlGroups;
	}

	public static String inputStreamToString(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		String line;
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}
	
	@Override
	public String replacePortAndBasePath(String url, String deltaUrl, String requestType) throws Live2VodException {
		String completeUrl = null;
		if (CommonUtils.isNotNullOrEmpty(url)) {
			Pattern pattern = Pattern.compile("(https?://)([^:^/]*)(:\\d*)?(.*)?");
			Matcher matcher = pattern.matcher(url);
		    matcher.find();
		    try { 
		    	ArrayList<String> ports =  separateHostAndPortFromUrl(deltaUrl);
			    String uri  = ports.get(0)+ports.get(1)+matcher.group(4);
			    completeUrl = requestType.equalsIgnoreCase(Constants.PREVIEW_STREAM) ? uri.split("\\.m3u8")[0]+"_5"+ uri.substring(uri.lastIndexOf(".")) : uri;
			    logger.info("complete uri:{}", completeUrl);
		    } catch(Exception e) {
		    	logger.error("Error occured while parsing delta url : {} ", e);
		    }
		    
		}
		
	    return completeUrl;
	}

	@Override
	public String sendRequestToMuleForDeleteAssets(String streamId, String date, String deltaUrl, String muleSoftUrl, Boolean deltaUrlFlag) {
		String response = null;
		ArrayList<String> urls = separateHostAndPortFromUrl(deltaUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(null, headers);
		logger.info("request body for delete assets: {}", request);
		RestTemplate restTemplate = new RestTemplate();
		String requestUrl =  createDeltaAssetDeleteUrl(muleSoftUrl, streamId, date, urls, deltaUrlFlag);
		logger.info("complete delete assets mule api url: {}", requestUrl);
		ResponseEntity<String> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.DELETE, request,
				 String.class);
		logger.info("response delete asset api : {}", responseEntity);
		if (responseEntity.getBody() != null) {
			logger.info("retrieve body from delete asset api response: {}", responseEntity.getBody());
			response = Constants.SUCCESS;

		}
		return response;
	}
	
	private String createDeltaAssetDeleteUrl(String muleSoftUrl, String streamId, String date, ArrayList<String> urls, Boolean deltaUrlFlag) {
		if (deltaUrlFlag) {
			return muleSoftUrl+Constants.MULE_BASE_PATH+streamId+Constants.DELTAHOST+urls.get(1)+"&date="+date+Constants.DELTA_URL_FLAG+deltaUrlFlag;
		} else {
			return muleSoftUrl+Constants.MULE_BASE_PATH+streamId+Constants.DELTAHOST+urls.get(1)+"&date="+date;

		}
	}
	
	private CreateFilterRequest createExtractAssetRequestObject(Asset asset, Integer startFrame, Integer endFrame, String startTime, String endTime) {
		CreateFilterRequest request = new CreateFilterRequest();
		request.setHlsName(asset.getUniqueHlsName());
		request.setAdRemove(asset.getAdRemoval() ? 1 : 0);
		request.setStartFrame(startFrame);
		request.setEndFrame(endFrame);
		request.setStartTime(startTime);
		request.setEndTime(endTime);
		request.setId(CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId());
		request.setIdType("0");
		return request;
	}
	
	@Override 
	public String makeMp4Url(String url) { 
		String mp4Url = null;
		Integer mp4Id =
				Integer.valueOf(url.substring(url.lastIndexOf('/')+1,url.indexOf(".m3u8")));
		logger.info("mp4 id : {}", mp4Id); if(mp4Id!=null) { mp4Id -=2;
		logger.info("mp4 id : {}", mp4Id); mp4Url =
				url.substring(0,url.lastIndexOf('/')+1)+mp4Id+".mp4";
		logger.info("m3u8 url : {}", url); logger.info("MP4 url  : {}", mp4Url); }
		return mp4Url; 
	}
	 
	
	@Override
	public String makeMp4Url(String m3u8Url, String m3u8Id) {
		return  m3u8Url.replace(".m3u8", ".mp4").replace(m3u8Id, String.valueOf((Integer.valueOf(m3u8Id)-4)));
	}

	@Override
	public String sendRequestToMuleForDeleteAssetsPackageAndVideo(String streamId, String deltaUrl,
			Integer hlsPackageId, String muleSoftUrl, String pilatId) {
		String response = null;
		ArrayList<String> urls = separateHostAndPortFromUrl(deltaUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(null, headers);
		logger.info("request body for delete published assets based on hls:{},  package id: {}, content id:{}, pilat id: {}", request, hlsPackageId, streamId, pilatId);
		RestTemplate restTemplate = new RestTemplate();
		String requestUrl =  createDeltaUrlForDeleteAsset(muleSoftUrl, streamId, urls, hlsPackageId, pilatId);
		logger.info("complete delete assets mule api url to delete published asset: {}", requestUrl);
		ResponseEntity<String> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.DELETE, request,
				 String.class);
		logger.info("response delete published asset api : {}", responseEntity);
		if (responseEntity.getBody() != null && responseEntity.getStatusCodeValue()==200) {
			logger.info("retrieve body from delete published asset api response: {}", responseEntity.getBody());
			response = Constants.SUCCESS;

		}
		return response;
	}
	
	private String createDeltaUrlForDeleteAsset(String muleSoftUrl, String streamId, ArrayList<String> urls, Integer hlsPackageId,  String pilatId) {
		return muleSoftUrl+Constants.MULE_BASE_PATH+streamId+"/filters/"+hlsPackageId+Constants.DELTAHOST+urls.get(1)+"&id="+pilatId;
	
	}

	@Override
	public String sendRequestToMuleForCreatingXmlFile(Asset asset, String muleSoftUrl) {
		String response = null;
				String requestPath = muleSoftUrl + Constants.MULE_BASE_PATH 
						+ Constants.MULE_URL_XML_CREATION_CUT_POINT;
				String requestUrl = createMuleUrlForCreatingXmlFile(requestPath, asset);
				logger.info("Complete url to create xml file {} and and asset pilat id: {}", requestUrl, asset.getPilatId());
				try {
					response = callMuleRestApiToCreateXml(requestUrl);
					logger.info("Response from mule to create xml file: {} and pilat_id: {}", response, asset.getPilatId());
				} catch (Exception e) {
					logger.error("Exception from mule while to create xml file: {} and pilat id: {} ", e.getMessage(), asset.getPilatId());
					throw new Live2VodException(Messages.XML_FILE_NOT_PREPARED);
				}
				
				return response;
	}
	
	private String createMuleUrlForCreatingXmlFile(String basePathUrl, Asset asset) {
		String pilatOrMpxId = CommonUtils.isNotNullOrEmpty(asset.getPilatId()) ? asset.getPilatId() : asset.getMpxId();
		String outType = asset.getGeoTargeting().equalsIgnoreCase("Geo blocked") ? "GeoBlocked"
				: asset.getGeoTargeting().equalsIgnoreCase("Not Geo Blocked") ? "NonBlocking" : "Encrypted";
		return  basePathUrl+pilatOrMpxId+"?outputType="+outType+"&duration="+asset.getMp4VideoDuration();
	}
	
	private String callMuleRestApiToCreateXml(String requestUrl) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
		headers.setContentType(MediaType.APPLICATION_JSON);
		//HttpEntity<CreateFilterRequest> request = new HttpEntity<>(createFilterRequest, headers);
		logger.info("xml creation asset request:{} ", requestUrl);
		ResponseEntity<String> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, null, String.class);
		logger.info("xml creation asset response:{} and getStatus code value:{}", responseEntity, responseEntity.getStatusCodeValue());
		String response = null;
		if (responseEntity.getStatusCodeValue() == 200) {
			return Constants.SUCCESS;
			
		}
		return response;
	}
	
}
