package com.chirpn.live2vod.service.adgroupservice;

import java.util.List;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.entity.Group;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.model.GroupDTO;

/**
* 
* @author  Satish ghonge
*  
*/
public interface AdGroupService {
	
	Response<List<ChannelDTO>> createNewGroup(List<ChannelDTO> channelModel) throws Exception;
	
	void mapGroupEntityToGroupDto(GroupDTO groupDto, Group groupEntity);
	
	void mapGroupDtoToGroupEntity(GroupDTO groupDto, Group groupEntity);
}
