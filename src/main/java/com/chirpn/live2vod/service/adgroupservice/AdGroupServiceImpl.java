package com.chirpn.live2vod.service.adgroupservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.Group;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.model.GroupDTO;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.GroupRepository;
import com.chirpn.live2vod.service.channelservice.ChannelService;
import com.chirpn.live2vod.util.CommonUtils;

/**
* 
* @author  Satish ghonge
*  
*/
@Service
@Transactional(rollbackFor=Exception.class)
public class AdGroupServiceImpl implements AdGroupService {
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired 
	private GroupRepository groupRepository;
	
	@Autowired
	private ChannelService channelService;

	@Override
	public void mapGroupEntityToGroupDto(GroupDTO groupDto, Group groupEntity){
		groupDto.setGroupNo(groupEntity.getGroupNo());
		groupDto.setGroupName(groupEntity.getGroupName());
	}
	
	public void mapGroupDtoToGroupEntity(GroupDTO groupDto, Group groupEntity){
		if (groupEntity.getGroupNo() == null) {
			groupEntity.setGroupNo(CommonUtils.generateGroupNumber());
		}
		groupEntity.setGroupName(groupDto.getGroupName());
	}
	
	@Override
	public Response<List<ChannelDTO>> createNewGroup(List<ChannelDTO> channelModel) throws Exception{
		Response<List<ChannelDTO>> response = new Response<>();
		List<Channel> channels = new ArrayList<>();
		List<ChannelDTO> channelsDto = new ArrayList<>();
		channelModel.forEach(c -> {
			if (CommonUtils.isNotNullOrEmpty(c.getChannelNo()) && c.getGroups() != null) {
					 Channel sbsChannel = channelRepository.findByChannelNo(c.getChannelNo());
					 if (sbsChannel != null) {
						  List<Group> groups = new ArrayList<>();
						  c.getGroups().forEach(g -> {
							  Group group = groupRepository.findByChannelId(sbsChannel.getId());
							  if (group != null) {
								  group.setGroupName(g.getGroupName());
							  } else {
								 group = new Group();
								 mapGroupDtoToGroupEntity(g, group);
								 group.setChannel(sbsChannel);
								 
							 }
							 groups.add(group);
						 
						 sbsChannel.setGroups(new HashSet<>(groups));
						 channels.add(sbsChannel);
					 });
				}
			}
		});
		
		if (!channels.isEmpty()) {
			channelRepository.save(channels);
			channelsDto = channelService.channelEntityMapToChannelDto(channels);
			response.setData(channelsDto);
			response.setStatus(HttpStatus.OK);
	        response.setStatusMessage(HttpStatus.OK.name());
	        return response;
		}
		response.setData(channelsDto);
		response.setStatus(HttpStatus.NOT_FOUND);
        response.setStatusMessage(HttpStatus.NOT_FOUND.name());
        return response;
	}

}
