package com.chirpn.live2vod.audit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.chirpn.live2vod.security.SecurityServiceImpl;

@Component
public class AuditorAwareImpl implements AuditorAware<String> {

	protected static final Logger LOGGER = Logger.getLogger(AuditorAwareImpl.class);

	@Autowired
	private SecurityServiceImpl securityServiceImpl; 
	
	@Override
	public String getCurrentAuditor() {
		try {
			Object principal = securityServiceImpl.getLoggeInUser();
			if (principal != null) {
				UserDetails userDetails = (UserDetails) principal;
				if (userDetails != null) {
					return userDetails.getUsername();
				}
			}
			return "SCHEDULER";
		} catch (Exception e) {
			LOGGER.error("Exception while fetching logged in user from SecurityContextHolder." + e.getMessage());
		}
		return "SCHEDULER";
	}
}