package com.chirpn.live2vod.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chirpn.live2vod.common.exception.Live2VodException;

@Controller
public class RevokeTokenEndPoint {
    @Autowired
    TokenStore tokenStore;

    @RequestMapping(method = RequestMethod.DELETE, value = "/oauth/revoke-token")
    @ResponseBody
    public void revokeToken(HttpServletRequest request) throws Live2VodException {
	String tokenId = request.getParameter("token");
	OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenId);
	if (accessToken == null)
	    throw new AuthenticationCredentialsNotFoundException("Invalid Token");
	tokenStore.removeAccessToken(accessToken);
    }
}
