package com.chirpn.live2vod.security;


public enum UserType {
	DEFAULT, ADMINISTRATOR, OPERATOR
}
