package com.chirpn.live2vod.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

public class LdapUserDetailsServiceCustom extends LdapUserDetailsService {

	@Autowired
	public LdapUserDetailsServiceCustom(LdapUserSearch userSearch) {
		super(userSearch);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LdapUserDetailsImpl ldapUserDetailsImpl = (LdapUserDetailsImpl) super.loadUserByUsername(username);
		Collection<GrantedAuthority> grantAuthorities = ldapUserDetailsImpl.getAuthorities();

		List<String> groupNames = new ArrayList<String>();

		// get channels by lap groups the user is part of. And set to
		// 'LdapUserDetailsImplCustom'
		for (GrantedAuthority grantedAuthority : grantAuthorities) {
			groupNames.add(grantedAuthority.getAuthority());
		}
		return ldapUserDetailsImpl;
	}

	@Override
	public void setUserDetailsMapper(UserDetailsContextMapper userDetailsMapper) {
		super.setUserDetailsMapper(userDetailsMapper);
	}

}
