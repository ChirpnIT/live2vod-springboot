
package com.chirpn.live2vod.security;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Configuration
@ConfigurationProperties("live2vod")
public class AppProperties {
	
	@NotEmpty
    private String mountFolderPath;

	public String getMountFolderPath() {
		return mountFolderPath;
	}

	public void setMountFolderPath(String mountFolderPath) {
		this.mountFolderPath = mountFolderPath;
	}

	

	
}
