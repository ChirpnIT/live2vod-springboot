package com.chirpn.live2vod.security.ldap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;

@Profile("ldap")
@Configuration
public class LdapBeanConfig {

	@Value("${active.directory.search.baseOU.enabled}")
	private boolean activeDirectorySearchBaseOUEnabled = false;

	@Autowired
	private Environment env;

	@Autowired
	private UserDetailsContextMapper userDetailsContextMapper;
	
	@Autowired
	IntegrationPropertiesRepository integrationProperties;

	@Bean
	public LdapContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		List<IntegrationProperties> prop = integrationProperties.findAllByOrderByIdDesc();
		contextSource.setUrl(prop != null && !prop.isEmpty() && prop.get(0).getLdapUrl() != null && !prop.get(0).getLdapUrl().isEmpty() ? prop.get(0).getLdapUrl() : env.getProperty("ldap.url"));
		contextSource.setUserDn(env.getProperty("ldap.userDn"));
		contextSource.setPassword(env.getProperty("ldap.password"));
		return contextSource;
	}

	@Autowired
	@Bean
	public LdapTemplate ldapTemplate(LdapContextSource ldapContextSource) {
		LdapTemplate ldaptemplate = new LdapTemplate(ldapContextSource);
		return ldaptemplate;
	}

	@Autowired
	@Bean
	public LdapUserSearch getLdapUserSearch(LdapContextSource ldapContextSource) {
		LdapUserSearch ldapUserSearch = new FilterBasedLdapUserSearch(env.getProperty("active.directory.rootDn"), "(|(userprincipalname={0})(sAMAccountName={0}))",
				ldapContextSource);
		return ldapUserSearch;

	}

	@Bean
	public ActiveDirectoryLdapAuthenticationProvider getActiveDirectoryLdapAuthenticationProvider() {
		List<IntegrationProperties> prop = integrationProperties.findAllByOrderByIdDesc();
		ActiveDirectoryLdapAuthenticationProvider activeDirectoryLdapAuthenticationProvider = new ActiveDirectoryLdapAuthenticationProvider(
				env.getProperty("active.directory.domain"), prop != null && !prop.isEmpty() && prop.get(0).getLdapUrl() != null && !prop.get(0).getLdapUrl().isEmpty() ? prop.get(0).getLdapUrl() : env.getProperty("ldap.url"), env.getProperty("active.directory.rootDn"));
		activeDirectoryLdapAuthenticationProvider.setConvertSubErrorCodesToExceptions(true);
		activeDirectoryLdapAuthenticationProvider.setUseAuthenticationRequestCredentials(true);
		activeDirectoryLdapAuthenticationProvider.setAuthoritiesMapper(new SimpleAuthorityMapper());
		activeDirectoryLdapAuthenticationProvider.setUserDetailsContextMapper(userDetailsContextMapper);
		return activeDirectoryLdapAuthenticationProvider;
	}
	
	private String getSearchFilter() {
		return "(&(objectClass=user)(userPrincipalName={0}))";
	}


}
