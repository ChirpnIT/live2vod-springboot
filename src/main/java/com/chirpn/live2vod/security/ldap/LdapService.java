package com.chirpn.live2vod.security.ldap;

import java.util.List;

public interface LdapService {
	
	List<String> getGroups();

}
