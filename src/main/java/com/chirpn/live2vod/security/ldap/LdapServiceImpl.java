package com.chirpn.live2vod.security.ldap;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;

@Service
public class LdapServiceImpl implements LdapService {

	@Autowired
	private LdapTemplate ldapTemplate;

	@Override
	public List<String> getGroups() {
		return ldapTemplate.search(query().where("objectclass").is("group"), (AttributesMapper<String>) attributes -> attributes.get("cn").get().toString());
	}

}
