package com.chirpn.live2vod.security.ldap;

import java.util.Collection;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.stereotype.Component;

@Component
public class LdapUserDetailsMapperCustom extends LdapUserDetailsMapper {

	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
		return super.mapUserFromContext(ctx, username, authorities);
	}

	@Override
	public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
		super.mapUserToContext(user, ctx);
	}

	@Override
	protected String mapPassword(Object passwordValue) {
		return super.mapPassword(passwordValue);
	}

	@Override
	protected GrantedAuthority createAuthority(Object role) {
		return super.createAuthority(role);
	}

	@Override
	public void setConvertToUpperCase(boolean convertToUpperCase) {
		super.setConvertToUpperCase(convertToUpperCase);
	}

	@Override
	public void setPasswordAttributeName(String passwordAttributeName) {
		super.setPasswordAttributeName(passwordAttributeName);
	}

	@Override
	public void setRoleAttributes(String[] roleAttributes) {
		super.setRoleAttributes(roleAttributes);
	}

	@Override
	public void setRolePrefix(String rolePrefix) {
		super.setRolePrefix(rolePrefix);
	}

}
