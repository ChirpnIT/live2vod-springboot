package com.chirpn.live2vod.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public interface SecurityService {

	/*
	 * Get userDetails object for current logged in user from SecurityContext
	 */
	UserDetails getLoggeInUser();

	/*
	 * Get list all administrator roles from properties files and integration
	 * properties
	 */
	List<String> getAdministratorRoles();

	/*
	 * Is currently logged in user belongs to administrator group
	 */
	Boolean isAdministrator();

	Boolean isAdministrator(List<String> userRoles);

	Boolean isOperator(List<String> userRoles);

	/*
	 * Get string list of roles for currently logged in user
	 */
	List<String> getUserRoles();

	List<String> getUserRoles(Collection<GrantedAuthority> authotities);

	Boolean hasAccessToTheChannel(String channelNo);

}
