package com.chirpn.live2vod.security;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;

public class DefaultAccessTokenConverterCustom extends DefaultAccessTokenConverter {

	final String USER_DETAILS = "userDetails";
	final String USER_TYPE = "userType";

	private SecurityService securityService;

	public DefaultAccessTokenConverterCustom(SecurityService securityService) {
		super();
		this.securityService = securityService;
	}

	@Override
	public Map<String, ?> convertAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		Map<String, Object> response = new LinkedHashMap<>();
		response.remove("user_name");
		response.put(USER_DETAILS, authentication.getPrincipal());

		if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
			response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
		}

		if (securityService.isAdministrator(securityService.getUserRoles(authentication.getAuthorities()))) {
			response.put(USER_TYPE, UserType.ADMINISTRATOR);
		} else if (securityService.isOperator(securityService.getUserRoles(authentication.getAuthorities()))) {
			response.put(USER_TYPE, UserType.OPERATOR);
		} else {
			response.put(USER_TYPE, UserType.DEFAULT);
		}

		return response;
	}
}
