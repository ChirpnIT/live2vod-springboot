package com.chirpn.live2vod.security;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.constants.Messages;
import com.chirpn.live2vod.common.exception.UnauthorizedChannelAccessException;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.repository.AssetRepository;

@Aspect
@Configuration
public class ChannelAccessAspect {

	@Autowired
	private SecurityService securityService;

	@Autowired
	private AssetRepository assetRepository;

	@Before("execution(* com.chirpn.live2vod.api.AssetsController.saveAsset(..))")
	public void beforeSaveAsset(JoinPoint joinPoint) throws UnauthorizedChannelAccessException {
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			AssetDTO assetDTO = (AssetDTO) args[0];
			if (!securityService.hasAccessToTheChannel(assetDTO.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
			}
		}
	}

	@AfterReturning(pointcut = "execution(* com.chirpn.live2vod.api.AssetsController.getAssetByAssetNo(..))", returning="resultObject")
	public void beforegetAssetByAssetNo(Object resultObject) {
		ResponseEntity<Response<AssetDTO>> responseEntity = (ResponseEntity<Response<AssetDTO>>) resultObject;
		Response<AssetDTO> response = responseEntity.getBody();
		if (response.getData() != null) {
			AssetDTO assetDTO = response.getData();
			if (!securityService.hasAccessToTheChannel(assetDTO.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
			}
		}
	}

	@Before("execution(* com.chirpn.live2vod.api.AssetsController.updateAsset(..))")
	public void beforeUpdaetAsset(JoinPoint joinPoint) throws Exception {
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			String assetNo = (String) args[0];
			Asset asset = assetRepository.findByAssetNo(assetNo);
			if (asset != null && !securityService.hasAccessToTheChannel(asset.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
				
			}
		}
	}

	@Before("execution(* com.chirpn.live2vod.api.AssetsController.cancelScheduledAsset(..))")
	public void beforeCancelScheduledAsset(JoinPoint joinPoint) throws Exception {
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			String assetNo = (String) args[0];
			Asset asset = assetRepository.findByAssetNo(assetNo);
			if (asset != null && !securityService.hasAccessToTheChannel(asset.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
				
			}
		}
	}

	@Before("execution(* com.chirpn.live2vod.api.AssetsController.publishAsset(..))")
	public void beforePublishAsset(JoinPoint joinPoint) throws Exception {
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			String assetNo = (String) args[0];
			Asset asset = assetRepository.findByAssetNo(assetNo);
			if (asset != null && !securityService.hasAccessToTheChannel(asset.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
				
			}
		}
	}

	@Before("execution(* com.chirpn.live2vod.api.AssetsController.deleteAssets(..))")
	public void beforeDeleteAsset(JoinPoint joinPoint) throws Exception {
		Object[] args = joinPoint.getArgs();
		if (args != null && args.length > 0) {
			String assetNo = (String) args[0];
			Asset asset = assetRepository.findByAssetNo(assetNo);
			if (asset != null && !securityService.hasAccessToTheChannel(asset.getChannel().getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
				
			}
		}
	}
	
	@AfterReturning(pointcut = "execution(* com.chirpn.live2vod.api.ChannelController.retrieve(String))", returning = "resultObject")
	public void afterRetrivingChannel(Object resultObject) throws Exception {
		ResponseEntity<Response<ChannelDTO>> responseEntity = (ResponseEntity<Response<ChannelDTO>>) resultObject;
		Response<ChannelDTO> response = responseEntity.getBody();
		if (response.getData() != null) {
			ChannelDTO channelDTO = response.getData();
			if (!securityService.hasAccessToTheChannel(channelDTO.getChannelNo())) {
				throw new UnauthorizedChannelAccessException(Messages.UNAUTHORIZED_CHANNEL_ACCESS);
			}
		}
	}

}
