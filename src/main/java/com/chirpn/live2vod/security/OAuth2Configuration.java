package com.chirpn.live2vod.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@Configuration
public class OAuth2Configuration {

	@Autowired
	DataSource dataSource;

	@Value("classpath:oauth2/schema.sql")
	private Resource schemaScript;

	@Autowired
	private ClientDetailsService clientDetailsService;

	@Autowired
	private SecurityService securityService;

	private static final int ACCESS_TOKEN_VALIDITY_SECONDS = 60; // * 60 * 12; // default
	// 12
	// hours.
	private static final int FREFRESH_TOKEN_VALIDITY_SECONDS = 60 * 60 * 24 * 30; // default

	@Bean
	public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
		DataSourceInitializer initializer = new DataSourceInitializer();
		initializer.setDataSource(dataSource);
		initializer.setDatabasePopulator(databasePopulator());
		return initializer;
	}

	private DatabasePopulator databasePopulator() {
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScript(schemaScript);
		return populator;
	}

	@Bean
	@Autowired
	public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore) {
		TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		handler.setTokenStore(tokenStore);
		handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
		handler.setClientDetailsService(clientDetailsService);
		return handler;
	}

	@Bean
	@Autowired
	public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
		TokenApprovalStore store = new TokenApprovalStore();
		store.setTokenStore(tokenStore);
		return store;
	}

	@Bean
	public TokenStore tokenStore() {
		TokenStore tokenStore = new JdbcTokenStore(dataSource);
		return tokenStore;
	}

	@Primary
	@Autowired
	@Bean
	public ResourceServerTokenServices getResourceServerTokenServices(TokenStore tokenStore) {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore);
		defaultTokenServices.setAccessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_SECONDS);
		defaultTokenServices.setRefreshTokenValiditySeconds(FREFRESH_TOKEN_VALIDITY_SECONDS);
		defaultTokenServices.setReuseRefreshToken(true);
		defaultTokenServices.setSupportRefreshToken(true);
		return defaultTokenServices;
	}

	@Bean
	public AccessTokenConverter getAccessTokenConverter() {
		AccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverterCustom(securityService);
		return accessTokenConverter;
	}

	@Bean
	public WebResponseExceptionTranslator getWebResponseExceptionTranslator() {
		WebResponseExceptionTranslator exceptionTranslator = new DefaultWebResponseExceptionTranslator();
		return exceptionTranslator;
	}
}
