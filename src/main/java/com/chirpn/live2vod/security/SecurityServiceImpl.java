package com.chirpn.live2vod.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.chirpn.live2vod.entity.Channel;
import com.chirpn.live2vod.entity.Group;
import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.repository.IntegrationPropertiesRepository;

@Service
public class SecurityServiceImpl implements SecurityService {

	@Autowired
	private ChannelRepository channelRepository;

	@Value("#{'${live2vod.sbs.administrator.roles}'.split(',')}")
	private Set<String> live2vod_sbs_administrator_roles = new HashSet<String>();

	@Autowired
	private IntegrationPropertiesRepository integrationPropertiesRepository;

	@Override
	public List<String> getAdministratorRoles() {
		Set<String> administrativeGroups = new HashSet<String>();

		// Get administrator group from integration properties
		List<IntegrationProperties> integrationProperties = integrationPropertiesRepository.findAll();
		if (integrationProperties != null && !integrationProperties.isEmpty()) {
			IntegrationProperties integrationProperty = integrationProperties.get(0);
			String adminAdGroup = integrationProperty.getAdminADGroup();
			if (adminAdGroup != null && !adminAdGroup.isEmpty()) {
				administrativeGroups.add(adminAdGroup);
			}
		}

		// Get administrator from properties file
		if (live2vod_sbs_administrator_roles != null && !live2vod_sbs_administrator_roles.isEmpty()) {
			administrativeGroups.addAll(live2vod_sbs_administrator_roles);
		}
		return new ArrayList<String>(administrativeGroups);
	}

	@Override
	public Boolean isAdministrator() {
		List<String> userRoles = this.getUserRoles();
		return this.isAdministrator(userRoles);
	}

	@Override
	public Boolean isAdministrator(List<String> userRoles) {
		List<String> administratorRoles = getAdministratorRoles();
		for (String userRole : userRoles) {
			if (administratorRoles.contains(userRole)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Boolean isOperator(List<String> userRoles) {
		if (!this.isAdministrator(userRoles)) {
			return true;
		}
		return false;
	}

	@Override
	public UserDetails getLoggeInUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			Object principal = auth.getPrincipal();
			if (principal != null) {
				UserDetails userDetails = (UserDetails) principal;
				if (userDetails != null) {
					return userDetails;
				}
			}
		}
		return null;
	}

	@Override
	public List<String> getUserRoles() {
		UserDetails usereDetails = getLoggeInUser();
		List<String> roles = new ArrayList<String>();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) usereDetails.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			roles.add(grantedAuthority.getAuthority());
		}
		return roles;
	}

	@Override
	public List<String> getUserRoles(Collection<GrantedAuthority> authotities) {
		List<String> roles = new ArrayList<String>();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authotities;
		for (GrantedAuthority grantedAuthority : authorities) {
			roles.add(grantedAuthority.getAuthority().replace("ROLE_", ""));
		}
		return roles;
	}

	@Override
	public Boolean hasAccessToTheChannel(String channelNo) {
		if (this.isAdministrator()) {
			return true;
		}
		Channel channel = channelRepository.findByChannelNo(channelNo);
		String channelRole = null;
		if (channel != null && !channel.getGroups().isEmpty()) {
				Iterator<Group> iterator = channel.getGroups().iterator();
				if (iterator.hasNext()) {
					Group group = iterator.next();
					if (group.getGroupName() != null && !group.getGroupName().isEmpty()) {
						channelRole = group.getGroupName();
					}
				}
			
		}
		if (channelRole != null) {
			List<String> userRoles = getUserRoles();
			if (userRoles.contains(channelRole)) {
				return true;
			}
		}
		return false;
	}
}
