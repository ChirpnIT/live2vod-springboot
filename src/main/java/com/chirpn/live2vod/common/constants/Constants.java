package com.chirpn.live2vod.common.constants;

/**
* 
* @author  Satish ghonge
*  
*/
public class Constants {

	public static final String USERNAME = "";
	public static final String SUCCESS = "SUCCESS";
	public static final String MULE_BASE_PATH = "/sbs/digital/live2vod/";
	public static final String MULE_URL_EXTRACT_ASSETS_CUT_POINT = "createfilter/";
	public static final String DELTA_REQUEST_TYPE = "Delta";
	public static final String MPX_REQUEST_TYPE = "Mpx";
	public static final String PREVIEW_STREAM = "preview_stream";
	public static final String EXTRACT_STREAM = "extract_stream";
	public static final String FILTER = "filter";
	public static final String MATCHING_NOT_FOUND= " matching nodes found";
	public static final String NODE_TEXT_CONTEXT= "Node text content: ";
	public static final String FOUND_LABLE_NODE_REQUEST_VALUE="Found label node with requested value '";
	public static final String NODE_VALUE= "Node value: ";
	public static final String NODE = "Node: ";
	public static final String IS_NOT_A_REQUEST= " is not request value: ";
	public static final String IS_NOT_A_LABEL_NODE= " is not label node";
	public static final String DOCUMENT_OBJECT_IS_EMPTY="Document object is empty";
	public static final String EXCEPTION_PARSING_XML="Exception parsing xml: ";
	public static final String M3U8_FILE =".m3u8";
	public static final String HTTP ="http";
	public static final String HTTPS ="https";
	public static final String FALSE = "false";
	public static final String LABLE="label";
	public static final String CHANGE_PILAT_ID="PILAT_ID_CHANGED";
	public static final String MPX_PILAT_ID="MPX_ID_CHANGED";
	public static final String DELTAHOST = "?deltaHost=";
	public static final String DELTA_URL_FLAG= "&deltaUrlFlag=";
	public static final String MULE_URL_XML_CREATION_CUT_POINT = "prepareExport/";
}
