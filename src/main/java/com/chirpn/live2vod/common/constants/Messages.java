package com.chirpn.live2vod.common.constants;

/**
 * 
 * @author Satish ghonge
 * 
 */
public class Messages {

	 public static final String UNAUTHORIZED = "Userdetails not found";
	 public static final String INVALID_USER_ID = "Invalid User id";
	 public static final String ACTIVITY_NOT_FOUND = "Activity not found";
	 public static final String ERROR_WHILE_PROCESSING_RETRIEVE_ASSETS = "Error while processing retrieve assets";
	 public static final String ASSETS_NOT_FOUND = "Assets not found";
	 public static final String CHANNEL_NOT_FOUND = "Channel not found";
	 public static final String RECORD_NOT_FOUND = "Record not found";
	 public static final String CHANNEL_LIST = "Channel list should not be empty";
	 public static final String GROUP_LIST = "Group list should not be empty";
	 public static final String PROPERTIES_INTEGRATION_SHOULD_NOT_BE_NULL = "Integration peoperties list should not be empty";
	 public static final String INTEGRATION_PROP_UPDATE_SUCCESSFULLY="Update integration properties sucessfully";
	 public static final String DELTAEXCEPTION = "Please call some time, delta server is not responding";
	 public static final String ASSETS_NO_IS_REQUIRED = "Assets number is required";
	 public static final String ASSETS_NOT_FOUND_BASED_ON_ASSETS_ID = "Asset can not deleted. invalid asset id";
	 public static final String ASSETS_DELETED = "Asset deleted sucessfully";
	 public static final String MPXEXCEPTION = "Please call some time, mpx server is not responding";
	 public static final String ASSETS_NOT_IN_READY_TO_PUBLISH_STATE = "Assets can not be published. Not in Ready To Publish state";
	 public static final String ASSETS_NOT_PUBLISHED = "Assets is not published";
	 public static final String ASSETS_IS_BEING_PUBLISHED = "Assets is being published";
	 public static final String ASSET_CAN_NOT_BE_CANCELLED = "Assets is not in scheduled state. Can not be cancelled";
	 public static final String ASSET_ALREADY_EXISTS_WITH_PILAT_ID = "Assets already exists with pilat id";
	 public static final String ASSET_ALREADY_EXISTS_WITH_MPX_ID = "Assets already exists with mpx id";
	 public static final String INTEGRATION_PROPERTY_UNAVAILABLE = "Please set Integration property from Setting";
	 public static final String UNAUTHORIZED_CHANNEL_ACCESS = "User has no permission to access this channel";
	 public static final String CHANNEL_NO_IS_REQUIRED = "Channel number is required";
	 public static final String HLS_VIDEO_PREVIEW_STREAM_NOT_AVAILABLE = "Preview stream is not available";
	 public static final String GEO_TRAGETING_SHOULD_NOT_BE_NULL = "Geo trageting should not be null";
	 public static final String DELTA_STREAM_ID_SHOULD_NOT_BE_NULL = "Delta stream id should not be null";
	 public static final String DELTA_STREAM_FAIL_OVER_ID_SHOULD_NOT_BE_NULL = "Delta stream fail over id should not be null";
	 public static final String PILAT_ID_EXIST_WITH_DIFFERENT_ASSETS = "Pilat id already exists with other assets";
	 public static final String MPX_ID_EXIST_WITH_DIFFERENT_ASSETS = "Mpx id already exists with other assets";
	 public static final String ASSET_IS_NEAR_TO_FOUR_MINUTE_BOUNDARY = "Asset is not update. Asset is within 4 minute boundary.";
	 public static final String ASSET_JSON_SHOULD_NOT_BE_NULL = "Asset json should not be null.";
	 public static final String XML_FILE_NOT_PREPARED = "Please call some time, mule not responding";
	 public static final String ASSET_IS_DELETED_FROM_DELTA_SERVER = "Asset can't not publish, its has been deleted from delta server";
	 


}
