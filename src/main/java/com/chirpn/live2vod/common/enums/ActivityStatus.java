package com.chirpn.live2vod.common.enums;

/**
* 
* @author  Satish ghonge
*  
*/
public enum ActivityStatus {
	CREATE_USER,
	UPDATE_USER;

}
