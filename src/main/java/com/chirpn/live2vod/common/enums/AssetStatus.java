package com.chirpn.live2vod.common.enums;

/**
 * 
 * @author Satish ghonge
 * 
 */
public enum AssetStatus {
	SCHEDULED,
	EXTRACTING,
	READY_TO_PUBLISH, 
	CANCELLED,
	PUBLISHED,
	DELETED,
	SUBMITTED_FOR_EXTRACTION,
	SUBMITTED_FOR_PUBLICATION,
	FAILED_TO_EXTRACT,
	FAILED_TO_PUBLISH
}
