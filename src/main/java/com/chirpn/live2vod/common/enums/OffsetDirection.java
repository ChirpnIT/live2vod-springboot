package com.chirpn.live2vod.common.enums;


public enum OffsetDirection {
	NEGATIVE,
	POSITIVE
}
