package com.chirpn.live2vod.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class Live2VodException extends RuntimeException  {

	private static final long serialVersionUID = -7140228936006447533L;

	public Live2VodException(String string) {
		super(string);
	}
}
