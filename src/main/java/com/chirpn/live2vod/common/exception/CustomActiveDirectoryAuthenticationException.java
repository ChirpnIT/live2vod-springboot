package com.chirpn.live2vod.common.exception;

import org.springframework.security.core.AuthenticationException;


@SuppressWarnings("serial")
public final class CustomActiveDirectoryAuthenticationException extends AuthenticationException {
	private final String dataCode;

	public CustomActiveDirectoryAuthenticationException(String dataCode, String message,
			Throwable cause) {
		super(message, cause);
		this.dataCode = dataCode;
	}

	public String getDataCode() {
		return dataCode;
	}
}