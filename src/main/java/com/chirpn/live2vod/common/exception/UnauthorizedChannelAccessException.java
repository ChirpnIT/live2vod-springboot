package com.chirpn.live2vod.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UnauthorizedChannelAccessException extends Live2VodException  {

	private static final long serialVersionUID = 2543296244153926230L;

	public UnauthorizedChannelAccessException(String string) {
		super(string);
	}
}
