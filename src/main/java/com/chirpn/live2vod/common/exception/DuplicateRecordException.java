package com.chirpn.live2vod.common.exception;

public class DuplicateRecordException extends RuntimeException {
   /**
	 * 
	 */
	private static final long serialVersionUID = 2109680434347434555L;

public DuplicateRecordException(String message){
	   super(message);
   }
}
