package com.chirpn.live2vod.common.model;

public abstract class AbstractJsonEntity {
	public AbstractJsonEntity() {
		super();
	}

	protected boolean enable = true;

	public abstract boolean isEnable();

	public abstract void setEnable(boolean enable);
}
