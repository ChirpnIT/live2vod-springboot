package com.chirpn.live2vod.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.chirpn.live2vod.service.muleintegrationservice.ChirpnMuleIntegrationServiceImpl;
import com.chirpn.live2vod.service.muleintegrationservice.MuleIntegrationService;
import com.chirpn.live2vod.service.muleintegrationservice.SBSMuleIntegrationServiceImpl;

@Configuration
public class ProfileConfiguration {

	@Profile("chirpn")
	@Bean
	public MuleIntegrationService getChirpnMuleIntegrationService() {
		return new ChirpnMuleIntegrationServiceImpl();
	}

	@Profile("sbs")
	@Bean
	public MuleIntegrationService getSBSMuleIntegrationService() {
		return new SBSMuleIntegrationServiceImpl();
	}

}
