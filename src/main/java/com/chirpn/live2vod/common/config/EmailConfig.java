/*
 * package com.chirpn.live2vod.common.config;
 * 
 * import java.util.Properties;
 * 
 * import javax.mail.Authenticator; import javax.mail.Message; import
 * javax.mail.MessagingException; import javax.mail.PasswordAuthentication;
 * import javax.mail.Session; import javax.mail.internet.InternetAddress; import
 * javax.mail.internet.MimeMessage;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.beans.factory.annotation.Value; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.context.annotation.Configuration; import
 * org.springframework.context.annotation.Profile;
 * 
 * @Configuration public class EmailConfig {
 * 
 * @Value("${spring.mail.default-encoding}") private String
 * mail_default_encoding;
 * 
 * @Value("${spring.mail.host}") private String mail_host;
 * 
 * @Value("${spring.mail.port}") private String mail_port;
 * 
 * @Value("${spring.mail.protocol}") private String mail_protocol;
 * 
 * @Value("${spring.mail.username}") private String mail_username;
 * 
 * @Value("${spring.mail.password}") private String mail_password;
 * 
 * @Value("${spring.mail.from}") private String mail_from;
 * 
 * @Value("${spring.mail.ssl.trust}") private String mail_ssl_trust;
 * 
 * @Value("${spring.mail.smtp.auth}") private String mail_smtp_auth;
 * 
 * @Value("${spring.mail.smtp.starttls.enable}") private String
 * mail_smtp_starttls_enable;
 * 
 * public Properties getSbsEmailProperties() { Properties emailProperties = new
 * Properties(); emailProperties.put("mail.smtp.host", mail_host);
 * emailProperties.put("mail.smtp.ssl.trust", mail_ssl_trust); return
 * emailProperties; }
 * 
 * public Properties getChirpnEmailProperties() { Properties emailProperties =
 * new Properties(); emailProperties.put("mail.smtp.host", mail_host);
 * emailProperties.put("mail.smtp.port", mail_port);
 * emailProperties.put("mail.smtp.auth", mail_smtp_auth);
 * emailProperties.put("mail.smtp.ssl.trust", mail_ssl_trust);
 * emailProperties.put("mail.smtp.starttls.enable", mail_smtp_starttls_enable);
 * return emailProperties; }
 * 
 * public Authenticator getChirpnAuthenticator() { Authenticator authenticator =
 * new Authenticator() { public PasswordAuthentication
 * getPasswordAuthentication() { return new
 * PasswordAuthentication(mail_username, mail_password); } }; return
 * authenticator; }
 * 
 * @Profile("chirpn")
 * 
 * @Bean public Session getChirpnSession() { return
 * Session.getInstance(getChirpnEmailProperties(), getChirpnAuthenticator()); }
 * 
 * @Profile("sbs")
 * 
 * @Bean public Session getSbsSession() { return
 * Session.getInstance(getKtasEmailProperties()); }
 * 
 * @Autowired
 * 
 * @Bean(name = "mimeMessage") public Message getMessage(Session session) throws
 * MessagingException { Message message = new MimeMessage(session);
 * message.setFrom(new InternetAddress(mail_from));
 * message.setHeader("Content-Type", "multipart/mixed"); return message; } }
 */