package com.chirpn.live2vod.common.config;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.chirpn.live2vod.audit.AuditorAwareImpl;

@Configuration
@EnableJpaAuditing(auditorAwareRef="auditorAware")
public class JpaConfig {

	@PostConstruct
	private void initializeDBForAdminEntry(){}
	
	@Bean
    AuditorAware<String> auditorAware() {
        return new AuditorAwareImpl();
    }
}
