package com.chirpn.live2vod.common.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.chirpn.live2vod.entity.IntegrationProperties;
import com.chirpn.live2vod.model.IntegrationPropertiesDTO;

@Configuration
public class ModelMapperConfig extends WebMvcConfigurerAdapter {

	@Bean("integrationPropertiesMapper")
	public ModelMapper getIntegrationPropertiesMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
		
		PropertyMap<IntegrationProperties, IntegrationPropertiesDTO> integrationPropertiesMap = new PropertyMap<IntegrationProperties, IntegrationPropertiesDTO>() {

			protected void configure() {

					map().setId(source.getId());


					map().setLdapUrl(source.getLdapUrl());


					map().setMuleSoft(source.getMuleSoft());


					map().setDeltaUrl(source.getDeltaUrl());


					map().setMpxUrl(source.getMpxUrl());


					map().setOffsetTime(source.getOffsetTime());

					map().setFailOverOffsetTime(source.getFailOverOffsetTime());
			}
		};

		modelMapper.addMappings(integrationPropertiesMap);
		return modelMapper;
	}
}
