package com.chirpn.live2vod.common.validation;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LocalTimeSerializer extends StdSerializer<LocalTime> {

	private static final long serialVersionUID = 4029193928839440863L;

	protected LocalTimeSerializer() {
		super(LocalTime.class, false);
	}

	@Override
	public void serialize(LocalTime value, JsonGenerator generator, SerializerProvider provider) throws IOException {
		generator.writeString(value.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
	}

}
