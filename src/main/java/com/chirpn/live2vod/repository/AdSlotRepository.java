package com.chirpn.live2vod.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chirpn.live2vod.entity.AdSlot;
import com.chirpn.live2vod.entity.Asset;

public interface AdSlotRepository extends JpaRepository<AdSlot, Long> {

	AdSlot findByAdSlotNo(String adSlotNo) throws Exception;
	void deleteByAsset(Asset asset);

}
