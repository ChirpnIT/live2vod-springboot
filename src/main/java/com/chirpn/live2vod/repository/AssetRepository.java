package com.chirpn.live2vod.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.entity.Asset;
import com.chirpn.live2vod.entity.Channel;

/**
 * @author awalunj
 *
 */
@Repository
public interface AssetRepository extends PagingAndSortingRepository<Asset, Long> {

	
	Page<Asset> findByDateLessThan(LocalDate date, Pageable pageable) throws Exception;
	
	@Query("SELECT a FROM Asset a WHERE a.date IN :dates")
	Page<Asset> findByDateIn(@Param("dates") List<LocalDate> threeDayDates, Pageable pageable) throws Exception;
	
	Asset findByAssetNo(String assetNo) throws Exception;
	
	boolean existsByPilatIdAndDateGreaterThan(String pilatId, LocalDate aMonthOldDate) throws Exception;
	
	boolean existsByMpxIdAndDateGreaterThan(String pilatId, LocalDate aMonthOldDates) throws Exception;
	
	Page<Asset> findByChannelChannelNo(String channelNo, Pageable pageable) throws Exception;
	
	@Query("SELECT a FROM Asset a WHERE a.channel.channelNo =:channelNo AND a.date IN :dates")
	Page<Asset> findByChannelChannelNoAndDateIn(@Param("channelNo") String channelNo, @Param("dates") List<LocalDate> threeDayDates, Pageable pageable) throws Exception;

	Page<Asset> findByDate(LocalDate date, Pageable pageable) throws Exception;
	
	Page<Asset> findByStatus(AssetStatus status, Pageable pageable) throws Exception;
	
	@Query("SELECT a FROM Asset a WHERE a.status =:status AND a.date IN :dates")
	Page<Asset> findByStatusAndDateIn(@Param("status")AssetStatus status, @Param("dates") List<LocalDate> threeDayDates, Pageable pageable) throws Exception;

	Page<Asset> findByChannelChannelNoAndDate(String channelNo, LocalDate date, Pageable pageable) throws Exception;

	Page<Asset> findByChannelChannelNoAndStatus(String channelNo, AssetStatus status, Pageable pageable) throws Exception;
	
	Page<Asset> findByDateAndStatus(LocalDate date, AssetStatus status, Pageable pageable) throws Exception;

	Page<Asset> findByChannelChannelNoAndDateAndStatus(String channelNo, LocalDate date, AssetStatus status, Pageable pageable) throws Exception;
	
	Page<Asset> findByChannelChannelNoAndDateLessThan(String channelNo, LocalDate date, Pageable pageable) throws Exception;
	
//	@Query("FROM Asset a WHERE ((a.date =:lastDate AND a.endTime < '23:59:59') OR (a.date =:currentDate AND a.endTime < :endTime)) AND (a.status = 0 OR a.status = 6) AND a.failAttempt < 3")
//	List<Asset> findByStartDateStartTimeAndEndDateAndEndTime(@Param("lastDate") LocalDate lastDate,  @Param("currentDate") LocalDate currentDate, @Param("endTime") LocalTime endTime) throws Exception;
//	
//	@Query("FROM Asset a WHERE ((a.date =:lastDate AND a.endTime < '23:59:59') OR (a.date =:currentDate AND a.endTime < :endTime)) AND (a.status = 2 OR a.status = 7) AND a.failAttempt < 4")
//	List<Asset> findByStartDateStartTimeAndEndDateAndEndTimeAndStatus(@Param("lastDate") LocalDate lastDate,  @Param("currentDate") LocalDate currentDate, @Param("endTime") LocalTime endTime) throws Exception;
	
	@Query("FROM Asset a WHERE ((a.endDate <=:currentDate AND a.endTime < :endTime) OR (a.endDate <:currentDate AND a.endTime < '23:59:59')) AND (a.status = 0 OR a.status = 6) AND a.failAttempt < 3")
	List<Asset> findByEndDateAndEndTime(@Param("currentDate") LocalDate currentDate, @Param("endTime") LocalTime endTime) throws Exception;
	
	@Query("FROM Asset a WHERE ((a.endDate <=:currentDate AND a.endTime < :endTime) OR (a.endDate <:currentDate AND a.endTime < '23:59:59')) AND (a.status = 2 OR a.status = 7) AND a.failAttempt < 4")
	List<Asset> findByEndDateAndEndTimeAndStatus(@Param("currentDate") LocalDate currentDate, @Param("endTime") LocalTime endTime) throws Exception;
	
	@Modifying
	@Transactional
	@Query(value="delete from Asset a where a.assetNo = :assetNo")
	void deleteByAssetNo(@Param("assetNo") String assetNo);
	
	@Modifying
	@Transactional
	@Query(value="update Asset a set a.deleted = true where a.channel= :channel and a.deltaUrlFlag = :flag and a.date < :date")
	void updateByChannelAndDate(@Param("channel") Channel channel, @Param("date") LocalDate date, @Param("flag") boolean flag);
	
	
	@Query("FROM Asset a WHERE  a.pilatId= :pilatId AND (a.status = 6 OR a.status = 7)") 
	List<Asset> existByPilatIdAndStatus(@Param("pilatId") String pilatId) throws Exception;
	  
	@Query("FROM Asset a WHERE  a.mpxId= :mpxId AND (a.status = 6 OR a.status = 7)") 
	List<Asset> existByMpxIdAndStatus(@Param("mpxId") String mpxId) throws Exception;
	
	Asset findByPilatId(String pilatId) throws Exception;
	
	@Query("FROM Asset a WHERE (a.status in(3,4,8,9)) AND a.deleted = false AND a.lastModifiedDate < :date") 
	List<Asset> retrieveByStatus(@Param("date") Date date) throws Exception;
	
	@Query("FROM Asset a WHERE a.status = 1 AND a.failAttempt < 20 and exportingXmlFileFlag = false")
	List<Asset> findByStatus() throws Exception;
	
	@Query("FROM Asset a WHERE a.status = 1 AND a.failAttempt < 15 and exportingXmlFileFlag = true")
	List<Asset> findByStatusExportingXmlFileFlag() throws Exception;
	
	
}
