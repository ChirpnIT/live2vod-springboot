package com.chirpn.live2vod.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.entity.IntegrationProperties;

/**
* 
* @author  Satish ghonge
*  
*/
@Repository
public interface IntegrationPropertiesRepository extends JpaRepository<IntegrationProperties, Long> {

	IntegrationProperties findTopByOrderById() throws Live2VodException;
	
	List<IntegrationProperties> findAllByOrderByIdDesc();

}
