package com.chirpn.live2vod.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chirpn.live2vod.entity.Group;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

	Group findByGroupName(String groupName);

	Group findByChannelId(Long channel);
	
	Group findByGroupNo(String grouopNumber);

}
