package com.chirpn.live2vod.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chirpn.live2vod.entity.Channel;

/**
 * 
 * @author Satish ghonge
 * 
 */
@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {

	Channel findByChannelNo(String channelNo);

	@Query("SELECT c FROM Channel c JOIN c.groups g WHERE g.groupName IN :groupNames")
	List<Channel> findByGroups(@Param("groupNames") List<String> authorities);

	@Transactional
	Long deleteByChannelNo(String channelNo);
}
