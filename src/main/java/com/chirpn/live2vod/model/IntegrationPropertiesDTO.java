package com.chirpn.live2vod.model;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.entity.IntegrationProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Satish ghonge
 * 
 */
public class IntegrationPropertiesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 236116000196218736L;

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationPropertiesDTO.class);

	private Long id;

	private String adminADGroup;

	private String ldapUrl;

	private String muleSoft;

	private String deltaUrl;

	private String deltaFailOverUrl;

	private String mpxUrl;

	private String mpxFailOverUrl;
	
	private Boolean deltaUrlFlag;
	
	private Boolean mpxUrlFlag;
	
	private long offsetTime;
	
	private long failOverOffsetTime;

	public IntegrationPropertiesDTO() {
		super();
	}

	public IntegrationPropertiesDTO(IntegrationProperties integrationProperties) {
		super();
		this.id = integrationProperties.getId();
		this.adminADGroup = integrationProperties.getAdminADGroup();
		this.ldapUrl = integrationProperties.getLdapUrl();
		this.muleSoft = integrationProperties.getMuleSoft();
		this.deltaUrl = integrationProperties.getDeltaUrl();
		this.deltaFailOverUrl = integrationProperties.getDeltaFailOverUrl();
		this.mpxUrl = integrationProperties.getMpxUrl();
		this.mpxFailOverUrl = integrationProperties.getMpxFailOverUrl();
		this.offsetTime = integrationProperties.getOffsetTime();
		this.deltaUrlFlag = integrationProperties.getDeltaUrlFlag();
		this.mpxUrlFlag = integrationProperties.getMpxUrlFlag();
		this.failOverOffsetTime = integrationProperties.getFailOverOffsetTime();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdminADGroup() {
		return adminADGroup;
	}

	public void setAdminADGroup(String adminADGroup) {
		this.adminADGroup = adminADGroup;
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}

	public String getMuleSoft() {
		return muleSoft;
	}

	public void setMuleSoft(String muleSoft) {
		this.muleSoft = muleSoft;
	}

	public String getDeltaUrl() {
		return deltaUrl;
	}

	public void setDeltaUrl(String deltaUrl) {
		this.deltaUrl = deltaUrl;
	}

	public String getMpxUrl() {
		return mpxUrl;
	}

	public void setMpxUrl(String mpxUrl) {
		this.mpxUrl = mpxUrl;
	}

	public String getDeltaFailOverUrl() {
		return deltaFailOverUrl;
	}

	public void setDeltaFailOverUrl(String deltaFailOverUrl) {
		this.deltaFailOverUrl = deltaFailOverUrl;
	}

	public String getMpxFailOverUrl() {
		return mpxFailOverUrl;
	}

	public void setMpxFailOverUrl(String mpxUrlFailOverUrl) {
		this.mpxFailOverUrl = mpxUrlFailOverUrl;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getDeltaUrlFlag() {
		return deltaUrlFlag;
	}

	public void setDeltaUrlFlag(Boolean deltaUrlFlag) {
		this.deltaUrlFlag = deltaUrlFlag;
	}

	public Boolean getMpxUrlFlag() {
		return mpxUrlFlag;
	}

	public void setMpxUrlFlag(Boolean mpxUrlFlag) {
		this.mpxUrlFlag = mpxUrlFlag;
	}

	public long getOffsetTime() {
		return offsetTime;
	}

	public void setOffsetTime(long offsetTime) {
		this.offsetTime = offsetTime;
	}

	public long getFailOverOffsetTime() {
		return failOverOffsetTime;
	}

	public void setFailOverOffsetTime(long failOverOffsetTime) {
		this.failOverOffsetTime = failOverOffsetTime;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			LOGGER.error("Exception in method:{}", e.getMessage());
		}
		return str;
	}

}
