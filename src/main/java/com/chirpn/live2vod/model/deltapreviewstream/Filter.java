package com.chirpn.live2vod.model.deltapreviewstream;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 
 * @author Satish ghonge
 * 
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "description", "label", "parent_id", "filter_type", "default_endpoint_uri",
		"custom_endpoint_uri", "ancestry", "url_extension", "output_url", "filter_settings" })
public class Filter {

	@JsonProperty("id")
	private String id;
	@JsonProperty("description")
	private String description;
	@JsonProperty("label")
	private String label;
	@JsonProperty("parent_id")
	private String parentId;
	@JsonProperty("filter_type")
	private String filterType;
	@JsonProperty("default_endpoint_uri")
	private String defaultEndpointUri;
	@JsonProperty("custom_endpoint_uri")
	private String customEndpointUri;
	@JsonProperty("ancestry")
	private String ancestry;
	@JsonProperty("url_extension")
	private String urlExtension;
	@JsonProperty("output_url")
	private String outputUrl;
	@JsonProperty("filter_settings")
	private FilterSettings filterSettings;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("label")
	public String getLabel() {
		return label;
	}

	@JsonProperty("label")
	public void setLabel(String label) {
		this.label = label;
	}

	@JsonProperty("parent_id")
	public String getParentId() {
		return parentId;
	}

	@JsonProperty("parent_id")
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@JsonProperty("filter_type")
	public String getFilterType() {
		return filterType;
	}

	@JsonProperty("filter_type")
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	@JsonProperty("default_endpoint_uri")
	public String getDefaultEndpointUri() {
		return defaultEndpointUri;
	}

	@JsonProperty("default_endpoint_uri")
	public void setDefaultEndpointUri(String defaultEndpointUri) {
		this.defaultEndpointUri = defaultEndpointUri;
	}

	@JsonProperty("custom_endpoint_uri")
	public String getCustomEndpointUri() {
		return customEndpointUri;
	}

	@JsonProperty("custom_endpoint_uri")
	public void setCustomEndpointUri(String customEndpointUri) {
		this.customEndpointUri = customEndpointUri;
	}

	@JsonProperty("ancestry")
	public String getAncestry() {
		return ancestry;
	}

	@JsonProperty("ancestry")
	public void setAncestry(String ancestry) {
		this.ancestry = ancestry;
	}

	@JsonProperty("url_extension")
	public String getUrlExtension() {
		return urlExtension;
	}

	@JsonProperty("url_extension")
	public void setUrlExtension(String urlExtension) {
		this.urlExtension = urlExtension;
	}

	@JsonProperty("output_url")
	public String getOutputUrl() {
		return outputUrl;
	}

	@JsonProperty("output_url")
	public void setOutputUrl(String outputUrl) {
		this.outputUrl = outputUrl;
	}

	@JsonProperty("filter_settings")
	public FilterSettings getFilterSettings() {
		return filterSettings;
	}

	@JsonProperty("filter_settings")
	public void setFilterSettings(FilterSettings filterSettings) {
		this.filterSettings = filterSettings;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
