package com.chirpn.live2vod.model.deltapreviewstream;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 
 * @author Satish ghonge
 * 
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "start_time", "end_time", "start_over", "manifest_scope", "allow_url_start_end_params",
		"start_frame", "end_frame", "frame_accurate", "lowest_framerate_numerator", "lowest_framerate_denominator" })
public class FilterSettings {

	@JsonProperty("id")
	private String id;
	@JsonProperty("start_time")
	private String startTime;
	@JsonProperty("end_time")
	private String endTime;
	@JsonProperty("start_over")
	private String startOver;
	@JsonProperty("manifest_scope")
	private String manifestScope;
	@JsonProperty("allow_url_start_end_params")
	private String allowUrlStartEndParams;
	@JsonProperty("start_frame")
	private String startFrame;
	@JsonProperty("end_frame")
	private String endFrame;
	@JsonProperty("frame_accurate")
	private String frameAccurate;
	@JsonProperty("lowest_framerate_numerator")
	private String lowestFramerateNumerator;
	@JsonProperty("lowest_framerate_denominator")
	private String lowestFramerateDenominator;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("start_time")
	public String getStartTime() {
		return startTime;
	}

	@JsonProperty("start_time")
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@JsonProperty("end_time")
	public String getEndTime() {
		return endTime;
	}

	@JsonProperty("end_time")
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@JsonProperty("start_over")
	public String getStartOver() {
		return startOver;
	}

	@JsonProperty("start_over")
	public void setStartOver(String startOver) {
		this.startOver = startOver;
	}

	@JsonProperty("manifest_scope")
	public String getManifestScope() {
		return manifestScope;
	}

	@JsonProperty("manifest_scope")
	public void setManifestScope(String manifestScope) {
		this.manifestScope = manifestScope;
	}

	@JsonProperty("allow_url_start_end_params")
	public String getAllowUrlStartEndParams() {
		return allowUrlStartEndParams;
	}

	@JsonProperty("allow_url_start_end_params")
	public void setAllowUrlStartEndParams(String allowUrlStartEndParams) {
		this.allowUrlStartEndParams = allowUrlStartEndParams;
	}

	@JsonProperty("start_frame")
	public String getStartFrame() {
		return startFrame;
	}

	@JsonProperty("start_frame")
	public void setStartFrame(String startFrame) {
		this.startFrame = startFrame;
	}

	@JsonProperty("end_frame")
	public String getEndFrame() {
		return endFrame;
	}

	@JsonProperty("end_frame")
	public void setEndFrame(String endFrame) {
		this.endFrame = endFrame;
	}

	@JsonProperty("frame_accurate")
	public String getFrameAccurate() {
		return frameAccurate;
	}

	@JsonProperty("frame_accurate")
	public void setFrameAccurate(String frameAccurate) {
		this.frameAccurate = frameAccurate;
	}

	@JsonProperty("lowest_framerate_numerator")
	public String getLowestFramerateNumerator() {
		return lowestFramerateNumerator;
	}

	@JsonProperty("lowest_framerate_numerator")
	public void setLowestFramerateNumerator(String lowestFramerateNumerator) {
		this.lowestFramerateNumerator = lowestFramerateNumerator;
	}

	@JsonProperty("lowest_framerate_denominator")
	public String getLowestFramerateDenominator() {
		return lowestFramerateDenominator;
	}

	@JsonProperty("lowest_framerate_denominator")
	public void setLowestFramerateDenominator(String lowestFramerateDenominator) {
		this.lowestFramerateDenominator = lowestFramerateDenominator;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
