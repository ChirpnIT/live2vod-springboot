package com.chirpn.live2vod.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.audit.Auditable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author awalunj
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hls_video_url", "lowest_framerate_numerator", "lowest_framerate_denominator" })
public class ChannelContentDTO extends Auditable<String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChannelContentDTO.class);

	@JsonProperty("hls_video_url")
	private String hlsVideoURL;

	@JsonProperty("lowest_framerate_numerator")
	private String lowestFramerateNumerator;

	@JsonProperty("lowest_framerate_denominator")
	private String lowestFramerateDenominator;

	public String getHlsVideoURL() {
		return hlsVideoURL;
	}

	public void setHlsVideoURL(String hlsVideoURL) {
		this.hlsVideoURL = hlsVideoURL;
	}

	public String getLowestFramerateNumerator() {
		return lowestFramerateNumerator;
	}

	public void setLowestFramerateNumerator(String lowestFramerateNumerator) {
		this.lowestFramerateNumerator = lowestFramerateNumerator;
	}

	public String getLowestFramerateDenominator() {
		return lowestFramerateDenominator;
	}

	public void setLowestFramerateDenominator(String lowestFramerateDenominator) {
		this.lowestFramerateDenominator = lowestFramerateDenominator;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			LOGGER.error("Exception in AssetsModel:{}", e.getMessage());
		}
		return str;
	}
}
