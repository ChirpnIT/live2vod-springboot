package com.chirpn.live2vod.model;

import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.common.validation.LocalTimeDeserializer;
import com.chirpn.live2vod.common.validation.LocalTimeSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author awalunj
 *
 */
public class AdSlotDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(AdSlotDTO.class);

	@JsonProperty("ad_slot_no")
	private String adSlotNo;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonProperty("start_time")
	private LocalTime startTime;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonProperty("end_time")
	private LocalTime endTime;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonProperty("duration")
	private LocalTime duration;

	@JsonProperty("start_frame")
	private Integer startFrame;

	@JsonProperty("end_frame")
	private Integer endFrame;
	
	@JsonProperty("start_date_time")
	private String slotStartTime;
	
	@JsonProperty("end_date_time")
	private String slotEndTime;

	public String getAdSlotNo() {
		return adSlotNo;
	}

	public void setAdSlotNo(String adSlotNo) {
		this.adSlotNo = adSlotNo;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LocalTime getDuration() {
		return duration;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	public Integer getStartFrame() {
		return startFrame;
	}

	public void setStartFrame(Integer startFrame) {
		this.startFrame = startFrame;
	}

	public Integer getEndFrame() {
		return endFrame;
	}

	public void setEndFrame(Integer endFrame) {
		this.endFrame = endFrame;
	}

	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when adslotDto object created:{}", e.getMessage());
		}
		return str;
	}

}
