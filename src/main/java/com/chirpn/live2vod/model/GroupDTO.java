package com.chirpn.live2vod.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Satish ghonge
 * 
 */
public class GroupDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(GroupDTO.class);

	@JsonProperty("group_no")
	private String groupNo;

	@JsonProperty("group_name")
	private String groupName;

	public String getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(String groupId) {
		this.groupNo = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when groupDto object created:{}", e.getMessage());
		}
		return str;
	}
}
