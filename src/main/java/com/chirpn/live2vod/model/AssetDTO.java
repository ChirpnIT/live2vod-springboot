package com.chirpn.live2vod.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chirpn.live2vod.audit.Auditable;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.common.enums.ExtractionType;
import com.chirpn.live2vod.common.validation.LocalDateDeserializer;
import com.chirpn.live2vod.common.validation.LocalDateSerializer;
import com.chirpn.live2vod.common.validation.LocalTimeDeserializer;
import com.chirpn.live2vod.common.validation.LocalTimeSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author awalunj
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"sr_no", "asset_no", "program_name", "date", "end_date", "start_time", "end_time", "start_frame", 
	"end_frame", "pilat_id", "mpx_id", "geo_targeting", "ad_removal", "status", "url", "duration", 
	"auto_publish", "repeat_type", "repeat_end_date", "requested_by",
	"requested_at", "meta_tags", "channel", "ad_slots", "extractionType" })
public class AssetDTO extends Auditable<String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssetDTO.class);
	
	@JsonProperty("sr_no")
	private Long id;

	@JsonProperty("asset_no")
	private String assetNo;

	@JsonProperty("program_name")
	private String programName;

	@JsonProperty("date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate date;
	
	@JsonProperty("end_date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate endDate;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonProperty("start_time")
	private LocalTime startTime;

	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonProperty("end_time")
	private LocalTime endTime;

	@JsonProperty("start_frame")
	private Integer startFrame;

	@JsonProperty("end_frame")
	private Integer endFrame;
	
	@Column(name = "duration")
	private String duration;

	@JsonProperty("pilat_id")
	private String pilatId;

	@JsonProperty("external_id")
	private String externalId;

	@JsonProperty("mpx_id")
	private String mpxId;

	@JsonProperty("geo_targeting")
	private String geoTargeting;

	@JsonProperty("auto_publish")
	private Boolean autoPublish = false;

	@JsonProperty("ad_removal")
	private Boolean adRemoval = false;

	@JsonProperty("meta_tags")
	private List<String> metaTags = null;

	@JsonProperty("repeat_type")
	private String repeat;

	@JsonProperty("repeat_end_date")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate repeatEndDate;

	@JsonProperty("status")
	private AssetStatus status = AssetStatus.SCHEDULED;

	@JsonProperty("url")
	private String url;

	@JsonProperty("channel")
	private ChannelDTO channel;

	@JsonProperty("ad_slots")
	private List<AdSlotDTO> adSlots = new ArrayList<>();

	@JsonProperty("editable")
	private Boolean editable = Boolean.TRUE;
	
	@JsonProperty("deleted")
	private Boolean deleted = Boolean.FALSE;
	
	@JsonProperty("extractionType")
	private ExtractionType extractionType;
	
	@JsonProperty("mp4_url")
	private String mp4Url;
	
	@JsonProperty("updateKey")
	private boolean updateKey = true;
	
	@JsonProperty("start_date_time")
	private String startDateTime;
	
	@JsonProperty("end_date_time")
	private String endDateTime;
	
	public ChannelDTO getChannel() {
		return channel;
	}

	public void setChannel(ChannelDTO channel) {
		this.channel = channel;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getPilatId() {
		return pilatId;
	}

	public void setPilatId(String pilatId) {
		this.pilatId = pilatId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getGeoTargeting() {
		return geoTargeting;
	}

	public void setGeoTargeting(String geoTargeting) {
		this.geoTargeting = geoTargeting;
	}

	public Boolean getAdRemoval() {
		return adRemoval;
	}

	public void setAdRemoval(Boolean adRemoval) {
		this.adRemoval = adRemoval;
	}

	public List<String> getMetaTags() {
		return metaTags;
	}

	public void setMetaTags(List<String> metaTags) {
		this.metaTags = metaTags;
	}

	public AssetStatus getStatus() {
		return status;
	}

	public void setStatus(AssetStatus status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getAssetNo() {
		return assetNo;
	}

	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}

	public List<AdSlotDTO> getAdSlots() {
		return adSlots;
	}

	public void setAdSlots(List<AdSlotDTO> adSlots) {
		this.adSlots = adSlots;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public Boolean getAutoPublish() {
		return autoPublish;
	}

	public void setAutoPublish(Boolean autoPublish) {
		this.autoPublish = autoPublish;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public LocalDate getRepeatEndDate() {
		return repeatEndDate;
	}

	public void setRepeatEndDate(LocalDate repeatEndDate) {
		this.repeatEndDate = repeatEndDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getStartFrame() {
		return startFrame;
	}

	public void setStartFrame(Integer startFrame) {
		this.startFrame = startFrame;
	}

	public Integer getEndFrame() {
		return endFrame;
	}

	public void setEndFrame(Integer endFrame) {
		this.endFrame = endFrame;
	}

	public String getMpxId() {
		return mpxId;
	}

	public void setMpxId(String mpxId) {
		this.mpxId = mpxId;
	}
	
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public ExtractionType getExtractionType() {
		return extractionType;
	}

	public void setExtractionType(ExtractionType extractionType) {
		this.extractionType = extractionType;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getMp4Url() {
		return mp4Url;
	}

	public void setMp4Url(String mp4Url) {
		this.mp4Url = mp4Url;
	}

	public boolean isUpdateKey() {
		return updateKey;
	}

	public void setUpdateKey(boolean updateKey) {
		this.updateKey = updateKey;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			LOGGER.error("Exception in AssetsModel:{}", e.getMessage());
		}
		return str;
	}

}
