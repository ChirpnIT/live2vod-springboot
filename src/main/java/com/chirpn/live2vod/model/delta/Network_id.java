package com.chirpn.live2vod.model.delta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nil"})
public class Network_id {

	@XmlAttribute
	private String nil;

	public Network_id() {
		super();
	}

	public Network_id(String nil) {
		super();
		this.nil = nil;
	}

	public String getNil() {
		return nil;
	}

	public void setNil(String nil) {
		this.nil = nil;
	}
}