package com.chirpn.live2vod.model.delta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Output_url {
	
	private static final Logger logger = LoggerFactory.getLogger(Output_url.class);

	private String nil;

	public String getNil() {
		return nil;
	}

	public void setNil(String nil) {
		this.nil = nil;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when Output_url object created:{}", e.getMessage());
		}
		return str;
	}
}