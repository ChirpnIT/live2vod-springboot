package com.chirpn.live2vod.model.delta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "filter_settings")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "start_time", 
		"end_time", 
		"start_over", 
		"allow_url_start_end_params",
		"frame_accurate",
		"start_frame",
		"end_frame",
		"lowest_framerate_numerator",
		"lowest_framerate_denominator",
		"segment_duration",
		"index_duration",
		"playlist_type",
		"avail_trigger",
		"ad_markers",
		"broadcast_time",
		"ignore_web_delivery_allowed",
		"ignore_no_regional_blackout",
		"enable_blackout",
		"enable_network_end_blackout",
		"network_id",
		"include_program_date_time",
		"program_date_time_interval" })
public class Filter_settings {


	private String start_time;


	private String end_time;
	

	private String start_over;
	

	private String allow_url_start_end_params;
	

	private String frame_accurate;
	

	private String start_frame;
	

	private String end_frame;
	

	private String lowest_framerate_numerator;
	
	
	private String lowest_framerate_denominator;

	
	private String enable_network_end_blackout;

	
	private String ignore_web_delivery_allowed;

	
	private String include_program_date_time;

	
	private String segment_duration;

	
	private String program_date_time_interval;

	
	private String playlist_type;

	private Network_id network_id;

	
	private String enable_blackout;

	
	private String avail_trigger;

	
	private String ad_markers;

	
	private String index_duration;

	
	private String ignore_no_regional_blackout;

	
	private String broadcast_time;

	public Filter_settings() {
		super();
	}
	
	public Filter_settings(String start_time, String end_time, String start_over, String allow_url_start_end_params, String frame_accurate, String start_frame, String end_frame, String lowest_framerate_numerator, String lowest_framerate_denominator) {
		super();
		this.start_time = start_time;
		this.end_time = end_time;
		this.start_over = start_over;
		this.allow_url_start_end_params = allow_url_start_end_params;
		this.frame_accurate = frame_accurate;
		this.start_frame = start_frame;
		this.end_frame = end_frame;
		this.lowest_framerate_numerator = lowest_framerate_numerator;
		this.lowest_framerate_denominator = lowest_framerate_denominator;
	}


	public Filter_settings(String enable_network_end_blackout, String ignore_web_delivery_allowed, String include_program_date_time, String segment_duration, String program_date_time_interval, String playlist_type, Network_id network_id, String enable_blackout,
			String avail_trigger, String ad_markers, String index_duration, String ignore_no_regional_blackout, String broadcast_time) {
		super();
		this.enable_network_end_blackout = enable_network_end_blackout;
		this.ignore_web_delivery_allowed = ignore_web_delivery_allowed;
		this.include_program_date_time = include_program_date_time;
		this.segment_duration = segment_duration;
		this.program_date_time_interval = program_date_time_interval;
		this.playlist_type = playlist_type;
		this.network_id = network_id;
		this.enable_blackout = enable_blackout;
		this.avail_trigger = avail_trigger;
		this.ad_markers = ad_markers;
		this.index_duration = index_duration;
		this.ignore_no_regional_blackout = ignore_no_regional_blackout;
		this.broadcast_time = broadcast_time;
	}

	
	
	public String getStart_time() {
		return start_time;
	}

	
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	
	public String getEnd_time() {
		return end_time;
	}

	
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	
	public String getStart_over() {
		return start_over;
	}

	
	public void setStart_over(String start_over) {
		this.start_over = start_over;
	}

	
	public String getAllow_url_start_end_params() {
		return allow_url_start_end_params;
	}

	
	public void setAllow_url_start_end_params(String allow_url_start_end_params) {
		this.allow_url_start_end_params = allow_url_start_end_params;
	}

	
	public String getFrame_accurate() {
		return frame_accurate;
	}

	
	public void setFrame_accurate(String frame_accurate) {
		this.frame_accurate = frame_accurate;
	}

	
	public String getStart_frame() {
		return start_frame;
	}

	
	public void setStart_frame(String start_frame) {
		this.start_frame = start_frame;
	}

	
	public String getEnd_frame() {
		return end_frame;
	}

	
	public void setEnd_frame(String end_frame) {
		this.end_frame = end_frame;
	}

	
	public String getLowest_framerate_numerator() {
		return lowest_framerate_numerator;
	}

	
	public void setLowest_framerate_numerator(String lowest_framerate_numerator) {
		this.lowest_framerate_numerator = lowest_framerate_numerator;
	}

	
	public String getLowest_framerate_denominator() {
		return lowest_framerate_denominator;
	}

	
	public void setLowest_framerate_denominator(String lowest_framerate_denominator) {
		this.lowest_framerate_denominator = lowest_framerate_denominator;
	}

	public String getEnable_network_end_blackout() {
		return enable_network_end_blackout;
	}

	public void setEnable_network_end_blackout(String enable_network_end_blackout) {
		this.enable_network_end_blackout = enable_network_end_blackout;
	}

	public String getIgnore_web_delivery_allowed() {
		return ignore_web_delivery_allowed;
	}

	public void setIgnore_web_delivery_allowed(String ignore_web_delivery_allowed) {
		this.ignore_web_delivery_allowed = ignore_web_delivery_allowed;
	}

	public String getInclude_program_date_time() {
		return include_program_date_time;
	}

	public void setInclude_program_date_time(String include_program_date_time) {
		this.include_program_date_time = include_program_date_time;
	}

	public String getSegment_duration() {
		return segment_duration;
	}

	public void setSegment_duration(String segment_duration) {
		this.segment_duration = segment_duration;
	}

	public String getProgram_date_time_interval() {
		return program_date_time_interval;
	}

	public void setProgram_date_time_interval(String program_date_time_interval) {
		this.program_date_time_interval = program_date_time_interval;
	}

	public String getPlaylist_type() {
		return playlist_type;
	}

	public void setPlaylist_type(String playlist_type) {
		this.playlist_type = playlist_type;
	}

	public Network_id getNetwork_id() {
		return network_id;
	}

	public void setNetwork_id(Network_id network_id) {
		this.network_id = network_id;
	}

	public String getEnable_blackout() {
		return enable_blackout;
	}

	public void setEnable_blackout(String enable_blackout) {
		this.enable_blackout = enable_blackout;
	}

	public String getAvail_trigger() {
		return avail_trigger;
	}

	public void setAvail_trigger(String avail_trigger) {
		this.avail_trigger = avail_trigger;
	}

	public String getAd_markers() {
		return ad_markers;
	}

	public void setAd_markers(String ad_markers) {
		this.ad_markers = ad_markers;
	}

	public String getIndex_duration() {
		return index_duration;
	}

	public void setIndex_duration(String index_duration) {
		this.index_duration = index_duration;
	}

	public String getIgnore_no_regional_blackout() {
		return ignore_no_regional_blackout;
	}

	public void setIgnore_no_regional_blackout(String ignore_no_regional_blackout) {
		this.ignore_no_regional_blackout = ignore_no_regional_blackout;
	}

	public String getBroadcast_time() {
		return broadcast_time;
	}

	public void setBroadcast_time(String broadcast_time) {
		this.broadcast_time = broadcast_time;
	}
}