package com.chirpn.live2vod.model.delta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "filter" })
public class Filters {

	private Filter[] filter;

	public Filter[] getFilter() {
		return filter;
	}

	public void setFilter(Filter[] filter) {
		this.filter = filter;
	}
}
