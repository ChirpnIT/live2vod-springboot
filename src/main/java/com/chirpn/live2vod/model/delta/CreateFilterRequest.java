package com.chirpn.live2vod.model.delta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hlsName",
"startTime",
"endTime",
"startFrame",
"endFrame",
"id",
"idType",
"adRemove"
})
public class CreateFilterRequest {
	
	private static final Logger logger = LoggerFactory.getLogger(CreateFilterRequest.class);
	
	@JsonProperty("hlsName")
	private String hlsName;
	@JsonProperty("startTime")
	private String startTime;
	@JsonProperty("endTime")
	private String endTime;
	@JsonProperty("startFrame")
	private Integer startFrame;
	@JsonProperty("endFrame")
	private Integer endFrame;
	@JsonProperty("id")
	private String id;
	@JsonProperty("idType")
	private String idType;
	@JsonProperty("adRemove")
	private Integer adRemove;

	@JsonProperty("hlsName")
	public String getHlsName() {
	return hlsName;
	}

	@JsonProperty("hlsName")
	public void setHlsName(String hlsName) {
	this.hlsName = hlsName;
	}

	@JsonProperty("startTime")
	public String getStartTime() {
	return startTime;
	}

	@JsonProperty("startTime")
	public void setStartTime(String startTime) {
	this.startTime = startTime;
	}

	@JsonProperty("endTime")
	public String getEndTime() {
	return endTime;
	}

	@JsonProperty("endTime")
	public void setEndTime(String endTime) {
	this.endTime = endTime;
	}

	@JsonProperty("startFrame")
	public Integer getStartFrame() {
	return startFrame;
	}

	@JsonProperty("startFrame")
	public void setStartFrame(Integer startFrame) {
	this.startFrame = startFrame;
	}

	@JsonProperty("endFrame")
	public Integer getEndFrame() {
	return endFrame;
	}

	@JsonProperty("endFrame")
	public void setEndFrame(Integer endFrame) {
	this.endFrame = endFrame;
	}

	@JsonProperty("id")
	public String getId() {
	return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
	this.id = id;
	}

	@JsonProperty("idType")
	public String getIdType() {
	return idType;
	}

	@JsonProperty("idType")
	public void setIdType(String idType) {
	this.idType = idType;
	}

	@JsonProperty("adRemove")
	public Integer getAdRemove() {
	return adRemove;
	}

	@JsonProperty("adRemove")
	public void setAdRemove(Integer adRemove) {
	this.adRemove = adRemove;
	}
	
	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when asset object created:{}", e.getMessage());
		}
		return str;
	}
}


