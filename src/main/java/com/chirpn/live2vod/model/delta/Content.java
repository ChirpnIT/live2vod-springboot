package com.chirpn.live2vod.model.delta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "content")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"filters"})
public class Content {

	private Filters filters;
	
	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

}
