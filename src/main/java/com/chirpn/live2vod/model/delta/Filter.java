package com.chirpn.live2vod.model.delta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "label", "filter_type", "endpoint", "url_extension", "output_url", "description", "name", "parent_filter", "filter_settings" })
public class Filter {
	
	private String parent_filter;
	private String endpoint;
	private String filter_type;
	private String output_url;
	private String url_extension;
	private String name;
	private String description;
	private String label;
	private Filter_settings filter_settings;
	
	public Filter() {
		super();
	}
	
	public Filter(String parent_filter, String endpoint, String filter_type, String output_url, String url_extension, String name, String description, Filter_settings filter_settings, String label) {
		super();
		this.parent_filter = parent_filter;
		this.endpoint = endpoint;
		this.filter_type = filter_type;
		this.output_url = output_url;
		this.url_extension = url_extension;
		this.name = name;
		this.description = description;
		this.filter_settings = filter_settings;
		this.label = label;
	}



	public String getParent_filter() {
		return parent_filter;
	}

	public void setParent_filter(String parent_filter) {
		this.parent_filter = parent_filter;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getFilter_type() {
		return filter_type;
	}

	public void setFilter_type(String filter_type) {
		this.filter_type = filter_type;
	}

	public String getOutput_url() {
		return output_url;
	}

	public void setOutput_url(String output_url) {
		this.output_url = output_url;
	}

	public String getUrl_extension() {
		return url_extension;
	}

	public void setUrl_extension(String url_extension) {
		this.url_extension = url_extension;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Filter_settings getFilter_settings() {
		return filter_settings;
	}

	public void setFilter_settings(Filter_settings filter_settings) {
		this.filter_settings = filter_settings;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
