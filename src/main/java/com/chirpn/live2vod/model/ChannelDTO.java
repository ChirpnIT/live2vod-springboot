package com.chirpn.live2vod.model;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Satish ghonge
 * 
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({ "channel_no", "name", "deluxe_id", "stream_id", "groups", "delta_fail_over_stream_id"})
public class ChannelDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(ChannelDTO.class);

	@JsonProperty("channel_no")
	private String channelNo;

	@JsonProperty("deluxe_id")
	private String deluxeId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("stream_id")
	private String streamId;

	@JsonProperty("groups")
	List<GroupDTO> groups = new ArrayList<>();
	
	@JsonProperty("delta_fail_over_stream_id")
	private String deltaFailOverStreamId;
	
	@JsonProperty("duration")
	private String duration;

	public String getChannelNo() {
		return channelNo;
	}

	public void setChannelNo(String channelNo) {
		this.channelNo = channelNo;
	}

	public String getDeluxeId() {
		return deluxeId;
	}

	public void setDeluxeId(String deluxeId) {
		this.deluxeId = deluxeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreamId() {
		return streamId;
	}

	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}

	public List<GroupDTO> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupDTO> groups) {
		this.groups = groups;
	}

	public String getDeltaFailOverStreamId() {
		return deltaFailOverStreamId;
	}

	public void setDeltaFailOverStreamId(String deltaFailOverStreamId) {
		this.deltaFailOverStreamId = deltaFailOverStreamId;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when channelDto object created:{}", e.getMessage());
		}
		return str;
	}

}
