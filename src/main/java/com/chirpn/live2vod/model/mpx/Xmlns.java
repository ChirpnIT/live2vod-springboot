package com.chirpn.live2vod.model.mpx;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"pl1",
"yt",
"dcterms",
"media",
"pl",
"pla",
"plmedia",
"plfile",
"plrelease"
})
public class Xmlns {

	@JsonProperty("pl1")
	public String pl1;
	@JsonProperty("yt")
	public String yt;
	@JsonProperty("dcterms")
	public String dcterms;
	@JsonProperty("media")
	public String media;
	@JsonProperty("pl")
	public String pl;
	@JsonProperty("pla")
	public String pla;
	@JsonProperty("plmedia")
	public String plmedia;
	@JsonProperty("plfile")
	public String plfile;
	@JsonProperty("plrelease")
	public String plrelease;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String getPl1() {
		return pl1;
	}

	public void setPl1(String pl1) {
		this.pl1 = pl1;
	}

	public String getYt() {
		return yt;
	}

	public void setYt(String yt) {
		this.yt = yt;
	}

	public String getDcterms() {
		return dcterms;
	}

	public void setDcterms(String dcterms) {
		this.dcterms = dcterms;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getPl() {
		return pl;
	}

	public void setPl(String pl) {
		this.pl = pl;
	}

	public String getPla() {
		return pla;
	}

	public void setPla(String pla) {
		this.pla = pla;
	}

	public String getPlmedia() {
		return plmedia;
	}

	public void setPlmedia(String plmedia) {
		this.plmedia = plmedia;
	}

	public String getPlfile() {
		return plfile;
	}

	public void setPlfile(String plfile) {
		this.plfile = plfile;
	}

	public String getPlrelease() {
		return plrelease;
	}

	public void setPlrelease(String plrelease) {
		this.plrelease = plrelease;
	}

}