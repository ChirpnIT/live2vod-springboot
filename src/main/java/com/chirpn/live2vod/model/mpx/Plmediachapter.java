package com.chirpn.live2vod.model.mpx;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "plmedia$startTime", "plmedia$endTime", "plmedia$title", "plmedia$thumbnailUrl" })
public class Plmediachapter {
	
	private static final Logger logger = LoggerFactory.getLogger(Plmediachapter.class);

	@JsonProperty("plmedia$startTime")
	public Long plmedia$startTime;
	@JsonProperty("plmedia$endTime")
	public Long plmedia$endTime;
	@JsonProperty("plmedia$title")
	public String plmedia$title;
	@JsonProperty("plmedia$thumbnailUrl")
	public String plmedia$thumbnailUrl;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	public Long getPlmedia$startTime() {
		return plmedia$startTime;
	}

	public void setPlmedia$startTime(Long plmedia$startTime) {
		this.plmedia$startTime = plmedia$startTime;
	}

	public void setPlmedia$endTime(Long plmedia$endTime) {
		this.plmedia$endTime = plmedia$endTime;
	}

	public Long getPlmedia$endTime() {
		return plmedia$endTime;
	}

	public String getPlmedia$title() {
		return plmedia$title;
	}

	public void setPlmedia$title(String plmedia$title) {
		this.plmedia$title = plmedia$title;
	}

	public String getPlmedia$thumbnailUrl() {
		return plmedia$thumbnailUrl;
	}

	public void setPlmedia$thumbnailUrl(String plmedia$thumbnailUrl) {
		this.plmedia$thumbnailUrl = plmedia$thumbnailUrl;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured when Plmediachapter object created:{}", e.getMessage());
		}
		return str;
	}

}