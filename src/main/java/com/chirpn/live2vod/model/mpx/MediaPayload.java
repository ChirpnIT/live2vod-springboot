/*
 * package com.chirpn.live2vod.model.mpx;
 * 
 * import java.util.HashMap; import java.util.List; import java.util.Map; import
 * com.fasterxml.jackson.annotation.JsonAnyGetter; import
 * com.fasterxml.jackson.annotation.JsonAnySetter; import
 * com.fasterxml.jackson.annotation.JsonIgnore; import
 * com.fasterxml.jackson.annotation.JsonInclude; import
 * com.fasterxml.jackson.annotation.JsonProperty; import
 * com.fasterxml.jackson.annotation.JsonPropertyOrder;
 * 
 * @JsonInclude(JsonInclude.Include.NON_NULL)
 * 
 * @JsonPropertyOrder({ "$xmlns", "totalResults", "startIndex", "itemsPerPage",
 * "entryCount", "entries" }) public class MediaPayload {
 * 
 * @JsonProperty("$xmlns") public Xmlns $xmlns;
 * 
 * @JsonProperty("totalResults") public Integer totalResults;
 * 
 * @JsonProperty("startIndex") public Integer startIndex;
 * 
 * @JsonProperty("itemsPerPage") public Integer itemsPerPage;
 * 
 * @JsonProperty("entryCount") public Integer entryCount;
 * 
 * @JsonProperty("entries") public List<Media> entries = null;
 * 
 * @JsonIgnore private Map<String, Object> additionalProperties = new
 * HashMap<String, Object>();
 * 
 * @JsonAnyGetter public Map<String, Object> getAdditionalProperties() { return
 * this.additionalProperties; }
 * 
 * @JsonAnySetter public void setAdditionalProperty(String name, Object value) {
 * this.additionalProperties.put(name, value); }
 * 
 * public Xmlns get$xmlns() { return $xmlns; }
 * 
 * public void set$xmlns(Xmlns $xmlns) { this.$xmlns = $xmlns; }
 * 
 * public Integer getTotalResults() { return totalResults; }
 * 
 * public void setTotalResults(Integer totalResults) { this.totalResults =
 * totalResults; }
 * 
 * public Integer getStartIndex() { return startIndex; }
 * 
 * public void setStartIndex(Integer startIndex) { this.startIndex = startIndex;
 * }
 * 
 * public Integer getItemsPerPage() { return itemsPerPage; }
 * 
 * public void setItemsPerPage(Integer itemsPerPage) { this.itemsPerPage =
 * itemsPerPage; }
 * 
 * public Integer getEntryCount() { return entryCount; }
 * 
 * public void setEntryCount(Integer entryCount) { this.entryCount = entryCount;
 * }
 * 
 * public List<Media> getEntries() { return entries; }
 * 
 * public void setEntries(List<Media> entries) { this.entries = entries; }
 * 
 * }
 */