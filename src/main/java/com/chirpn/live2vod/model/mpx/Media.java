package com.chirpn.live2vod.model.mpx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"$xmlns",
"id",
"guid",
"updated",
"title",
"author",
"description",
"added",
"ownerId",
"addedByUserId",
"updatedByUserId",
"version",
"locked",
"media$availableDate",
"media$expirationDate",
"media$categories",
"media$copyright",
"media$copyrightUrl",
"media$credits",
"media$keywords",
"media$ratings",
"media$countries",
"media$excludeCountries",
"media$text",
"media$content",
"media$thumbnails",
"pla$adminTags",
"plmedia$approved",
"plmedia$categoryIds",
"plmedia$chapters",
"link",
"pubDate",
"plmedia$titleLocalized",
"plmedia$descriptionLocalized",
"plmedia$authorLocalized",
"plmedia$copyrightLocalized",
"plmedia$copyrightUrlLocalized",
"plmedia$keywordsLocalized",
"plmedia$linkLocalized",
"plmedia$textLocalized",
"plmedia$defaultThumbnailUrl",
"plmedia$originalOwnerIds",
"plmedia$provider",
"plmedia$providerId",
"plmedia$restrictionId",
"plmedia$adPolicyId",
"plmedia$originalMediaIds",
"plmedia$programId",
"plmedia$seriesId",
"plmedia$availabilityState",
"plmedia$pid",
"plmedia$publicUrl",
"plmedia$availabilityTags",
"plmedia$availabilityWindows",
"pl1$autoPublishComments",
"pl1$availableToApps",
"pl1$contentLink1",
"pl1$contentLink2",
"pl1$contentLink3",
"pl1$enableComments",
"pl1$grouping",
"pl1$language",
"pl1$loggerId",
"pl1$pilatId",
"pl1$programName",
"pl1$publishSonyAssets",
"pl1$saveForRepeat",
"pl1$seriesPremiere",
"pl1$site",
"pl1$useType"
})
public class Media {
	
	private static final Logger logger = LoggerFactory.getLogger(Media.class);

	@JsonProperty("$xmlns")
	public Xmlns $xmlns;
	@JsonProperty("id")
	public String id;
	@JsonProperty("guid")
	public String guid;
	@JsonProperty("updated")
	public String updated;
	@JsonProperty("title")
	public String title;
	@JsonProperty("author")
	public String author;
	@JsonProperty("description")
	public String description;
	@JsonProperty("added")
	public String added;
	@JsonProperty("ownerId")
	public String ownerId;
	@JsonProperty("addedByUserId")
	public String addedByUserId;
	@JsonProperty("updatedByUserId")
	public String updatedByUserId;
	@JsonProperty("version")
	public Integer version;
	@JsonProperty("locked")
	public Boolean locked;
	@JsonProperty("media$availableDate")
	public Integer media$availableDate;
	@JsonProperty("media$expirationDate")
	public Integer media$expirationDate;
	@JsonProperty("media$categories")
	public List<Object> media$categories = null;
	@JsonProperty("media$copyright")
	public String media$copyright;
	@JsonProperty("media$copyrightUrl")
	public String media$copyrightUrl;
	@JsonProperty("media$credits")
	public List<Object> media$credits = null;
	@JsonProperty("media$keywords")
	public String media$keywords;
	@JsonProperty("media$ratings")
	public List<Object> media$ratings = null;
	@JsonProperty("media$countries")
	public List<Object> media$countries = null;
	@JsonProperty("media$excludeCountries")
	public Boolean media$excludeCountries;
	@JsonProperty("media$text")
	public String media$text;
	@JsonProperty("media$content")
	public List<Object> media$content = null;
	@JsonProperty("media$thumbnails")
	public List<Object> media$thumbnails = null;
	@JsonProperty("pla$adminTags")
	public List<Object> pla$adminTags = null;
	@JsonProperty("plmedia$approved")
	public Boolean plmedia$approved;
	@JsonProperty("plmedia$categoryIds")
	public List<Object> plmedia$categoryIds = null;
	@JsonProperty("plmedia$chapters")
	public List<Plmediachapter> plmedia$chapters = null;
	@JsonProperty("plmedia$adslots")
	public List<Plmediachapter> plmedia$adslots = null;
	@JsonProperty("link")
	public String link;
	@JsonProperty("pubDate")
	public String pubDate;
	@JsonProperty("plmedia$titleLocalized")
	public PlmediatitleLocalized plmedia$titleLocalized;
	@JsonProperty("plmedia$descriptionLocalized")
	public PlmediadescriptionLocalized plmedia$descriptionLocalized;
	@JsonProperty("plmedia$authorLocalized")
	public PlmediaauthorLocalized plmedia$authorLocalized;
	@JsonProperty("plmedia$copyrightLocalized")
	public PlmediacopyrightLocalized plmedia$copyrightLocalized;
	@JsonProperty("plmedia$copyrightUrlLocalized")
	public PlmediacopyrightUrlLocalized plmedia$copyrightUrlLocalized;
	@JsonProperty("plmedia$keywordsLocalized")
	public PlmediakeywordsLocalized plmedia$keywordsLocalized;
	@JsonProperty("plmedia$linkLocalized")
	public PlmedialinkLocalized plmedia$linkLocalized;
	@JsonProperty("plmedia$textLocalized")
	public PlmediatextLocalized plmedia$textLocalized;
	@JsonProperty("plmedia$defaultThumbnailUrl")
	public String plmedia$defaultThumbnailUrl;
	@JsonProperty("plmedia$originalOwnerIds")
	public List<Object> plmedia$originalOwnerIds = null;
	@JsonProperty("plmedia$provider")
	public String plmedia$provider;
	@JsonProperty("plmedia$providerId")
	public String plmedia$providerId;
	@JsonProperty("plmedia$restrictionId")
	public String plmedia$restrictionId;
	@JsonProperty("plmedia$adPolicyId")
	public String plmedia$adPolicyId;
	@JsonProperty("plmedia$originalMediaIds")
	public List<Object> plmedia$originalMediaIds = null;
	@JsonProperty("plmedia$programId")
	public String plmedia$programId;
	@JsonProperty("plmedia$seriesId")
	public String plmedia$seriesId;
	@JsonProperty("plmedia$availabilityState")
	public String plmedia$availabilityState;
	@JsonProperty("plmedia$pid")
	public String plmedia$pid;
	@JsonProperty("plmedia$publicUrl")
	public String plmedia$publicUrl;
	@JsonProperty("plmedia$availabilityTags")
	public List<Object> plmedia$availabilityTags = null;
	@JsonProperty("plmedia$availabilityWindows")
	public List<Object> plmedia$availabilityWindows = null;
	@JsonProperty("pl1$autoPublishComments")
	public Boolean pl1$autoPublishComments;
	@JsonProperty("pl1$availableToApps")
	public Boolean pl1$availableToApps;
	@JsonProperty("pl1$contentLink1")
	public Pl1contentLink1 pl1$contentLink1;
	@JsonProperty("pl1$contentLink2")
	public Pl1contentLink2 pl1$contentLink2;
	@JsonProperty("pl1$contentLink3")
	public Pl1contentLink3 pl1$contentLink3;
	@JsonProperty("pl1$enableComments")
	public Boolean pl1$enableComments;
	@JsonProperty("pl1$grouping")
	public String pl1$grouping;
	@JsonProperty("pl1$language")
	public String pl1$language;
	@JsonProperty("pl1$loggerId")
	public String pl1$loggerId;
	@JsonProperty("pl1$pilatId")
	public String pl1$pilatId;
	@JsonProperty("pl1$programName")
	public String pl1$programName;
	@JsonProperty("pl1$publishSonyAssets")
	public Boolean pl1$publishSonyAssets;
	@JsonProperty("pl1$saveForRepeat")
	public Boolean pl1$saveForRepeat;
	@JsonProperty("pl1$seriesPremiere")
	public Boolean pl1$seriesPremiere;
	@JsonProperty("pl1$site")
	public String pl1$site;
	@JsonProperty("pl1$useType")
	public String pl1$useType;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Xmlns get$xmlns() {
		return $xmlns;
	}

	public void set$xmlns(Xmlns $xmlns) {
		this.$xmlns = $xmlns;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdded() {
		return added;
	}

	public void setAdded(String added) {
		this.added = added;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getAddedByUserId() {
		return addedByUserId;
	}

	public void setAddedByUserId(String addedByUserId) {
		this.addedByUserId = addedByUserId;
	}

	public String getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(String updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Integer getMedia$availableDate() {
		return media$availableDate;
	}

	public void setMedia$availableDate(Integer media$availableDate) {
		this.media$availableDate = media$availableDate;
	}

	public Integer getMedia$expirationDate() {
		return media$expirationDate;
	}

	public void setMedia$expirationDate(Integer media$expirationDate) {
		this.media$expirationDate = media$expirationDate;
	}

	public List<Object> getMedia$categories() {
		return media$categories;
	}

	public void setMedia$categories(List<Object> media$categories) {
		this.media$categories = media$categories;
	}

	public String getMedia$copyright() {
		return media$copyright;
	}

	public void setMedia$copyright(String media$copyright) {
		this.media$copyright = media$copyright;
	}

	public String getMedia$copyrightUrl() {
		return media$copyrightUrl;
	}

	public void setMedia$copyrightUrl(String media$copyrightUrl) {
		this.media$copyrightUrl = media$copyrightUrl;
	}

	public List<Object> getMedia$credits() {
		return media$credits;
	}

	public void setMedia$credits(List<Object> media$credits) {
		this.media$credits = media$credits;
	}

	public String getMedia$keywords() {
		return media$keywords;
	}

	public void setMedia$keywords(String media$keywords) {
		this.media$keywords = media$keywords;
	}

	public List<Object> getMedia$ratings() {
		return media$ratings;
	}

	public void setMedia$ratings(List<Object> media$ratings) {
		this.media$ratings = media$ratings;
	}

	public List<Object> getMedia$countries() {
		return media$countries;
	}

	public void setMedia$countries(List<Object> media$countries) {
		this.media$countries = media$countries;
	}

	public Boolean getMedia$excludeCountries() {
		return media$excludeCountries;
	}

	public void setMedia$excludeCountries(Boolean media$excludeCountries) {
		this.media$excludeCountries = media$excludeCountries;
	}

	public String getMedia$text() {
		return media$text;
	}

	public void setMedia$text(String media$text) {
		this.media$text = media$text;
	}

	public List<Object> getMedia$content() {
		return media$content;
	}

	public void setMedia$content(List<Object> media$content) {
		this.media$content = media$content;
	}

	public List<Object> getMedia$thumbnails() {
		return media$thumbnails;
	}

	public void setMedia$thumbnails(List<Object> media$thumbnails) {
		this.media$thumbnails = media$thumbnails;
	}

	public List<Object> getPla$adminTags() {
		return pla$adminTags;
	}

	public void setPla$adminTags(List<Object> pla$adminTags) {
		this.pla$adminTags = pla$adminTags;
	}

	public Boolean getPlmedia$approved() {
		return plmedia$approved;
	}

	public void setPlmedia$approved(Boolean plmedia$approved) {
		this.plmedia$approved = plmedia$approved;
	}

	public List<Object> getPlmedia$categoryIds() {
		return plmedia$categoryIds;
	}

	public void setPlmedia$categoryIds(List<Object> plmedia$categoryIds) {
		this.plmedia$categoryIds = plmedia$categoryIds;
	}

	public List<Plmediachapter> getPlmedia$chapters() {
		return plmedia$chapters;
	}

	public void setPlmedia$chapters(List<Plmediachapter> plmedia$chapters) {
		this.plmedia$chapters = plmedia$chapters;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public PlmediatitleLocalized getPlmedia$titleLocalized() {
		return plmedia$titleLocalized;
	}

	public void setPlmedia$titleLocalized(PlmediatitleLocalized plmedia$titleLocalized) {
		this.plmedia$titleLocalized = plmedia$titleLocalized;
	}

	public PlmediadescriptionLocalized getPlmedia$descriptionLocalized() {
		return plmedia$descriptionLocalized;
	}

	public void setPlmedia$descriptionLocalized(PlmediadescriptionLocalized plmedia$descriptionLocalized) {
		this.plmedia$descriptionLocalized = plmedia$descriptionLocalized;
	}

	public PlmediaauthorLocalized getPlmedia$authorLocalized() {
		return plmedia$authorLocalized;
	}

	public void setPlmedia$authorLocalized(PlmediaauthorLocalized plmedia$authorLocalized) {
		this.plmedia$authorLocalized = plmedia$authorLocalized;
	}

	public PlmediacopyrightLocalized getPlmedia$copyrightLocalized() {
		return plmedia$copyrightLocalized;
	}

	public void setPlmedia$copyrightLocalized(PlmediacopyrightLocalized plmedia$copyrightLocalized) {
		this.plmedia$copyrightLocalized = plmedia$copyrightLocalized;
	}

	public PlmediacopyrightUrlLocalized getPlmedia$copyrightUrlLocalized() {
		return plmedia$copyrightUrlLocalized;
	}

	public void setPlmedia$copyrightUrlLocalized(PlmediacopyrightUrlLocalized plmedia$copyrightUrlLocalized) {
		this.plmedia$copyrightUrlLocalized = plmedia$copyrightUrlLocalized;
	}

	public PlmediakeywordsLocalized getPlmedia$keywordsLocalized() {
		return plmedia$keywordsLocalized;
	}

	public void setPlmedia$keywordsLocalized(PlmediakeywordsLocalized plmedia$keywordsLocalized) {
		this.plmedia$keywordsLocalized = plmedia$keywordsLocalized;
	}

	public PlmedialinkLocalized getPlmedia$linkLocalized() {
		return plmedia$linkLocalized;
	}

	public void setPlmedia$linkLocalized(PlmedialinkLocalized plmedia$linkLocalized) {
		this.plmedia$linkLocalized = plmedia$linkLocalized;
	}

	public PlmediatextLocalized getPlmedia$textLocalized() {
		return plmedia$textLocalized;
	}

	public void setPlmedia$textLocalized(PlmediatextLocalized plmedia$textLocalized) {
		this.plmedia$textLocalized = plmedia$textLocalized;
	}

	public String getPlmedia$defaultThumbnailUrl() {
		return plmedia$defaultThumbnailUrl;
	}

	public void setPlmedia$defaultThumbnailUrl(String plmedia$defaultThumbnailUrl) {
		this.plmedia$defaultThumbnailUrl = plmedia$defaultThumbnailUrl;
	}

	public List<Object> getPlmedia$originalOwnerIds() {
		return plmedia$originalOwnerIds;
	}

	public void setPlmedia$originalOwnerIds(List<Object> plmedia$originalOwnerIds) {
		this.plmedia$originalOwnerIds = plmedia$originalOwnerIds;
	}

	public String getPlmedia$provider() {
		return plmedia$provider;
	}

	public void setPlmedia$provider(String plmedia$provider) {
		this.plmedia$provider = plmedia$provider;
	}

	public String getPlmedia$providerId() {
		return plmedia$providerId;
	}

	public void setPlmedia$providerId(String plmedia$providerId) {
		this.plmedia$providerId = plmedia$providerId;
	}

	public String getPlmedia$restrictionId() {
		return plmedia$restrictionId;
	}

	public void setPlmedia$restrictionId(String plmedia$restrictionId) {
		this.plmedia$restrictionId = plmedia$restrictionId;
	}

	public String getPlmedia$adPolicyId() {
		return plmedia$adPolicyId;
	}

	public void setPlmedia$adPolicyId(String plmedia$adPolicyId) {
		this.plmedia$adPolicyId = plmedia$adPolicyId;
	}

	public List<Object> getPlmedia$originalMediaIds() {
		return plmedia$originalMediaIds;
	}

	public void setPlmedia$originalMediaIds(List<Object> plmedia$originalMediaIds) {
		this.plmedia$originalMediaIds = plmedia$originalMediaIds;
	}

	public String getPlmedia$programId() {
		return plmedia$programId;
	}

	public void setPlmedia$programId(String plmedia$programId) {
		this.plmedia$programId = plmedia$programId;
	}

	public String getPlmedia$seriesId() {
		return plmedia$seriesId;
	}

	public void setPlmedia$seriesId(String plmedia$seriesId) {
		this.plmedia$seriesId = plmedia$seriesId;
	}

	public String getPlmedia$availabilityState() {
		return plmedia$availabilityState;
	}

	public void setPlmedia$availabilityState(String plmedia$availabilityState) {
		this.plmedia$availabilityState = plmedia$availabilityState;
	}

	public String getPlmedia$pid() {
		return plmedia$pid;
	}

	public void setPlmedia$pid(String plmedia$pid) {
		this.plmedia$pid = plmedia$pid;
	}

	public String getPlmedia$publicUrl() {
		return plmedia$publicUrl;
	}

	public void setPlmedia$publicUrl(String plmedia$publicUrl) {
		this.plmedia$publicUrl = plmedia$publicUrl;
	}

	public List<Object> getPlmedia$availabilityTags() {
		return plmedia$availabilityTags;
	}

	public void setPlmedia$availabilityTags(List<Object> plmedia$availabilityTags) {
		this.plmedia$availabilityTags = plmedia$availabilityTags;
	}

	public List<Object> getPlmedia$availabilityWindows() {
		return plmedia$availabilityWindows;
	}

	public void setPlmedia$availabilityWindows(List<Object> plmedia$availabilityWindows) {
		this.plmedia$availabilityWindows = plmedia$availabilityWindows;
	}

	public Boolean getPl1$autoPublishComments() {
		return pl1$autoPublishComments;
	}

	public void setPl1$autoPublishComments(Boolean pl1$autoPublishComments) {
		this.pl1$autoPublishComments = pl1$autoPublishComments;
	}

	public Boolean getPl1$availableToApps() {
		return pl1$availableToApps;
	}

	public void setPl1$availableToApps(Boolean pl1$availableToApps) {
		this.pl1$availableToApps = pl1$availableToApps;
	}

	public Pl1contentLink1 getPl1$contentLink1() {
		return pl1$contentLink1;
	}

	public void setPl1$contentLink1(Pl1contentLink1 pl1$contentLink1) {
		this.pl1$contentLink1 = pl1$contentLink1;
	}

	public Pl1contentLink2 getPl1$contentLink2() {
		return pl1$contentLink2;
	}

	public void setPl1$contentLink2(Pl1contentLink2 pl1$contentLink2) {
		this.pl1$contentLink2 = pl1$contentLink2;
	}

	public Pl1contentLink3 getPl1$contentLink3() {
		return pl1$contentLink3;
	}

	public void setPl1$contentLink3(Pl1contentLink3 pl1$contentLink3) {
		this.pl1$contentLink3 = pl1$contentLink3;
	}

	public Boolean getPl1$enableComments() {
		return pl1$enableComments;
	}

	public void setPl1$enableComments(Boolean pl1$enableComments) {
		this.pl1$enableComments = pl1$enableComments;
	}

	public String getPl1$grouping() {
		return pl1$grouping;
	}

	public void setPl1$grouping(String pl1$grouping) {
		this.pl1$grouping = pl1$grouping;
	}

	public String getPl1$language() {
		return pl1$language;
	}

	public void setPl1$language(String pl1$language) {
		this.pl1$language = pl1$language;
	}

	public String getPl1$loggerId() {
		return pl1$loggerId;
	}

	public void setPl1$loggerId(String pl1$loggerId) {
		this.pl1$loggerId = pl1$loggerId;
	}

	public String getPl1$pilatId() {
		return pl1$pilatId;
	}

	public void setPl1$pilatId(String pl1$pilatId) {
		this.pl1$pilatId = pl1$pilatId;
	}

	public String getPl1$programName() {
		return pl1$programName;
	}

	public void setPl1$programName(String pl1$programName) {
		this.pl1$programName = pl1$programName;
	}

	public Boolean getPl1$publishSonyAssets() {
		return pl1$publishSonyAssets;
	}

	public void setPl1$publishSonyAssets(Boolean pl1$publishSonyAssets) {
		this.pl1$publishSonyAssets = pl1$publishSonyAssets;
	}

	public Boolean getPl1$saveForRepeat() {
		return pl1$saveForRepeat;
	}

	public void setPl1$saveForRepeat(Boolean pl1$saveForRepeat) {
		this.pl1$saveForRepeat = pl1$saveForRepeat;
	}

	public Boolean getPl1$seriesPremiere() {
		return pl1$seriesPremiere;
	}

	public void setPl1$seriesPremiere(Boolean pl1$seriesPremiere) {
		this.pl1$seriesPremiere = pl1$seriesPremiere;
	}

	public String getPl1$site() {
		return pl1$site;
	}

	public void setPl1$site(String pl1$site) {
		this.pl1$site = pl1$site;
	}

	public String getPl1$useType() {
		return pl1$useType;
	}

	public void setPl1$useType(String pl1$useType) {
		this.pl1$useType = pl1$useType;
	}

	public List<Plmediachapter> getPlmedia$adslots() {
		return plmedia$adslots;
	}

	public void setPlmedia$adslots(List<Plmediachapter> plmedia$adslots) {
		this.plmedia$adslots = plmedia$adslots;
	}
	
	@Override
	public String toString() {
		String str = "";
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			logger.info("Error occured while media object created:{}", e.getMessage());
		}
		return str;
	}
}