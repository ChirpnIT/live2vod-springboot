
package com.chirpn.live2vod.api;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.enums.AssetStatus;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.AssetDTO;
import com.chirpn.live2vod.model.mpx.Media;
import com.chirpn.live2vod.repository.ChannelRepository;
import com.chirpn.live2vod.service.assetsservice.AssetService;
import com.chirpn.live2vod.service.mpx.MPXService;

/**
 * @author awalunj
 *
 */
@RestController
@RequestMapping(value = "api/assets")
public class AssetsController {

	private static final Logger logger = LoggerFactory.getLogger(AssetsController.class);

	@Autowired
	private AssetService assetService;

	@Autowired
	ChannelRepository channelRepository;

	@Autowired
	private MPXService mpxService;
	


	/**
	 * @param assetDTO
	 * @return
	 */
	@PostMapping("/{streamNo}")
	public ResponseEntity<Response<AssetDTO>> saveAsset(@RequestBody AssetDTO assetDTO, 
			@PathVariable("streamNo") String streamNo, 
			@RequestParam(value = "proceed", required = false) boolean proceed) {
		Response<AssetDTO> response = new Response<>();
		try {
			response = assetService.saveAsset(assetDTO, streamNo, proceed);
		} catch (Live2VodException le) {
			logger.error("User defined exception occured while performing save asset :{}", le.getMessage());
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while save asset {}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	/**
	 * @return
	 */
	@GetMapping
	public ResponseEntity<Response<Page<AssetDTO>>> getAssetsList(@RequestParam(name = "channel_no", required = false) Optional<String> optionalChannelNo, 
			@RequestParam(name = "date", required = false) 
			@DateTimeFormat(pattern = "dd/MM/yyyy") Optional<LocalDate> optionalDate,	
			@RequestParam(name = "asset_status", required = false) Optional<AssetStatus> optionalAssetStatus, 
			Pageable pageable) {
		Response<Page<AssetDTO>> response = new Response<>();
		try {
			response = assetService.getAssetsList(optionalChannelNo, optionalDate, optionalAssetStatus, pageable);
		} catch (Live2VodException le) {
			logger.error("Error while performing retrieve asset list:{}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while to get assets records{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());

	}

	/**
	 * @param assetNo
	 * @return
	 */
	@GetMapping("/{assetNo}")
	public ResponseEntity<Response<AssetDTO>> getAssetByAssetNo(@PathVariable("assetNo") String assetNo) {
		Response<AssetDTO> response = new Response<>();
		try {
			response = assetService.getAssetByAssetNo(assetNo);
		} catch (Live2VodException le) {
			logger.error("Error while performing get asset by assetNo:{}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while to get assets records by id{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	/**
	 * @param assetDTO
	 * @return
	 */
	@PutMapping
	public ResponseEntity<Response<AssetDTO>> updateAsset(@RequestBody AssetDTO assetDTO) {
		Response<AssetDTO> response = new Response<>();
		try {
			response = assetService.updateAsset(assetDTO);
		} catch (Live2VodException le) {
			logger.info("Error while update asset:{}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while update asset {} ", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	/**
	 * @param assetDTO
	 * @return
	 */
	@PutMapping("/cancel/{asset_no}")
	public ResponseEntity<Response<AssetDTO>> cancelScheduledAsset(@PathVariable("asset_no") String assetNo) {
		Response<AssetDTO> response = new Response<>();
		try {
			response = assetService.cancelScheduledAsset(assetNo);
		} catch (Live2VodException le) {
			logger.info("Error while cancel schedule asset: {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while update asset: {}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	/**
	 * @param assetDTO
	 * @return
	 */
	@PutMapping("/publish/{asset_no}")
	public ResponseEntity<Response<AssetDTO>> publishAsset(@PathVariable("asset_no") String assetNo) {
		Response<AssetDTO> response = new Response<>();
		try {
			response = assetService.publishAsset(assetNo);
		} catch (Live2VodException le) {
			logger.info("Error while publish asset: {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while update asset:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	/**
	 * @param
	 * @return
	 */
	@DeleteMapping("/{assetsNo}")
	public ResponseEntity<Response<String>> deleteAssets(@PathVariable("assetsNo") String assetsNo) {
		Response<String> response = new Response<>();
		try {
			response = assetService.deleteAssets(assetsNo);
		} catch (Live2VodException e) {
			logger.error("Error occurred for deleting assets:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		} catch (Exception e) {
			logger.error("Error occurred while deleting assets:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	/**
	 * @param
	 * @return
	 */
	@PostMapping("/test/mpxMedia")
	public ResponseEntity<Response<Media>> getMpxMedia(@RequestBody AssetDTO assetDTO) {
		Response<Media> response = new Response<>();
		try {
			Media media = new Media();
			mpxService.mapAssetToMedia(assetDTO, media);
			response.setStatus(HttpStatus.OK);
			response.setData(media);

		} catch (Exception e) {
			logger.error("Error occurred while getMpxMedia assets:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
}
