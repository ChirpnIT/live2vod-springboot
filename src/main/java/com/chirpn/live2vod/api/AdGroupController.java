package com.chirpn.live2vod.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.service.adgroupservice.AdGroupService;

/**
 * 
 * @author Satish ghonge
 * 
 */

@RestController
@RequestMapping(value = "api/group")
@CrossOrigin(origins = { "http://localhost:3000", "http://chirpn.in" })
public class AdGroupController {

	private static final Logger logger = LoggerFactory.getLogger(AdGroupController.class);

	@Autowired
	private AdGroupService adGroupService;

	/**
	 * @param channelModel
	 * @return
	 */
	@PostMapping("/create")
	public ResponseEntity<Response<List<ChannelDTO>>> update(@RequestBody List<ChannelDTO> channelModel) {
		Response<List<ChannelDTO>> response = new Response<>();
		try {
			response = adGroupService.createNewGroup(channelModel);
		} catch (Live2VodException le) {
			logger.info("Error : {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while create group records:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

}
