package com.chirpn.live2vod.api.response;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
public class Response<T> {

	private HttpStatus status = HttpStatus.OK;
	private String statusMessage;

	@JsonProperty("page")
	private Page<T> page;

	@JsonProperty("payload")
	private T data;

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Page<T> getPage() {
		return page;
	}

	public void setPage(Page<T> page) {
		this.page = page;
	}

	@Override
	public String toString() {
		String str = "";
		str = str.concat("{\nstatus : " + status);
		str = str.concat("\nstatusMessage : " + statusMessage);
		str = str.concat("\n}");
		return str;
	}

}
