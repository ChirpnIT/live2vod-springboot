package com.chirpn.live2vod.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.IntegrationPropertiesDTO;
import com.chirpn.live2vod.service.integrationpropertiesservice.IntegrationPropertiesService;

/**
 * 
 * @author Satish ghonge
 * 
 */

@RestController
@RequestMapping(value = "api/integration/properties")
@CrossOrigin(origins = { "http://localhost:3000", "http://chirpn.in" })
public class IntegrationPropertiesController {

	private static final Logger logger = LoggerFactory.getLogger(IntegrationPropertiesController.class);

	@Autowired
	private IntegrationPropertiesService integrationPropertiesService;

	@PostMapping
	public ResponseEntity<Response<IntegrationPropertiesDTO>> create(
			@RequestBody IntegrationPropertiesDTO integrationProperties) {
		Response<IntegrationPropertiesDTO> response = new Response<>();
		try {
			response = integrationPropertiesService.createIntegrationProperties(integrationProperties);
		} catch (Live2VodException le) {
			logger.info("Error while create integration properties:{}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while create integration properties record", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

	@GetMapping
	public ResponseEntity<Response<IntegrationPropertiesDTO>> retrieve() {
		Response<IntegrationPropertiesDTO> response = new Response<>();
		try {
			response = integrationPropertiesService.retrieve();
		} catch (Live2VodException le) {
			logger.info("Error while retrieve integration properties record:{}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while retrieve integration properties record", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

}
