package com.chirpn.live2vod.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.ChannelDTO;
import com.chirpn.live2vod.service.channelservice.ChannelService;

/**
 * 
 * @author Satish ghonge
 * 
 */

@RestController
@RequestMapping(value = "api/channels")
@CrossOrigin(origins = { "http://localhost:3000", "http://chirpn.in" })
public class ChannelController {

	private static final Logger logger = LoggerFactory.getLogger(ChannelController.class);

	@Autowired
	private ChannelService channelService;

	/**
	 * @return
	 */
	@GetMapping
	public ResponseEntity<Response<List<ChannelDTO>>> retrieve() {
		Response<List<ChannelDTO>> response = new Response<>();
		try {
			response = channelService.retrieve();
		} catch (Live2VodException le) {
			logger.info("Error while retrieve channel: {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while to get channel records", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	/**
	 * @return
	 */
	@GetMapping("/{channel_no}")
	public ResponseEntity<Response<ChannelDTO>> retrieve(@PathVariable ("channel_no") String channelNo) {
		Response<ChannelDTO> response = new Response<>();
		try {
			response = channelService.retrieve(channelNo);
		} catch (Live2VodException le) {
			logger.info("Error: {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while to get channel records{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	/**
	 * @param channelModel
	 * @return
	 */
	@PreAuthorize("@securityService.isAdministrator()")
	@PostMapping
	public ResponseEntity<Response<List<ChannelDTO>>> create(@RequestBody List<ChannelDTO> channelModel) {
		Response<List<ChannelDTO>> response = new Response<>();
		try {
			response = channelService.create(channelModel);
		} catch (Live2VodException le) {
			logger.info("Error: {} ", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while create channel records:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	/**
	 * @param channelModel
	 * @return
	 */
	@PreAuthorize("@securityService.isAdministrator()")
	@PutMapping
	public ResponseEntity<Response<List<ChannelDTO>>> update(@RequestBody List<ChannelDTO> channelModel) {
		Response<List<ChannelDTO>> response = new Response<>();
		try {
			response = channelService.update(channelModel);
		} catch (Live2VodException le) {
			logger.info("Error: {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while update channel records:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}
	
	/**
	 * @param channelModel
	 * @return
	 */
	@PreAuthorize("@securityService.isAdministrator()")
	@DeleteMapping("/{channel_no}")
	public ResponseEntity<Response<String>> delete(@PathVariable ("channel_no") String channelNo) {
		Response<String> response = new Response<>();
		try {
			response = channelService.delete(channelNo);
		} catch (Live2VodException le) {
			logger.info("Error : {}", le.getMessage());
			response.setStatus(HttpStatus.BAD_REQUEST);
			response.setStatusMessage(le.getMessage());
		} catch (Exception e) {
			logger.error("Error occurred while delete channel records:{}", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

}
