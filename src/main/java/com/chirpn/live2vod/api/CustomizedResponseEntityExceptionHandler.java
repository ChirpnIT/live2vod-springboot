package com.chirpn.live2vod.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.common.exception.UnauthorizedChannelAccessException;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Response> handleAllExceptions(Exception ex, WebRequest request) {
		Response response = new Response();
		response.setStatusMessage(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@ExceptionHandler(Live2VodException.class)
	public final ResponseEntity<Response> handleLive2VodExceptionExceptions(Live2VodException ex, WebRequest request) {
		Response response = new Response();
		response.setStatusMessage(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(UnauthorizedChannelAccessException.class)
	public final ResponseEntity<Response> handleUnauthorizedChannelAccessExceptions(UnauthorizedChannelAccessException ex, WebRequest request) {
		Response response = new Response();
		response.setStatusMessage(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
