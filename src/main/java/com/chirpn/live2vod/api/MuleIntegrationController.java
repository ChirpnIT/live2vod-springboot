package com.chirpn.live2vod.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chirpn.live2vod.api.response.Response;
import com.chirpn.live2vod.common.exception.Live2VodException;
import com.chirpn.live2vod.model.ChannelContentDTO;
import com.chirpn.live2vod.service.muleintegrationservice.MuleIntegrationService;

/**
 * 
 * @author Satish ghonge
 * 
 */

@RestController
@RequestMapping(value = "api/delta")
@CrossOrigin(origins = { "http://localhost:3000", "http://chirpn.in" })
public class MuleIntegrationController {

	private static final Logger logger = LoggerFactory.getLogger(MuleIntegrationController.class);

	@Autowired
	private MuleIntegrationService muleIntegrationService;

	/**
	 * @param streamId
	 * @return
	 */
	@GetMapping("/preview/stream/{channelNo}")
	public ResponseEntity<Response<ChannelContentDTO>> callToMuleForPreviewStream(
			@PathVariable("channelNo") String channelNo) {
		Response<ChannelContentDTO> response = new Response<>();
		try {
			response = muleIntegrationService.callMuleApiToRetrievePreviewAssetsStream(channelNo);
		} catch (Live2VodException e) {
			logger.error("Error occurred while preview stream", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		} catch (Exception e) {
			logger.error("Error occurred while preview stream", e);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setStatusMessage(HttpStatus.INTERNAL_SERVER_ERROR.name());
			return ResponseEntity.badRequest().body(response);
		}
		return new ResponseEntity<>(response, response.getStatus());
	}

}
